import { getProtected, postProtected } from '../configs/axiosProtected'
import { postPublic } from '../configs/axiosPublic'

 export const logninStaff = async (data:any)=>{
    return await  postPublic("http://localhost:3001/accounts/login/staff",data);
 } 

 export const logninCustomer= async (data:any)=>{
   return await  postPublic("http://localhost:3001/accounts/login/customer",data);
} 

 export const register = async (data:any)=>{
   return await  postPublic("http://localhost:3001/accounts",data);
} 

 export const getAccountInfo = async (data?:any)=>{
    return await  getProtected(`http://localhost:3001/accounts/logined`);
 } 