import { postProtected } from "../configs/axiosProtected";
import { apiUrl } from "../configs/urls";

export const ListLogs = async (data: any) => {
  return await postProtected(apiUrl + "/logs", data);
};
