import { getPublic } from "../configs/axiosPublic";
import {
  postProtected,
  getProtected,
  deleteProtected,
  putProtected,
} from "../configs/axiosProtected";
import { apiUrl } from "../configs/urls";

type accountListType = {
  limit: number;
  offset: number;
};

export const CreateAccount = async (data: any) => {
  return await postProtected(apiUrl + "/accounts", data);
};

export const ListAccount = async (data: any) => {
  return await postProtected(
    apiUrl + `/listaccounts` , data
  );
};

export const DeleteAccount = async (code: string) => {
  return await deleteProtected(apiUrl + `/accounts/${code}`);
};

export const FindbycodeAccount = async (code: string) => {
  return await getProtected(apiUrl + `/accounts/${code}`);
};

export const UpdateAccount = async (data: any) => {
  return await putProtected(apiUrl + `/accounts/${data.code}`, data.data);
};
