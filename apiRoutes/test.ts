import { getPublic } from '../configs/axiosPublic'

 export const fetchTest = async ()=>{
    return await  getPublic("https://jsonplaceholder.typicode.com/posts")
 } 