import {
  getProtected,
  postProtected,
  putProtected,
} from "../configs/axiosProtected";
import { apiUrl } from "../configs/urls";

export const CreateHotline = async (data: any) => {
  return await postProtected(apiUrl + "/hotline", data);
};

export const ListHotline = async (data: any) => {
  return await postProtected(apiUrl + `/listhotline`, data);
};

export const UpdateHotline = async (data: any) => {
  return await putProtected(apiUrl + `/hotline/${data.code}`, data.data);
};

export const FindbyCodeHotline = async (data: any) => {
  return await getProtected(apiUrl + `/hotline/${data}`);
};
//Action giữ số
export const KeepingHotline = async (data: any) => {
  return await postProtected(apiUrl + `/keepinghotline`, data);
};

//Action thu hồi số
export const EvictingHotline = async (data: any) => {
  return await postProtected(apiUrl + `/evictinghotline`, data);
};

//Action cập nhật trạng thái chưa sử dụng
export const ReadyHotline = async (data: any) => {
  return await postProtected(apiUrl + `/readyhotline`, data);
};

//Action hủy số
export const CancelHotline = async (data: any) => {
  return await postProtected(apiUrl + `/cancelinghotline`, data);
};


export const DoingHotline = async (data: any) => {
  return await postProtected(apiUrl + `/doinghotline`, data);
};

export const WaitingbrandnameHotline = async (data: any) => {
  return await postProtected(apiUrl + `/waitingbrandnamehotline`, data);
};



