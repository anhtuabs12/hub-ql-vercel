import { postProtected } from "../configs/axiosProtected";
import { apiUrl } from "../configs/urls";

export const importHotline = async (data: any) => {
    return await postProtected(apiUrl + "/importhotline", data);
  };