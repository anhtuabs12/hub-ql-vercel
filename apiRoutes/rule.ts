import { getPublic } from "../configs/axiosPublic";
import {
  postProtected,
  getProtected,
  deleteProtected,
  putProtected
} from "../configs/axiosProtected";
import { apiUrl } from "../configs/urls";

type postRuleType = {
  Type: String,
  Value: String,
  Title:String,
};

type putRuleType = {
  code :String,
  body: any
}

export const postRule = async (data: postRuleType) => {
  return await postProtected(apiUrl + "/rule", data);
};

export const listRule = async (data: any) => {
  return await getProtected(
      apiUrl + `/rule`
    );
  // let query = '?'
  // for (let key of Object.keys(data)) {
  //   let item = key + "=" + data[key] + '&'
  //   query += item
  // }
  // query = query.slice(0, query.length - 1)

  // return await getProtected(
  //   apiUrl + `/rule/${query}`
  // );
};
export const putRule = async (data: putRuleType) => {
  return await putProtected(apiUrl + `/rule/${data.code}`, data.body);
};

export const deleteRule = async (code: string) => {
  return await deleteProtected(apiUrl + `/rule/${code}`);
};

export const findbycodeRule = async (code: string) => {
  return await getProtected(apiUrl + `/rule/${code}`);
};



