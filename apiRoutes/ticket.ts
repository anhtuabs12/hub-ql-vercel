import { getPublic } from "../configs/axiosPublic";
import {
  postProtected,
  getProtected,
  deleteProtected,
  putProtected
} from "../configs/axiosProtected";
import { apiUrl } from "../configs/urls";

type postTicketType = {
  Type: String,
  Value: String,
  Title:String,
};

type putTicketType = {
  code :String,
  body: any
}

export const postTicket = async (data: postTicketType) => {
  console.log(data,"data_ticket")
  return await postProtected(apiUrl + "/ticket", data);
};

export const listTicket = async (data: any) => {
  // let query = '?'
  // for (let key of Object.keys(data)) {
  //   let item = key + "=" + data[key] + '&'
  //   query += item
  // }
  // query = query.slice(0, query.length - 1)

  return await postProtected(
    apiUrl + `/listticket`,data
  );
};
export const putTicket = async (data: putTicketType) => {
  return await putProtected(apiUrl + `/ticket/${data.code}`, data.body);
};

export const deleteTicket = async (code: string) => {
  return await deleteProtected(apiUrl + `/ticket/${code}`);
};

export const findByCodeTicket = async (code: string) => {
  return await getProtected(apiUrl + `/ticket/${code}`);
};



