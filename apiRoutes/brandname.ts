import { getPublic } from "../configs/axiosPublic";
import {
  postProtected,
  getProtected,
  deleteProtected,
  putProtected,
} from "../configs/axiosProtected";
import { apiUrl } from "../configs/urls";

export const postBrandname = async (data: any) => {
  return await postProtected(apiUrl + "/brandname", data);
};

export const listBrandname = async (data: any) => {
  return await postProtected(apiUrl + `/listbrandname`, data);
};
export const putBrandname = async (data: any) => {
  return await putProtected(apiUrl + `/brandname/${data.code}`, data.body);
};

export const deleteBrandname = async (code: string) => {
  return await deleteProtected(apiUrl + `/brandname/${code}`);
};

export const findByCodeBrandname = async (code: string) => {
  return await getProtected(apiUrl + `/brandname/${code}`);
};
