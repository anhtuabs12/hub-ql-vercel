import FormRender from "@/components/forms/FormRender";
import LayoutCpn from "@/components/layouts/LayoutCpn";
import Modalcpn from "@/components/modals";
import TableDefault from "@/components/tables/TableDefault";
import TableDefultEditer from "@/components/tables/TableDefultEditer";
import TitlePage from "@/components/titles/TitlePage";
import {
  AppstoreOutlined,
  InboxOutlined,
  PlusOutlined,
  SearchOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { Button, Input, Modal, Popover, Upload } from "antd";
import React, { useRef } from "react";
import { useForm } from "react-hook-form";
const { Dragger } = Upload;

type Props = {
  observer: any;
};

const TemplateRule_public = ({ observer }: Props) => {
  const total = observer.total;
  const isModalOpen = observer.isModalOpen;
  const handleOk = observer.handleOk;
  const handleCancel = observer.handleCancel;
  const propsupload = observer.propsupload;
  const columnsUpload = observer.columnsUpload;
  const dataSource = observer.dataSource;
  const showModal = observer.showModal;
  const content = observer.content;
  const columns = observer.columns;
  const page = observer.page;
  const setPage = observer.setPage;
  const DataModals = observer.DataModals;
  const hanldeOpenCreate = observer.hanldeOpenCreate;
  const handleReload = observer.handleReload;
  const handleSearch = observer.handleSearch

  const nameSearchref = useRef() as any
  const data = observer.data;
  const setData = observer.setData;
  const editingKey = observer.editingKey;
  const setEditingKey = observer.setEditingKey;
  const isEditing = observer.isEditing;
  const edit = observer.edit;
  const cancel = observer.cancel;
  const save = observer.save;
  const form = observer.form;
  const contextHolder = observer.contextHolder;
  return (
    <>
    {contextHolder}
      <Modalcpn DataModals={DataModals} />

      <LayoutCpn>
        <div>
          <div className="flex items-center justify-between">
            <TitlePage>Danh sách rule</TitlePage>
            <div className="flex items-center">
              {/* <Button
                onClick={showModal}
                className="bg-blue-500 text-white text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center"
              >
                <PlusOutlined className="text-[14px] text-white " /> Upload
              </Button>
              <Button className="bg-white text-black text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center">
                <PlusOutlined className="text-[14px] text-black " /> Ghi âm
              </Button> */}
            </div>
          </div>
          {/* <Button type="primary" className="bg-blue-500" onClick={showModal}>
                Open Modal
              </Button> */}
          <div className="flex items-center justify-between hidden">
            <div className="flex items-center">
              <div className="flex items-center">
                <Input
                ref={nameSearchref}
                  className="w-[250px] h-[34px] placeholder:italic"
                  placeholder="Tên khách hàng..."
                />
                <Button onClick={()=>{handleSearch(nameSearchref.current.input.value)}} className="bg-blue-500 text-white font-bold rounded-lg h-[36px] ml-[4px]">
                  <SearchOutlined className="text-[18px] " />
                </Button>
              </div>

              <div className="flex items-center">
                {/* <Popover
                  className="ml-[16px]"
                  placement="bottomRight"
                  content={content}
                  title="Bộ lọc"
                  trigger="click"
                  overlayStyle={{ width: "800px" }}
                >
                  <Button className=" bg-blue-500 text-white text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center hidden">
                    <AppstoreOutlined className="text-[20px] text-white " /> Bộ
                    lọc
                  </Button>
                </Popover> */}

                <Button
                  onClick={handleReload}
                  className="ml-[16px] bg-white text-blue-500 border-blue-500 text-[14px]  rounded-lg h-[36px]  flex items-center"
                >
                  <SyncOutlined className="text-[20px] text-blue-500 " /> Làm
                  mới
                </Button>
              </div>
            </div>
            <div>
              <Button
                onClick={hanldeOpenCreate}
                className=" bg-blue-500 text-white text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center"
              >
                <PlusOutlined className="text-[14px] text-white " />
                Tạo mới
              </Button>
            </div>
          </div>
          <div>
          <TableDefultEditer cancel={cancel} isEditing={isEditing} form={form}columns={columns} setData={setData} data={data} editingKey={editingKey} setEditingKey={setEditingKey}/>

            {/* <TableDefault
              columns={columns}
              dataSource={dataSource}
              setPage={setPage}
              total={total || 10}
            /> */}
          </div>
        </div>
      </LayoutCpn>
    </>
  );
};

export default TemplateRule_public;
