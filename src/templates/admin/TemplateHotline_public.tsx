import FromFilter from "@/components/filters/FromFilter";
import LayoutCpn from "@/components/layouts/LayoutCpn";
import Modalcpn from "@/components/modals";
import TableDefault from "@/components/tables/TableDefault";
import TitlePage from "@/components/titles/TitlePage";
import {
  AppstoreAddOutlined,
  AppstoreOutlined,
  DownloadOutlined,
  InboxOutlined,
  PlusOutlined,
  ReloadOutlined,
  SearchOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { Button, Input, Modal, Popconfirm, Popover, Upload } from "antd";
import React from "react";
import {
  checkStaffAuth,
  checkStaffSaleAdminAuth,
} from "../../../constants/permissions";
import { ROLE_TYPE, TICKET_TYPE } from "../../../constants/enum";
import { ModalStatus } from "@/components/tables/enums";
const { Dragger } = Upload;

type Props = {
  observer: any;
};

const TemplateHotline_public = ({ observer }: Props) => {
  const isModalOpen = observer.isModalOpen;
  const handleOk = observer.handleOk;
  const handleCancel = observer.handleCancel;
  const propsupload = observer.propsupload;
  const columnsUpload = observer.columnsUpload;
  const dataSource = observer.dataSource;
  const showModal = observer.showModal;
  const content = observer.content;
  const columns = observer.columns;
  const DataModals = observer.DataModals;
  const hanldeOpenCreate = observer.hanldeOpenCreate;
  const showModalupload = observer.showModalupload;
  const isModalOpenUpload = observer.isModalOpenUpload;
  const handleCancelUpload = observer.handleCancelUpload;
  const rowSelectionDatas = observer.rowSelectionDatas;
  const contextHolder = observer.contextHolder;
  const total = observer.total;
  const loading = observer.loading;
  const setPage = observer.setPage;
  const handleReload = observer.handleReload;
  const handleDownloadexam = observer.handleDownloadexam;
  const form = observer.form;
  const fieldFilterDefault = observer.fieldFilterDefault;
  const fieldFilterMore = observer.fieldFilterMore;
  const handleResetFilter = observer.handleResetFilter;
  const accountInfo = observer.accountInfo;
  const handleKeepingHotline = observer.handleKeepingHotline;
  const handleActions = observer.handleActions;
  const modalStatus = observer.modalStatus;
  const cancelPopconfirm = observer.cancelPopconfirm;
  const exportExcell = observer.exportExcell;

  const actionCustom: any = () => {
    let datas = [
      {
        label: "unstaff",
        content: (
          <div>
            {accountInfo.Role == ROLE_TYPE.CUSTOMER ? (
              <Popconfirm
                title="Giữ số"
                description="Bạn có chắc chắn muốn giữ số đã chọn?"
                onConfirm={handleKeepingHotline}
                onCancel={cancelPopconfirm}
                okText="Xác nhận & xuất file"
                cancelText="Hủy"
              >
                <Button
                  onClick={handleKeepingHotline}
                  className="duration-300  bg-blue-500 text-white text-[14px] border-blue-500  rounded-md  ml-[4px] flex items-center"
                >
                  Giữ số
                </Button>
              </Popconfirm>
            ) : (
              <Button
                onClick={handleKeepingHotline}
                className="duration-300  bg-blue-500 text-white text-[14px] border-blue-500  rounded-md  ml-[4px] flex items-center"
              >
                Giữ số
              </Button>
            )}
          </div>
        ),
      },
      {
        label: "1",
        content: (
          <Popconfirm
            title="Thu hồi số"
            description="Có chắc chắn thu hồi số đã chọn?"
            onConfirm={() => handleActions(TICKET_TYPE.WITHDRAWAL)}
            onCancel={cancelPopconfirm}
            okText="Xác nhận & xuất file"
            cancelText="Hủy"
          >
            <Button className="duration-300  bg-blue-500 text-white text-[14px] border-blue-500  rounded-md  ml-[4px] flex items-center">
              Thu hồi số
            </Button>
          </Popconfirm>
        ),
      },
      {
        label: "2",
        content: (
          <Popconfirm
            title="Hủy số"
            description="Chắc chắn hủy các số đã chọn"
            onConfirm={() => handleActions(TICKET_TYPE.CANCEL_NUMBER)}
            onCancel={cancelPopconfirm}
            okText="Xác nhận & xuất file"
            cancelText="Hủy"
          >
            <Button className="duration-300  bg-blue-500 text-white text-[14px] border-blue-500  rounded-md  ml-[4px] flex items-center">
              Hủy số
            </Button>
          </Popconfirm>
        ),
      },

      {
        label: "3",
        content: (
          <Popconfirm
            title="Chưa sử dụng"
            description="Chắc chắn cập nhật trạng thái về chưa sử dụng?"
            onConfirm={() => handleActions(TICKET_TYPE.UNUSED)}
            onCancel={cancelPopconfirm}
            okText="Xác nhận & xuất file"
            cancelText="Hủy"
          >
            <Button className="duration-300  bg-blue-500 text-white text-[14px] border-blue-500  rounded-md  ml-[4px] flex items-center">
              Chưa sử dụng
            </Button>
          </Popconfirm>
        ),
      },
      {
        label: "",
        content: (
          <Button
            onClick={() => handleActions(TICKET_TYPE.DOING)}
            className="duration-300  bg-blue-500 text-white text-[14px] border-blue-500  rounded-md  ml-[4px] flex items-center"
          >
            Đang sử dụng
          </Button>
        ),
      },

      {
        label: "",
        content: (
          <Button
            onClick={() => handleActions(TICKET_TYPE.WAITING_BRANDNAME)}
            className="duration-300  bg-blue-500 text-white text-[14px] border-blue-500  rounded-md  ml-[4px] flex items-center"
          >
            Chờ gán name
          </Button>
        ),
      },
    ];
    return (
      <div className="flex items-center mb-3 ml-[10px]">
        {datas.map((value: any, index: any) => {
          if (checkStaffSaleAdminAuth(accountInfo)) {
            if (
              value.label == "unstaff" ||
              value.label == "1" ||
              value.label == "2" ||
              value.label == "3"
            ) {
              return (
                <div className="mx-2" key={index}>
                  {value.content}
                </div>
              );
            } else {
              return null;
            }
          }
          if (!checkStaffAuth(accountInfo)) {
            if (value.label == "unstaff") {
              return (
                <div className="mx-2" key={index}>
                  {value.content}
                </div>
              );
            } else {
              return null;
            }
          } else {
            return (
              <div className="mx-2" key={index}>
                {value.content}
              </div>
            );
          }
        })}
      </div>
    );
  };

  return (
    <>
      {contextHolder}
      <Modalcpn DataModals={DataModals} />
      <LayoutCpn>
        <div>
          <div className="flex items-center justify-between">
            <TitlePage>Danh sách hotline </TitlePage>
            <div className="flex items-center">
              <Button
                onClick={hanldeOpenCreate}
                type="primary"
                className=" hover:!text-white text-[12px]   rounded-md mx-6 flex items-center"
              >
                <PlusOutlined className="text-[14px] hover:!text-white" />
                Thêm
              </Button>

              <div
                onClick={handleReload}
                className="mx-6 text-sm cursor-pointer	flex items-center"
              >
                <ReloadOutlined className="mr-2" /> Làm mới
              </div>
              <div
                onClick={handleDownloadexam}
                className="mx-6 text-sm cursor-pointer	flex items-center"
              >
                <DownloadOutlined className="mr-2" /> Tải mẫu
              </div>
              <div
                className="mx-6 text-sm cursor-pointer	"
                onClick={showModalupload}
              >
                Nhập dữ liệu
              </div>
              <div onClick={exportExcell} className="mx-6 text-sm cursor-pointer	">Xuất dữ liệu</div>

              {/* <Button className="bg-white text-black text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center">
                <PlusOutlined className="text-[14px] text-black " /> Ghi âm
              </Button> */}
            </div>
          </div>
          {/* <Button type="primary" className="bg-blue-500" onClick={showModal}>
                Open Modal
              </Button> */}
          <div className="flex items-center justify-start">
            <div className="w-full mt-4">
              {modalStatus == ModalStatus.EDIT ||
              modalStatus == ModalStatus.ACTION_HOTLINE_DOING ||
              modalStatus == ModalStatus.ACTION_HOTLINE_KEEPING ||
              modalStatus ==
                ModalStatus.ACTION_HOTLINE_WAITING_BRANDNAME ? null : (
                <FromFilter
                  accountInfo={accountInfo}
                  handleResetFilter={handleResetFilter}
                  form={form}
                  fieldFilterDefault={fieldFilterDefault}
                  fieldFilterMore={fieldFilterMore}
                />
              )}
            </div>
            <div></div>
          </div>
          <div>
            <TableDefault
              actionCustom={actionCustom}
              rowSelectionStatus={true}
              columns={columns}
              dataSource={dataSource}
              rowSelectionDatas={rowSelectionDatas}
              total={total || 10}
              loading={loading}
              setPage={setPage}
            />
          </div>
        </div>
      </LayoutCpn>

      <Modal
        footer={null}
        width={1000}
        title="Import "
        open={isModalOpenUpload}
        onOk={handleOk}
        onCancel={handleCancelUpload}
      >
        <div className="mt-[20px]">
          <Dragger {...propsupload}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">
              <span className="text-blue-500">Click vào đây</span> để chọn file
            </p>
            <p className="ant-upload-hint">
              {/* (Hỗ trợ định dạng file WAV và MP3. Tối đa 10 files và dung lượng
              mỗi file không quá 10MB) */}
            </p>
          </Dragger>
          {/* <TableDefault columns={columns} dataSource={dataSource} /> */}
          <div className="mt-[20px] flex items-center justify-end">
            <Button className="bg-blue-500 text-white">Upload All</Button>
            <Button type="primary" className="ml-[12px]" danger>
              Remove all
            </Button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default TemplateHotline_public;
