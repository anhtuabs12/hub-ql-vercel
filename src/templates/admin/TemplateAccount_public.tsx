import LayoutCpn from "@/components/layouts/LayoutCpn";
import Modalcpn from "@/components/modals";
import TableAddFieldScript from "@/components/tables/TableDefultEditer";
import TableDefault from "@/components/tables/TableDefault";
import TitlePage from "@/components/titles/TitlePage";
import {
  AppstoreOutlined,
  DownloadOutlined,
  InboxOutlined,
  PlusOutlined,
  ReloadOutlined,
  SearchOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import {
  Button,
  Form,
  Input,
  Modal,
  Popconfirm,
  Popover,
  Tabs,
  TabsProps,
  Typography,
  Upload,
} from "antd";
import React, { useState } from "react";
import TableDefultEditer from "@/components/tables/TableDefultEditer";
import FromFilter from "@/components/filters/FromFilter";
import { ModalStatus } from "@/components/tables/enums";
const { Dragger } = Upload;

type Props = {
  observer: any;
};

const TemplateAccount_public = ({ observer }: Props) => {
  const isModalOpen = observer.isModalOpen;
  const handleOk = observer.handleOk;
  const handleCancel = observer.handleCancel;
  const propsupload = observer.propsupload;
  const columnsUpload = observer.columnsUpload;
  const dataSource = observer.dataSource;
  const showModal = observer.showModal;
  const content = observer.content;
  const columns = observer.columns;
  const DataModals = observer.DataModals;
  const hanldeOpenCreate = observer.hanldeOpenCreate;
  const page = observer.page;
  const setPage = observer.setPage;
  const total = observer.total;
  const loading = observer.loading;
  const contextHolder = observer.contextHolder;
  const handleReload = observer.handleReload;
  const onChangeTab = observer.onChangeTab;
  const columnsCustomer = observer.columnsCustomer;
  const showModalupload = observer.showModalupload;
  const isModalOpenUpload = observer.isModalOpenUpload;
  const handleCancelUpload = observer.handleCancelUpload;
  const rowSelectionDatas = observer.rowSelectionDatas;
  const handleDownloadexam = observer.handleDownloadexam;
  const fieldFilterDefault = observer.fieldFilterDefault;
  const fieldFilterMore = observer.fieldFilterMore;
  const handleResetFilter = observer.handleResetFilter;
  const accountInfo = observer.accountInfo;
  const modalStatus = observer.modalStatus;
  const form = observer.form;
  <TableDefault />;
  const items: TabsProps["items"] = [
    {
      key: "staff",
      label: "Nhân viên",
      children: (
        <TableDefault
          // actionCustom={actionCustom}
          rowSelectionStatus={true}
          columns={columns}
          dataSource={dataSource}
          rowSelectionDatas={rowSelectionDatas}
          total={total || 10}
          loading={loading}
          setPage={setPage}
        />
      ),
    },
    {
      key: "customer",
      label: "Khách hàng",
      children: (
        <TableDefault
          // actionCustom={actionCustom}
          rowSelectionStatus={true}
          columns={columnsCustomer}
          dataSource={dataSource}
          rowSelectionDatas={rowSelectionDatas}
          total={total || 10}
          loading={loading}
          setPage={setPage}
        />
      ),
    },
  ];
  return (
    // <>
    //   {contextHolder}
    //   <Modalcpn DataModals={DataModals} />

    //   <LayoutCpn>
    //     <div>
    //       <div className="flex items-center justify-between">
    //         <TitlePage>Danh sách tài khoản</TitlePage>
    //         <div className="flex items-center">
    //           {/* <Button
    //             onClick={showModal}
    //             className="bg-blue-500 text-white text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center"
    //           >
    //             <PlusOutlined className="text-[14px] text-white " /> Upload
    //           </Button>
    //           <Button className="bg-white text-black text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center">
    //             <PlusOutlined className="text-[14px] text-black " /> Ghi âm
    //           </Button> */}
    //         </div>
    //       </div>
    //       {/* <Button type="primary" className="bg-blue-500" onClick={showModal}>
    //             Open Modal
    //           </Button> */}
    //       <div className="flex items-center justify-between">
    //         <div className="flex items-center">
    //           <div className="flex items-center">
    //             <Input
    //               className="w-[250px] h-[34px] placeholder:italic"
    //               placeholder="Tên nhân viên..."
    //             />
    //             <Button className="bg-blue-500 text-white font-bold rounded-lg h-[36px] ml-[4px]">
    //               <SearchOutlined className="text-[18px] " />
    //             </Button>
    //           </div>

    //           <div className="flex items-center ">
    //             {/* <Popover
    //               className="ml-[16px]"
    //               placement="bottomRight"
    //               content={content}
    //               title="Bộ lọc"
    //               trigger="click"
    //               overlayStyle={{ width: "800px" }}
    //             >
    //               <Button className=" bg-blue-500 text-white text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center hidden">
    //                 <AppstoreOutlined className="text-[20px] text-white " /> Bộ
    //                 lọc
    //               </Button>
    //             </Popover> */}

    //             <Button
    //               onClick={handleReload}
    //               className="ml-[16px] bg-white text-blue-500 border-blue-500 text-[14px]  rounded-lg h-[36px]  flex items-center"
    //             >
    //               <SyncOutlined className="text-[20px] text-blue-500 " /> Làm
    //               mới
    //             </Button>
    //           </div>
    //         </div>
    //         <div>
    //           <Button
    //             onClick={hanldeOpenCreate}
    //             className=" bg-blue-500 text-white text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center"
    //           >
    //             <PlusOutlined className="text-[14px] text-white " />
    //             Tạo mới
    //           </Button>
    //         </div>
    //       </div>
    //       <div>
    //         <Tabs
    //           defaultActiveKey="1"
    //           items={items}
    //           className="mt-8"
    //           onChange={onChangeTab}
    //         />
    //       </div>
    //     </div>
    //   </LayoutCpn>
    // </>

    <>
      {contextHolder}
      <Modalcpn DataModals={DataModals} />
      <LayoutCpn>
        <div>
          <div className="flex items-center justify-between">
            <TitlePage>Danh sách Account </TitlePage>
            <div className="flex items-center">
              <Button
                onClick={hanldeOpenCreate}
                type="primary"
                className=" hover:!text-white text-[12px]   rounded-md mx-6 flex items-center"
              >
                <PlusOutlined className="text-[14px] hover:!text-white" />
                Thêm
              </Button>

              <div
                onClick={handleReload}
                className="mx-6 text-sm cursor-pointer	flex items-center"
              >
                <ReloadOutlined className="mr-2" /> Làm mới
              </div>
              <div
                onClick={handleDownloadexam}
                className="mx-6 text-sm cursor-pointer	flex items-center"
              >
                <DownloadOutlined className="mr-2" /> Tải mẫu
              </div>
              <div
                className="mx-6 text-sm cursor-pointer	"
                onClick={showModalupload}
              >
                Nhập dữ liệu
              </div>
              <div className="mx-6 text-sm cursor-pointer	">Xuất dữ liệu</div>

              {/* <Button className="bg-white text-black text-[14px]  rounded-lg h-[36px] ml-[4px] flex items-center">
          <PlusOutlined className="text-[14px] text-black " /> Ghi âm
        </Button> */}
            </div>
          </div>
          {/* <Button type="primary" className="bg-blue-500" onClick={showModal}>
          Open Modal
        </Button> */}

          <div className="flex items-center justify-start">
            <div className="w-full mt-4">
              {modalStatus == ModalStatus.CREATE ||
              modalStatus == ModalStatus.EDIT ? null : (
                <FromFilter
                  accountInfo={accountInfo}
                  handleResetFilter={handleResetFilter}
                  form={form}
                  fieldFilterDefault={fieldFilterDefault}
                  fieldFilterMore={fieldFilterMore}
                />
              )}
            </div>
            <div></div>
          </div>
          <div>
            <Tabs
              defaultActiveKey="1"
              items={items}
              className="mt-8"
              onChange={onChangeTab}
            />
          </div>
        </div>
      </LayoutCpn>

      <Modal
        footer={null}
        width={1000}
        title="Import "
        open={isModalOpenUpload}
        onOk={handleOk}
        onCancel={handleCancelUpload}
      >
        <div className="mt-[20px]">
          <Dragger {...propsupload}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">
              <span className="text-blue-500">Click vào đây</span> để chọn file
            </p>
            <p className="ant-upload-hint">
              {/* (Hỗ trợ định dạng file WAV và MP3. Tối đa 10 files và dung lượng
        mỗi file không quá 10MB) */}
            </p>
          </Dragger>
          {/* <TableDefault columns={columns} dataSource={dataSource} /> */}
          <div className="mt-[20px] flex items-center justify-end">
            <Button className="bg-blue-500 text-white">Upload All</Button>
            <Button type="primary" className="ml-[12px]" danger>
              Remove all
            </Button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default TemplateAccount_public;
