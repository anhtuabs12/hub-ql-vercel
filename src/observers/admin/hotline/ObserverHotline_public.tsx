import { FormFilterFileHotline } from "@/components/forms/FieldList";
import FormRender from "@/components/forms/FormRender";
import {
  CREATE_TYPE_MODAL_HOTLINE,
  DEMO_TYPE_MODAL,
  EDIT_TYPE_MODAL_HOTLINE,
  TYPE_MODAL_FORM,
  TYPE_MODAL_HOTLINE,
  TYPE_MODAL_HOTLINE_ACTION,
  TYPE_MODAL_HOTLINE_LOG,
} from "@/components/modals/types";
import { ModalStatus } from "@/components/tables/enums";
import { ColunmHotline, ColunmUploadFiles } from "@/components/tables/field";
import { message, UploadProps } from "antd";
import React from "react";
import {
  hotlineCancelRequest,
  hotlineCreateRequest,
  hotlineDoingRequest,
  hotlineEvictingRequest,
  hotlineImportRequest,
  hotlineKeepingRequest,
  hotlineListRequest,
  hotlineReadyRequest,
  hotlineUpdateRequest,
  hotlineWaitingBrandnameRequest,
} from "../../../../redux/actions/hotlineAction";
import { openNotificationWithIcon } from "../../../../constants/notification";
import { NotificationType } from "@/pages/login";
import { apiUrl } from "../../../../configs/urls";
import {
  FilterHotlineSort,
  FilterHotlneMore,
} from "@/components/filters/Field";
import {
  HOTLINE_STATUS,
  NETWORK_TYPE,
  ROLE_TYPE,
  SORTDATE_TYPE,
  TICKET_TYPE,
} from "../../../../constants/enum";
import moment from "moment";
import { FindbyCodeHotline } from "../../../../apiRoutes/hotline";
import { ticketCreateRequest } from "../../../../redux/actions/ticketAction";
import dayjs from "dayjs";
import Logpage from "@/components/logpage";
import { timeDefaultKeeping } from "../../../../configs/dataConfig";
import { handleExportExcel } from "../../../../constants/excell/excell";
import { ColunmHotlineExcell } from "../../../../constants/excell/column";
type Props = {};

const ObserverHotline_public = (init: any) => {
  const {
    customerUnSip,
    changModalType,
    setChangeModalType,
    accountInfo,
    window,
    filterData,
    setFilterData,
    loading,
    total,
    contextHolder,
    api,
    dispatch,
    setSelectedRows,
    selectedRows,
    form,
    setIsModalOpen,
    isModalOpen,
    setModalStatus,
    modalStatus,
    setDataSource,
    dataSource,
    page,
    setPage,
    fieldFormsList,
    setFieldFormsList,
    editorId,
    setEditorId,
    setIsModalOpenUpload,
    isModalOpenUpload,
  } = init;

  const fetchData = async () => {
    dispatch(
      hotlineListRequest({
        offset: (page - 1) * 10,
        limit: 10,
        ...filterData,
        accountInfoRole: accountInfo?.Role,
      })
    );
  };
  const HandleEdit = async (code: any) => {
    setEditorId(code);
    let data = await FindbyCodeHotline(code);
    form.setFieldsValue({
      Type_NetWork: data.data.Type_NetWork,
      Hotline: data.data.Hotline,
      PurchaseDate: data.data.PurchaseDate
        ? dayjs(data.data.PurchaseDate)
        : null,
      Status: data.data.Status,
      CustomerCode: data.data.CustomerCode,
      Lunch_date: data.data.Lunch_date ? dayjs(data.data.Lunch_date) : null,
      BrandName: data.data.BrandName,
      Extention_periodDate: data.data.Extention_periodDate
        ? Number(
            dayjs(data.data.Extention_periodDate).diff(
              dayjs(data.data.Updated_at),
              "hour"
            )
          ) + 1
        : null,
      Description: data.data.Description,
    });
    setModalStatus(ModalStatus.EDIT);
    showModal();
  };

  const handleShowLog = (Hotline: string) => {
    setEditorId(Hotline);
    setModalStatus(ModalStatus.HOTLINE_LOG);
    form.resetFields();
    showModal();
  };
  const dataColunms = {
    handleShowLog: handleShowLog,
    deletedAction: true,
    accountInfo,
    HandleEdit: HandleEdit,
    HandleDelete: async (id: any) => {},
  };
  const onFinish = async (values: any) => {
    switch (modalStatus) {
      case ModalStatus.CREATE:
        const currentDate = moment();
        if (values.Extention_periodDate) {
          const extendedDate = currentDate.add(
            Number(values.Extention_periodDate),
            "hours"
          );
          values.Extention_periodDate = extendedDate.toDate();
        }

        dispatch(
          hotlineCreateRequest({
            data: values,
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Tạo mới thành công",
                ""
              );
              setSelectedRows([]);

              handleOk();
            },
          })
        );
        break;
      case ModalStatus.ACTION_HOTLINE_WAITING_BRANDNAME:
        values.hotline_codes = selectedRows.map((value: any) => value.Code);
        dispatch(
          hotlineWaitingBrandnameRequest({
            data: values,
            callbackError: (error: string) => {
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Cập nhật thành công",
                ""
              );
              setSelectedRows([]);
              handleOk();
            },
          })
        );
        break;

      case ModalStatus.ACTION_HOTLINE_KEEPING:
        const currentDates = moment();
        if (values.Extention_periodDate) {
          const extendedDate = currentDates.add(
            Number(values.Extention_periodDate),
            "hours"
          );
          values.Extention_periodDate = extendedDate.toDate();
        }
        let codeList = selectedRows.map((value: any) => value.Code);
        dispatch(
          hotlineKeepingRequest({
            data: { hotline_codes: codeList, ...values },
            callbackError: (error: string) => {
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Giữ số thành công",
                ""
              );
              setSelectedRows([]);
              handleOk();
            },
          })
        );
        break;

      case ModalStatus.ACTION_HOTLINE_DOING:
        values.hotline_codes = selectedRows.map((value: any) => value.Code);
        dispatch(
          hotlineDoingRequest({
            data: values,
            callbackError: (error: string) => {
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Giữ số thành công",
                ""
              );
              setSelectedRows([]);

              handleOk();
            },
          })
        );
        break;
      case ModalStatus.EDIT:
        const currentDatess = moment();
        if (values.Extention_periodDate) {
          const extendedDate = currentDatess.add(
            Number(values.Extention_periodDate),
            "hours"
          );
          values.Extention_periodDate = extendedDate.toDate();
        }

        dispatch(
          hotlineUpdateRequest({
            data: {
              code: editorId,
              data: values,
            },
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Tạo mới thành công",
                ""
              );
              handleOk();
            },
          })
        );
      default:
        break;
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log(errorInfo, "---value da nhan o ngoai");
  };

  const initialValues = {
    mausac: "#0079FF",
  };

  const Datas = {
    customerUnSip,
    modalType: modalStatus,
    dispatch: dispatch,
    submitBtn: {
      title: (
        <>
          <div className="font-semibold  ">
            {modalStatus == ModalStatus.CREATE ? "Thêm" : "Lưu"}
          </div>
        </>
      ),
      offset: 18,
      span: 6,
      classname: "h-[36px] !rounded-lg",
    },
    initialValues,
    onFinish,
    onFinishFailed,
    selectDisable:
      modalStatus == ModalStatus.ACTION_HOTLINE_DOING ||
      modalStatus == ModalStatus.ACTION_HOTLINE_WAITING_BRANDNAME ||
      modalStatus == ModalStatus.ACTION_HOTLINE_KEEPING,
    form,
  };

  // let fieldListFilter = FormFilterFileHotline(Datas as any);

  const handleOk = () => {
    setModalStatus(null);
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setModalStatus(null);
    setIsModalOpen(false);
  };

  const TypeModalRender = (modalStatus: ModalStatus | null) => {
    switch (modalStatus) {
      case ModalStatus.ACTION_HOTLINE_DOING:
      case ModalStatus.ACTION_HOTLINE_WAITING_BRANDNAME:
      case ModalStatus.ACTION_HOTLINE_KEEPING:
        return TYPE_MODAL_HOTLINE_ACTION;

      case ModalStatus.EDIT:
      case ModalStatus.CREATE:
        return TYPE_MODAL_HOTLINE;
      case ModalStatus.HOTLINE_LOG:
        return TYPE_MODAL_HOTLINE_LOG;

      default:
      // return TYPE_MODAL_HOTLINE;
    }
  };

  const DataModals = {
    Render: {
      renderStatus: modalStatus == ModalStatus.HOTLINE_LOG,
      content: <Logpage Hotline={editorId} />,
    },
    customerUnSip,
    width: 800,
    form: form,
    title: modalStatus == ModalStatus.CREATE ? "Tạo mới" : "Chỉnh sửa",
    isModalOpen,
    handleOk,
    handleCancel,
    type: TypeModalRender(modalStatus),
    dataModals: Datas,
    dispatch: dispatch,
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const hanldeOpenCreate = () => {
    setModalStatus(ModalStatus.CREATE);
    form.resetFields();
    form.setFieldValue("PurchaseDate", dayjs(new Date()));
    form.setFieldValue("Lunch_date", dayjs(new Date()));
    form.setFieldValue("Extention_periodDate", timeDefaultKeeping);
    form.setFieldValue("Status", HOTLINE_STATUS.READY);
    form.setFieldValue("Type_NetWork", NETWORK_TYPE.VIETTEL);
    showModal();
  };

  const hanldeOpenAction = (Type: any) => {
    form.resetFields();
    switch (Type) {
      case TICKET_TYPE.DOING:
        setModalStatus(ModalStatus.ACTION_HOTLINE_DOING);
        form.setFieldsValue({ Status: HOTLINE_STATUS.DOING });
        form.setFieldValue("Lunch_date", dayjs(new Date()));
        break;
      case TICKET_TYPE.WAITING_BRANDNAME:
        setModalStatus(ModalStatus.ACTION_HOTLINE_WAITING_BRANDNAME);
        form.setFieldsValue({ Status: HOTLINE_STATUS.WAITINGBRANDNAME });
        break;
      case TICKET_TYPE.KEEPING:
        setModalStatus(ModalStatus.ACTION_HOTLINE_KEEPING);
        form.setFieldsValue({ Status: HOTLINE_STATUS.KEEPING });
        form.setFieldValue("Extention_periodDate", timeDefaultKeeping);
        break;

      default:
        form.resetFields();
        break;
    }

    showModal();
  };

  const columns = ColunmHotline(dataColunms) as any;

  // const content = (
  //   <>
  //     <div className="w-[400px] min-h-[300px]">
  //       <FormRender FieldList={fieldListFilter} form={form} />
  //     </div>
  //   </>
  // );

  const gettoken = window?.localStorage?.getItem("hub_token");

  const propsupload: UploadProps = {
    name: "file",
    multiple: false,
    action: apiUrl + "/importhotline",
    headers: {
      Authorization: "Bearer " + gettoken,
    },
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
  };

  const columnsUpload = ColunmUploadFiles() as any;

  const showModalupload = () => {
    setIsModalOpenUpload(true);
  };
  const handleCancelUpload = () => {
    setIsModalOpenUpload(false);
  };

  const onChangeSelection = () => {};

  const rowSelectionDatas = {
    setSelectedRows,
    selectedRows,
    onChangeSelection,
  };

  const handleReload = () => {
    dispatch(hotlineListRequest({ offset: (1 - 1) * 10, limit: 10 }));
  };

  const handleResetFilter = () => {
    setFilterData({});
    dispatch(hotlineListRequest({ offset: (1 - 1) * 10, limit: 10 }));
  };

  const handleDownloadexam = () => {
    const fileUrl = "/ex.xlsx";
    const anchor = document.createElement("a");
    anchor.href = fileUrl;
    anchor.download = "ex.xlsx";
    anchor.click();
  };

  let FilterHotlineSort_data = {
    accountInfo,
    submitBtn: {
      classname: "hidden",
    },
    onFinishFailed: () => {},
    onFinish: async () => {},
    onchange: (key: any, value: any, data: any) => {
      const today = moment();
      if (data.Times == SORTDATE_TYPE.DAY) {
        data.from_date = today.clone().startOf("day");
        data.to_date = today.clone().endOf("day");
      }
      if (data.Times == SORTDATE_TYPE.WEEK) {
        data.from_date = today.clone().startOf("week");
        data.to_date = today.clone().endOf("week");
      }
      if (data.Times == SORTDATE_TYPE.MONTH) {
        data.from_date = today.clone().startOf("month");
        data.to_date = today.clone().endOf("month");
      }
      setFilterData(data);
      dispatch(
        hotlineListRequest({ offset: (1 - 1) * 10, limit: 10, ...data })
      );
    },
    form: form,
  };

  let FilterHotlneMore_data = {
    submitBtn: {
      classname: "",
      title: "Lọc",
      span: 4,
      offset: 20,
    },
    onFinishFailed: () => {},
    onFinish: (values: any) => {
      setFilterData(values);
      dispatch(
        hotlineListRequest({ offset: (1 - 1) * 10, limit: 10, ...values })
      );
      console.log(values, "filtermore");
    },
    form: form,
  };
  const fieldFilterDefault = FilterHotlineSort(FilterHotlineSort_data);

  const fieldFilterMore = FilterHotlneMore(FilterHotlneMore_data);

  const handleKeepingHotline = () => {
    switch (accountInfo.Role) {
      case ROLE_TYPE.CUSTOMER:
        let codeList = selectedRows.map((value: any) => value.Code);
        dispatch(
          hotlineKeepingRequest({
            data: { hotline_codes: codeList },
            callbackError: (error: string) => {
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Giữ số thành công",
                ""
              );
              setSelectedRows([]);
            },
          })
        );
        break;
      case ROLE_TYPE.ADMINISTRATOR:
      case ROLE_TYPE.SALEADMIN:
        hanldeOpenAction(TICKET_TYPE.KEEPING);

      default:
        break;
    }
  };

  const handleActionForAdmin = (Type: any, value: any) => {
    switch (Type) {
      case TICKET_TYPE.WITHDRAWAL:
        dispatch(
          hotlineEvictingRequest({
            data: value,
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Đã thu hồi thành công",
                ""
              );
              setSelectedRows([]);
              handleOk();
            },
          })
        );
        break;

      case TICKET_TYPE.UNUSED:
        dispatch(
          hotlineReadyRequest({
            data: value,
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Đã thu hồi thành công",
                ""
              );
              setSelectedRows([]);
              handleOk();
            },
          })
        );
        break;

      case TICKET_TYPE.CANCEL_NUMBER:
        dispatch(
          hotlineCancelRequest({
            data: value,
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Đã thu hồi thành công",
                ""
              );
              setSelectedRows([]);
              handleOk();
            },
          })
        );
        break;

      case TICKET_TYPE.DOING:
        // setChangeModalType(TICKET_TYPE.DOING);
        hanldeOpenAction(TICKET_TYPE.DOING);
        break;
      case TICKET_TYPE.WAITING_BRANDNAME:
        // setChangeModalType(TICKET_TYPE.WAITING_BRANDNAME);
        hanldeOpenAction(TICKET_TYPE.WAITING_BRANDNAME);
        break;
      default:
        break;
    }
  };

  const handleActions = (type: any) => {
    switch (accountInfo.Role) {
      case ROLE_TYPE.SALEADMIN:
        let values_for_SALEADMIN = {
          Type: type,
          Hotlines_codes: selectedRows.map((value: any) => value.Hotline),
          Status: 1,
          Ticket_from: 1,
          Description: "",
        };
        dispatch(
          ticketCreateRequest({
            data: values_for_SALEADMIN,
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Tạo ticket thành công",
                ""
              );
              handleOk();
            },
          })
        );
        break;

      case ROLE_TYPE.ADMINISTRATOR:
        let values_for_ADMINISTRATOR = {
          hotline_codes: selectedRows.map((value: any) => value.Code),
        };
        handleActionForAdmin(type, values_for_ADMINISTRATOR);
        break;

      default:
        break;
    }
  };

  const cancelPopconfirm = (e: any) => {
    console.log(e);
    message.error("Click on No");
  };

  const exportExcell = () => {
    handleExportExcel(ColunmHotlineExcell(dataColunms), dataSource);
  };
  return {
    exportExcell,
    cancelPopconfirm,
    modalStatus,
    handleActions,
    // handelCancelNumber,
    handleKeepingHotline,
    accountInfo,
    handleResetFilter,
    fieldFilterDefault,
    fieldFilterMore,
    form,
    handleDownloadexam,
    handleReload,
    setPage,
    loading,
    total,
    contextHolder,
    rowSelectionDatas,
    handleCancelUpload,
    showModalupload,
    setIsModalOpenUpload,
    isModalOpenUpload,
    columnsUpload,
    propsupload,
    // content,
    columns,
    dataSource,
    isModalOpen,
    showModal,
    handleOk,
    handleCancel,
    DataModals,
    fetchData,
    hanldeOpenCreate,
  };
};

export default ObserverHotline_public;
