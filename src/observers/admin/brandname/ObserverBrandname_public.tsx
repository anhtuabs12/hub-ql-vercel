import { FormFilterFileAccount } from "@/components/forms/FieldList";
import FormRender from "@/components/forms/FormRender";
import {
  DEMO_TYPE_MODAL,
  TYPE_MODAL_ACCOUNT,
  TYPE_MODAL_BRANDNAME,
  TYPE_MODAL_FORM,
} from "@/components/modals/types";
import { ModalStatus } from "@/components/tables/enums";
import {
  ColunmAccount,
  ColunmBrandname,
  ColunmTicket,
  ColunmUploadFiles,
} from "@/components/tables/field";
import {
  Button,
  message,
  Popconfirm,
  Radio,
  Space,
  Spin,
  UploadProps,
} from "antd";
import React from "react";
import {
  accountCreateRequest,
  accountDeleteRequest,
  accountListRequest,
  accountUpdateRequest,
} from "../../../../redux/actions/accountAction";
import { FindbycodeAccount } from "../../../../apiRoutes/acount";
import { openNotificationWithIcon } from "../../../../constants/notification";
import { NotificationType } from "@/pages/login";
import {
  ticketDeleteRequest,
  ticketListRequest,
  ticketUpdateRequest,
} from "../../../../redux/actions/ticketAction";
import { findByCodeTicket } from "../../../../apiRoutes/ticket";
import { SORTDATE_TYPE, TICKETSTATUS_TYPE } from "../../../../constants/enum";
import {
  FilterBrandnameMore,
  FilterBrandnameSort,
  FilterTicketMore,
  FilterTicketSort,
} from "@/components/filters/Field";
import moment from "moment";
import {
  brandnameCreateRequest,
  brandnameDeleteRequest,
  brandnameListRequest,
  brandnameUpdateRequest,
} from "../../../../redux/actions/brandnameAction";
import { findByCodeBrandname } from "../../../../apiRoutes/brandname";
type Props = {};

const ObserverBrandname_public = (init: any) => {
  const {
    filterData,
    setFilterData,
    setSelectedRows,
    selectedRows,
    setIsModalOpenUpload,
    isModalOpenUpload,
    contextHolder,
    api,
    form,
    setIsModalOpen,
    isModalOpen,
    setModalStatus,
    modalStatus,
    brandnameList,
    accountInfo,
    total,
    setDataSource,
    // dataSource,
    page,
    setPage,
    fieldFormsList,
    setFieldFormsList,
    editorId,
    setEditorId,
    dispatch,
    loading,
  } = init;

  const fetchData = async () => {
    try {
      dispatch(
        brandnameListRequest({
          data: { offset: (page - 1) * 10, limit: 10, ...filterData },
        })
      );
    } catch (error) {}
  };

  const dataSource = brandnameList?.map((value: any, index: any) => {
    value.key = index;
    return value;
  });

  const HandleEdit = async (code: any) => {
    console.log(code, "code");
    let data = await findByCodeBrandname(code);
    console.log(data, "dataaaaaa");
    form.setFieldsValue({
      CheckBrandnameToHotline: data.data.checkBrandnameToHotline,
      Brandname: data.data.checkBrandname.Brandname,
      Certificate_number: data.data.checkBrandname.Certificate_number,
      Enterprise_number_company:
        data.data.checkBrandname.Enterprise_number_company,
      Lunch_date: data.data.checkBrandname.Lunch_date
        ? moment(data.data.checkBrandname.Lunch_date, "YYYY-MM-DDTHH:mm")
        : null,
      Expiry_date: data.data.checkBrandname.Expiry_date
        ? moment(data.data.checkBrandname.Expiry_date, "YYYY-MM-DDTHH:mm")
        : null,
      Note: data.data.checkBrandname.Note,
      Status: data.data.checkBrandname.Status,
    });

    console.log(data, "data");
    setModalStatus(ModalStatus.EDIT);
    showModal();
  };
  const dataColunms = {
    accountInfo: accountInfo,
    HandleEdit: HandleEdit,
    HandleDelete: async (code: any) => {
      dispatch(
        brandnameDeleteRequest({
          data: code,
          callbackError: (error: string) => {
            console.log(error, "error");
            openNotificationWithIcon(api, NotificationType.ERROR, error, "");
          },
          callbackSuccess: () => {
            openNotificationWithIcon(
              api,
              NotificationType.SUCCESS,
              "Xóa thành công",
              ""
            );
            handleOk();
          },
        })
      );
    },
  };
  const onFinish = async (values: any) => {
    switch (modalStatus) {
      case ModalStatus.CREATE:
        dispatch(
          brandnameCreateRequest({
            data: values,
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Tạo mới thành công",
                ""
              );
              handleOk();
            },
          })
        );
        break;
      case ModalStatus.EDIT:
        dispatch(
          brandnameUpdateRequest({
            data: values,
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(
                api,
                NotificationType.SUCCESS,
                "Sửa mới thành công",
                ""
              );
              handleOk();
            },
          })
        );
        break;

      default:
        break;
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    // console.log(errorInfo, "---value da nhan o ngoai");
  };

  const initialValues = {
    mausac: "#0079FF",
  };

  const Datas = {
    submitBtn: {
      title: (
        <>
          <div className="font-semibold  ">
            {loading ? (
              <span className="mr-4">
                <Spin></Spin>
              </span>
            ) : (
              ""
            )}
            {modalStatus == ModalStatus.CREATE ? "Thêm" : "Lưu"}
          </div>
        </>
      ),
      disable: loading,
      offset: 18,
      span: 6,
      classname: "h-[36px] !rounded-lg",
    },
    initialValues,
    onFinish,
    onFinishFailed,
    form,
    hidden: modalStatus == !ModalStatus.CREATE ? false : true,
    disable: modalStatus == ModalStatus.CREATE ? false : true,
  };

  // let fieldListFilter = FormFilterFileAccount(Datas as any);

  const handleOk = () => {
    setIsModalOpen(false);
    setModalStatus(null);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setModalStatus(null);
  };

  const DataModals = {
    width: 600,
    form: form,
    title: modalStatus == ModalStatus.CREATE ? "Tạo mới" : "Chỉnh sửa",
    isModalOpen,
    handleOk,
    handleCancel,
    type: TYPE_MODAL_BRANDNAME,
    dataModals: Datas,
  };
  const showModal = () => {
    setIsModalOpen(true);
  };

  const hanldeOpenCreate = () => {
    setModalStatus(ModalStatus.CREATE);
    form.resetFields();
    showModal();
  };

  const columns = ColunmBrandname(dataColunms) as any;

  // const content = (
  //   <>
  //     <div className="w-[400px] min-h-[300px]">
  //       <FormRender FieldList={fieldListFilter} form={form} />
  //     </div>
  //   </>
  // );

  const propsupload: UploadProps = {
    name: "file",
    multiple: true,
    action: "https://660d2bd96ddfa2943b33731c.mockapi.io/api/upload",
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        // console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      // console.log("Dropped files", e.dataTransfer.files);
    },
  };

  const columnsUpload = ColunmUploadFiles() as any;
  const showModalupload = () => {
    setIsModalOpenUpload(true);
  };
  const handleCancelUpload = () => {
    setIsModalOpenUpload(false);
  };

  const onChangeSelection = () => {};

  const rowSelectionDatas = {
    setSelectedRows,
    selectedRows,
    onChangeSelection,
  };
  const handleReload = () => {
    // dispatch(ticketListRequest({ offset: 0, limit: 10 }));
    dispatch(
      brandnameListRequest({
        data: { offset: (1 - 1) * 10, limit: 10 },
      })
    );
  };

  const handleResetFilter = () => {
    // dispatch(ticketListRequest({ offset: 0, limit: 10 }));
    dispatch(
      brandnameListRequest({
        data: { offset: (1 - 1) * 10, limit: 10 },
      })
    );
  };

  const handleDownloadexam = () => {
    const fileUrl = "/ex.xlsx";
    const anchor = document.createElement("a");
    anchor.href = fileUrl;
    anchor.download = "ex.xlsx";
    anchor.click();
  };
  // console.log(form.getFieldsValue(), "getFieldsValue");

  let FilterBrandnameSort_data = {
    accountInfo,
    submitBtn: {
      classname: "hidden",
    },
    onFinishFailed: () => {},
    onFinish: () => {},
    onchange: (key: any, value: any, data: any) => {
      console.log(data, "dâttatatataat");
      const today = moment();
      if (data.Times == SORTDATE_TYPE.DAY) {
        data.from_date = today.clone().startOf("day");
        data.to_date = today.clone().endOf("day");
      }
      if (data.Times == SORTDATE_TYPE.WEEK) {
        data.from_date = today.clone().startOf("week");
        data.to_date = today.clone().endOf("week");
      }
      if (data.Times == SORTDATE_TYPE.MONTH) {
        data.from_date = today.clone().startOf("month");
        data.to_date = today.clone().endOf("month");
      }
      setFilterData(data);
      // dispatch(ticketListRequest({ offset: (1 - 1) * 10, limit: 10, ...data }));
      dispatch(
        brandnameListRequest({
          data: { offset: (1 - 1) * 10, limit: 10, ...data },
        })
      );
    },
    form: form,
  };

  let FilterBrandnameMore_data = {
    submitBtn: {
      classname: "",
      title: "Lọc",
      span: 4,
      offset: 20,
    },
    onFinishFailed: () => {},
    
    onFinish: (values: any) => {
      setFilterData(values);
      // dispatch(
      //   ticketListRequest({ offset: (1 - 1) * 10, limit: 10, ...values })
      // );
      dispatch(
        brandnameListRequest({
          data: { offset: (1 - 1) * 10, limit: 10, ...values },
        })
      );
      // console.log(values, "filtermore");
    },
    form: form,
  };
  const fieldFilterDefault = FilterBrandnameSort(FilterBrandnameSort_data);

  const fieldFilterMore = FilterBrandnameMore(FilterBrandnameMore_data);

  return {
    modalStatus,
    form,
    accountInfo,
    handleResetFilter,
    fieldFilterDefault,
    fieldFilterMore,
    handleDownloadexam,
    handleReload,
    rowSelectionDatas,
    handleCancelUpload,
    showModalupload,
    setIsModalOpenUpload,
    isModalOpenUpload,
    columnsUpload,
    propsupload,
    // content,
    columns,
    dataSource,
    isModalOpen,
    showModal,
    handleOk,
    handleCancel,
    DataModals,
    fetchData,
    hanldeOpenCreate,
    page,
    setPage,
    total,
    loading,
    contextHolder,
  };
};

export default ObserverBrandname_public;
