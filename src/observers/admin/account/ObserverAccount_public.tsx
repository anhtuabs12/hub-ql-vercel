import { FormFilterFileAccount } from "@/components/forms/FieldList";
import FormRender from "@/components/forms/FormRender";
import {
  DEMO_TYPE_MODAL,
  TYPE_MODAL_ACCOUNT,
  TYPE_MODAL_FORM,
} from "@/components/modals/types";
import { ModalStatus } from "@/components/tables/enums";
import { ColumnsAccountCustomer, ColunmAccount, ColunmUploadFiles } from "@/components/tables/field";
import { message, Spin, UploadProps } from "antd";
import React from "react";
import {
  accountCreateRequest,
  accountDeleteRequest,
  accountListRequest,
  accountUpdateRequest,
} from "../../../../redux/actions/accountAction";
import { FindbycodeAccount } from "../../../../apiRoutes/acount";
import { openNotificationWithIcon } from "../../../../constants/notification";
import { NotificationType } from "@/pages/login";
import { ROLE_TYPE, SORTDATE_TYPE } from "../../../../constants/enum";
import moment from "moment";
import { FilterAccountSort } from "@/components/filters/Field";
type Props = {};

const ObserverAccount_public = (init: any) => {
  const {
    setIsModalOpenUpload,
    isModalOpenUpload,
    setSelectedRows,
    selectedRows,
    accountInfo,
    contextHolder,
    api,
    setFilterData,
    filterData,
    form,
    setIsModalOpen,
    isModalOpen,
    setModalStatus,
    modalStatus,
    accountList,
    total,
    setDataSource,
    dataSource,
    page,
    setPage,
    fieldFormsList,
    setFieldFormsList,
    editorId,
    setEditorId,
    dispatch,
    loading,
  } = init;

  const fetchData = async () => {
    console.log(filterData, "filterData")
    dispatch(accountListRequest({ offset: (page - 1) * 10, limit: 10 , ...filterData}));
  };

  const handleReload = () => {
    dispatch(accountListRequest({ offset: 0, limit: 10 }));
  };

  const HandleEdit = async (code: any) => {
    setEditorId(code);
    let dataEdit = await FindbycodeAccount(code);
    form.setFieldsValue({
      FullName: dataEdit.data.info.FullName,
      Email: dataEdit.data.info.Email,
      enterprise_number: dataEdit.data.info.enterprise_number,
      companyname: dataEdit.data.info.companyname,
      Role: dataEdit.data.info.Role,
    });
    setModalStatus(ModalStatus.EDIT);
    showModal();
  };
  const dataColunms = {
    modalStatus,
    HandleEdit: HandleEdit,
    HandleDelete: async (code: any) => {
      dispatch(accountDeleteRequest({ data: code }));
    },
  };
  const onFinish = async (values: any) => {
    if (modalStatus == ModalStatus.CREATE) {
      dispatch(
        accountCreateRequest({
          data: values,
          callbackError: (error: string) => {
            console.log(error, "error");
            openNotificationWithIcon(api, NotificationType.ERROR, error, "");
          },
          filterData,
          callbackSuccess: () => {
            openNotificationWithIcon(
              api,
              NotificationType.SUCCESS,
              "Tạo mới thành công",
              ""
            );
            handleOk();
          },
        })
      );
    } else {
      dispatch(
        accountUpdateRequest({
          data: {
            code: editorId,
            data: values,
          },
          filterData,
          callbackSuccess: () => {
            handleOk();
          },
        })
      );
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    // console.log(errorInfo, "---value da nhan o ngoai");
  };

  const initialValues = {
    mausac: "#0079FF",
  };

  const Datas = {
    modalStatus,
    submitBtn: {
      title: (
        <>
          <div className="font-semibold  ">
            {loading ? (
              <span className="mr-4">
                <Spin></Spin>
              </span>
            ) : (
              ""
            )}
            {modalStatus == ModalStatus.CREATE ? "Thêm" : "Lưu"}
          </div>
        </>
      ),
      disable: loading,
      offset: 18,
      span: 6,
      classname: "h-[36px] !rounded-lg",
    },
    initialValues,
    onFinish,
    onFinishFailed,
    form,
    hidden: modalStatus == !ModalStatus.CREATE ? false : true,
    disable: modalStatus == ModalStatus.CREATE ? false : true,
  };

  // let fieldListFilter = FormFilterFileAccount(Datas as any);

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const DataModals = {
    modalStatus,
    width: 600,
    form: form,
    title: modalStatus == ModalStatus.CREATE ? "Tạo mới" : "Chỉnh sửa",
    isModalOpen,
    handleOk,
    handleCancel,
    type: TYPE_MODAL_ACCOUNT,
    dataModals: Datas,
  };
  const showModal = () => {
    setIsModalOpen(true);
  };

  const hanldeOpenCreate = () => {
    setModalStatus(ModalStatus.CREATE);
    form.resetFields();
    showModal();
  };

  const columns = ColunmAccount(dataColunms) as any;

  const columnsCustomer = ColumnsAccountCustomer(dataColunms) as any

  // const content = (
  //   <>
  //     <div className="w-[400px] min-h-[300px]">
  //       <FormRender FieldList={fieldListFilter} form={form} />
  //     </div>
  //   </>
  // );

  const propsupload: UploadProps = {
    name: "file",
    multiple: true,
    action: "https://660d2bd96ddfa2943b33731c.mockapi.io/api/upload",
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
  };

  const columnsUpload = ColunmUploadFiles() as any;

  const onChangeTab = (key: any) => {
    let payloadRole = [] as any
    if (key == "staff") {
      payloadRole = [ROLE_TYPE.ADMINISTRATOR, ROLE_TYPE.SALEADMIN];
    }
    if (key == "customer") {
      payloadRole = [ROLE_TYPE.CUSTOMER];
    }
    setFilterData({ ...filterData, Role: payloadRole });
  };

  const handleDownloadexam = () => {
    const fileUrl = "/ex.xlsx";
    const anchor = document.createElement("a");
    anchor.href = fileUrl;
    anchor.download = "ex.xlsx";
    anchor.click();
  };

  const handleResetFilter = () => {
    // dispatch(ticketListRequest({ offset: 0, limit: 10 }));
    dispatch(accountListRequest({ offset: (page - 1) * 10, limit: 10}));
  };
  let FilterAccountSort_data = {
    accountInfo,
    submitBtn: {
      classname: "hidden",
    },
    onFinishFailed: () => {},
    onFinish: () => {},
    onchange: (key: any, value: any, data: any) => {
      console.log(data, "dâttatatataat");
      const today = moment();
      if (data.Times == SORTDATE_TYPE.DAY) {
        data.from_date = today.clone().startOf("day");
        data.to_date = today.clone().endOf("day");
      }
      if (data.Times == SORTDATE_TYPE.WEEK) {
        data.from_date = today.clone().startOf("week");
        data.to_date = today.clone().endOf("week");
      }
      if (data.Times == SORTDATE_TYPE.MONTH) {
        data.from_date = today.clone().startOf("month");
        data.to_date = today.clone().endOf("month");
      }
      setFilterData(data);
      // dispatch(ticketListRequest({ offset: (1 - 1) * 10, limit: 10, ...data }));
      dispatch(accountListRequest({ offset: (page - 1) * 10, limit: 10, ...data}));
    },
    form: form,
  };
  const fieldFilterDefault = FilterAccountSort(FilterAccountSort_data);
  return {
    modalStatus,
    accountInfo,
    handleResetFilter,
    fieldFilterDefault,
    handleDownloadexam,
    form,
    columnsCustomer,
    onChangeTab,
    handleReload,
    columnsUpload,
    propsupload,
    // content,
    columns,
    dataSource,
    isModalOpen,
    showModal,
    handleOk,
    handleCancel,
    DataModals,
    fetchData,
    hanldeOpenCreate,
    page,
    setPage,
    total,
    loading,
    contextHolder,
  };
};

export default ObserverAccount_public;
