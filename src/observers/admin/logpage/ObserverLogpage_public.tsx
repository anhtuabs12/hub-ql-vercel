import { FormFilterFileAccount } from "@/components/forms/FieldList";
import FormRender from "@/components/forms/FormRender";
import {
  DEMO_TYPE_MODAL,
  TYPE_MODAL_ACCOUNT,
  TYPE_MODAL_FORM,
} from "@/components/modals/types";
import { ModalStatus } from "@/components/tables/enums";
import {
  ColunmAccount,
  ColunmLogpage,
  ColunmTicket,
  ColunmUploadFiles,
} from "@/components/tables/field";
import {
  Button,
  message,
  Popconfirm,
  Radio,
  Space,
  Spin,
  UploadProps,
} from "antd";
import React from "react";
import {
  accountCreateRequest,
  accountDeleteRequest,
  accountListRequest,
  accountUpdateRequest,
} from "../../../../redux/actions/accountAction";
import { FindbycodeAccount } from "../../../../apiRoutes/acount";
import { openNotificationWithIcon } from "../../../../constants/notification";
import { NotificationType } from "@/pages/login";
import {
  ticketListRequest,
  ticketUpdateRequest,
} from "../../../../redux/actions/ticketAction";
import { findByCodeTicket } from "../../../../apiRoutes/ticket";
import { SORTDATE_TYPE, TICKETSTATUS_TYPE } from "../../../../constants/enum";
import {
  FilterlogsSort,
  FilterTicketMore,
  FilterTicketSort,
} from "@/components/filters/Field";
import moment from "moment";
import { logListRequest } from "../../../../redux/actions/logAction";
type Props = {};

const ObserverLogpage_public = (init: any) => {
  const {
    Hotline,
    filterData,
    setFilterData,
    setIsModalOpenUpload,
    isModalOpenUpload,
    contextHolder,
    api,
    form,
    setIsModalOpen,
    isModalOpen,
    setModalStatus,
    modalStatus,
    logList,
    accountInfo,
    total,
    setDataSource,
    // dataSource,
    page,
    setPage,
    fieldFormsList,
    setFieldFormsList,
    editorId,
    setEditorId,
    dispatch,
    loading,
  } = init;

  const fetchData = async (Hotline: any) => {
    if (Hotline) {
      filterData.Hotline = Hotline;
    }

    console.log(
      { offset: (page - 1) * 10, limit: 10, ...filterData },
      "{ offset: (page - 1) * 10, limit: 10, ...filterData }"
    );
    try {
      dispatch(
        logListRequest({ offset: (page - 1) * 10, limit: 10, ...filterData })
      );
    } catch (error) {}
  };

  const dataSource = logList?.map((value: any, index: any) => {
    value.key = index;
    return value;
  });
  const HandleEdit = async (code: any) => {
    dispatch(
      ticketUpdateRequest({
        data: {
          code: code,
          body: { Status: TICKETSTATUS_TYPE.AGREE },
        },
        callbackError: (error: string) => {
          console.log(error, "error");
          openNotificationWithIcon(api, NotificationType.ERROR, error, "");
        },
        callbackSuccess: () => {
          openNotificationWithIcon(
            api,
            NotificationType.SUCCESS,
            "Sửa thành công",
            ""
          );
          handleOk();
        },
      })
    );
  };
  const dataColunms = {
    // actionCustom: 1,
    accountInfo: accountInfo,
    HandleEdit: HandleEdit,
    HandleDelete: async (code: any) => {
      dispatch(
        ticketUpdateRequest({
          data: {
            code: code,
            body: { Status: TICKETSTATUS_TYPE.DISAGREE },
          },
          callbackError: (error: string) => {
            console.log(error, "error");
            openNotificationWithIcon(api, NotificationType.ERROR, error, "");
          },
          callbackSuccess: () => {
            openNotificationWithIcon(
              api,
              NotificationType.SUCCESS,
              "Sửa thành công",
              ""
            );
            handleOk();
          },
        })
      );
    },
  };
  const onFinish = async (values: any) => {
    // if (modalStatus == ModalStatus.CREATE) {
    //   dispatch(
    //     accountCreateRequest({
    //       data: values,
    //       callbackError: (error: string) => {
    //         console.log(error, "error");
    //         openNotificationWithIcon(api, NotificationType.ERROR, error, "");
    //       },
    //       callbackSuccess: () => {
    //         openNotificationWithIcon(
    //           api,
    //           NotificationType.SUCCESS,
    //           "Tạo mới thành công",
    //           ""
    //         );
    //         handleOk();
    //       },
    //     })
    //   );
    // } else {
    //   dispatch(
    //     ticketUpdateRequest({
    //       data: {
    //         code: editorId,
    //         data: values,
    //       },
    //       callbackSuccess: () => {
    //         handleOk();
    //       },
    //     })
    //   );
    // }
  };

  const onFinishFailed = (errorInfo: any) => {
    // console.log(errorInfo, "---value da nhan o ngoai");
  };

  const initialValues = {
    mausac: "#0079FF",
  };

  const Datas = {
    submitBtn: {
      title: (
        <>
          <div className="font-semibold  ">
            {loading ? (
              <span className="mr-4">
                <Spin></Spin>
              </span>
            ) : (
              ""
            )}
            {modalStatus == ModalStatus.CREATE ? "Thêm" : "Lưu"}
          </div>
        </>
      ),
      disable: loading,
      offset: 18,
      span: 6,
      classname: "h-[36px] !rounded-lg",
    },
    initialValues,
    onFinish,
    onFinishFailed,
    form,
    hidden: modalStatus == !ModalStatus.CREATE ? false : true,
    disable: modalStatus == ModalStatus.CREATE ? false : true,
  };

  // let fieldListFilter = FormFilterFileAccount(Datas as any);

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const DataModals = {
    width: modalStatus == ModalStatus.HOTLINE_LOG ? 1000:  600,
    form: form,
    title: modalStatus == ModalStatus.CREATE ? "Tạo mới" : "Chỉnh sửa",
    isModalOpen,
    handleOk,
    handleCancel,
    type: TYPE_MODAL_ACCOUNT,
    dataModals: Datas,
  };
  const showModal = () => {
    setIsModalOpen(true);
  };

  const hanldeOpenCreate = () => {
    setModalStatus(ModalStatus.CREATE);
    form.resetFields();
    showModal();
  };
  const columns = ColunmLogpage(dataColunms) as any;

  // const content = (
  //   <>
  //     <div className="w-[400px] min-h-[300px]">
  //       <FormRender FieldList={fieldListFilter} form={form} />
  //     </div>
  //   </>
  // );

  const propsupload: UploadProps = {
    name: "file",
    multiple: true,
    action: "https://660d2bd96ddfa2943b33731c.mockapi.io/api/upload",
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        // console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      // console.log("Dropped files", e.dataTransfer.files);
    },
  };

  const columnsUpload = ColunmUploadFiles() as any;
  const showModalupload = () => {
    setIsModalOpenUpload(true);
  };
  const handleCancelUpload = () => {
    setIsModalOpenUpload(false);
  };

  const handleReload = () => {
    if (Hotline) {
        setFilterData({ Hotline: Hotline });
        dispatch(logListRequest({ offset: 0, limit: 10, Hotline }));
      } else {
        setFilterData({});
        dispatch(logListRequest({ offset: 0, limit: 10 }));
      }
  };

  const handleResetFilter = () => {
    if (Hotline) {
      setFilterData({ Hotline: Hotline });
      dispatch(logListRequest({ offset: 0, limit: 10, Hotline }));
    } else {
      setFilterData({});
      dispatch(logListRequest({ offset: 0, limit: 10 }));
    }
  };

  const handleDownloadexam = () => {
    const fileUrl = "/ex.xlsx";
    const anchor = document.createElement("a");
    anchor.href = fileUrl;
    anchor.download = "ex.xlsx";
    anchor.click();
  };
  // console.log(form.getFieldsValue(), "getFieldsValue");

  let FilterLogSort_data = {
    Hotline,
    accountInfo,
    submitBtn: {
      classname: "hidden",
    },
    onFinishFailed: () => {},
    onFinish: () => {},
    onchange: (key: any, value: any, data: any) => {
      console.log(data, "dâttatatataat");
      const today = moment();
      if (data.Times == SORTDATE_TYPE.DAY) {
        data.from_date = today.clone().startOf("day");
        data.to_date = today.clone().endOf("day");
      }
      if (data.Times == SORTDATE_TYPE.WEEK) {
        data.from_date = today.clone().startOf("week");
        data.to_date = today.clone().endOf("week");
      }
      if (data.Times == SORTDATE_TYPE.MONTH) {
        data.from_date = today.clone().startOf("month");
        data.to_date = today.clone().endOf("month");
      }
      setFilterData(data);
      dispatch(logListRequest({ offset: (1 - 1) * 10, limit: 10, ...data }));
    },
    form: form,
  };

  let FilterTicketMore_data = {
    submitBtn: {
      classname: "",
      title: "Lọc",
      span: 4,
      offset: 20,
    },
    onFinishFailed: () => {},
    onFinish: (values: any) => {
      setFilterData(values);
      dispatch(logListRequest({ offset: (1 - 1) * 10, limit: 10, ...values }));
      // console.log(values, "filtermore");
    },
    form: form,
  };
  const fieldFilterDefault = FilterlogsSort(FilterLogSort_data);

  const fieldFilterMore = FilterTicketMore(FilterTicketMore_data);

  return {
    form,
    accountInfo,
    handleResetFilter,
    fieldFilterDefault,
    fieldFilterMore,
    handleDownloadexam,
    handleReload,
    handleCancelUpload,
    showModalupload,
    setIsModalOpenUpload,
    isModalOpenUpload,
    columnsUpload,
    propsupload,
    // content,
    columns,
    dataSource,
    isModalOpen,
    showModal,
    handleOk,
    handleCancel,
    DataModals,
    fetchData,
    hanldeOpenCreate,
    page,
    setPage,
    total,
    loading,
    contextHolder,
    Hotline,
  };
};

export default ObserverLogpage_public;
