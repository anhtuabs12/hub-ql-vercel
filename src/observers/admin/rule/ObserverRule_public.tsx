// import { FormFilterFileCustomer } from "@/components/forms/FieldList";
import FormRender from "@/components/forms/FormRender";
import {
  DEMO_TYPE_MODAL,
  TYPE_MODAL_CUSTOMER,
  TYPE_MODAL_FORM,
} from "@/components/modals/types";
import { ModalStatus } from "@/components/tables/enums";
import {
  ColunmRule,
  ColunmUploadFiles,
} from "@/components/tables/field";
import { message, UploadProps } from "antd";
import React from "react";
import {
  ruleCreateRequest,
  ruleDeleteRequest,
  ruleListRequest,
  ruleUpdateRequest,
} from "../../../../redux/actions/ruleAction";
import { openNotificationWithIcon } from "../../../../constants/notification";
import { NotificationType } from "@/pages/login";
// import { findbycodeCustomer } from "../../../../apiRoutes/rule";
type Props = {};

const ObserverRule_public = (init: any) => {
  const {
    contextHolder,
    api,
    data,
    setData,
    editingKey,
    setEditingKey,

    queryData, 
    setQueryData,
    form,
    setIsModalOpen,
    isModalOpen,
    setModalStatus,
    modalStatus,
    ruleList,
    total,
    setDataSource,
    // dataSource,
    page,
    setPage,
    fieldFormsList,
    setFieldFormsList,
    editorId,
    setEditorId,
    dispatch,
  } = init;

  const fetchData = async () => {
      dispatch(ruleListRequest(queryData));
  };

  const handleSearch = (name:any) =>{
    if(name){
      setQueryData({...queryData, name})
      dispatch(ruleListRequest({...queryData, name}));
    }else{
      setQueryData({ offset: (page - 1) * 10, limit: 10  })
      dispatch(ruleListRequest({ offset: (page - 1) * 10, limit: 10 }));
    }
  }

  const dataSource = ruleList?.map((value: any, index: any) => {
    value.key = index;
    return value;
  });

  const HandleEdit = async (code: any) => {
    // let data = await findbycodeCustomer(code);
    // console.log(data.data.code, "handleedit");
    // setEditorId(code);
    // form.setFieldsValue({
    //   code: data.data.code,
    //   fullname: data.data.fullname,
    //   description: data.data.description,
    // });

    setModalStatus(ModalStatus.EDIT);
    showModal();
  };

  const onFinish = async (values: any) => {
    switch (modalStatus) {
      // case ModalStatus.CREATE:
      //   dispatch(
      //     ruleCreateRequest({
      //       data: values,
      //       callbackSuccess: () => {
      //         handleOk();
      //       },
      //     })
      //   );
      //   break;
      case ModalStatus.EDIT:

        break;
      default:
        break;
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    // console.log(errorInfo, "---value da nhan o ngoai");
  };

  const initialValues = {
    mausac: "#0079FF",
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const Datas = {
    submitBtn: {
      title: (
        <>
          <div className="">
            {modalStatus == ModalStatus.CREATE ? "Thêm" : "Lưu"}
          </div>
        </>
      ),
      offset: 20,
      span: 4,
      classname: "h-[36px] !rounded-lg",
    },
    onClose: handleCancel,
    initialValues,
    onFinish,
    onFinishFailed,
    form,
    hidden: modalStatus == !ModalStatus.CREATE ? false : true,
    disable: modalStatus == ModalStatus.CREATE ? false : true,
  };



  const handleOk = () => {
    setIsModalOpen(false);
  };

  const DataModals = {
    width: 800,
    form: form,
    title: modalStatus == ModalStatus.CREATE ? "Tạo mới" : "Chỉnh sửa",
    isModalOpen,
    handleOk,
    handleCancel,
    type: TYPE_MODAL_CUSTOMER,
    dataModals: Datas,
  };
  const showModal = () => {
    setIsModalOpen(true);
  };

  const hanldeOpenCreate = () => {
    setModalStatus(ModalStatus.CREATE);
    form.resetFields();
    showModal();
  };


  // const content = (
  //   <>
  //     <div className="w-[400px] min-h-[300px]">
  //       <FormRender FieldList={fieldListFilter} form={form} />
  //     </div>
  //   </>
  // );

  const propsupload: UploadProps = {
    name: "file",
    multiple: true,
    action: "https://660d2bd96ddfa2943b33731c.mockapi.io/api/upload",
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (status === "done") {
        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
  };

  const columnsUpload = ColunmUploadFiles() as any;

  const handleReload = ()=>{
    dispatch(ruleListRequest({ offset: (page - 1) * 10, limit: 10 }));
  };




  const isEditing = (record: any) => {
    console.log(editingKey, "editingKey");
    return record.key === editingKey;
  };

  
  const edit = (record: any & { key: React.Key }) => {
    form.setFieldsValue({ name: "", age: "", address: "", ...record });
    console.log(record.key, "record.key");
    setEditingKey(record.key);
  };

  const cancel = () => {
    setEditingKey("");
  };

  const save = async (key: React.Key) => {
    try {
      const row = (await form.validateFields()) as any;

      const newData = [...data];
      const index = newData.findIndex((item) => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });

        let Code  = newData.find((value:any)=> value.key == key)?.Code

        // console.log(Code, row, "dasdasd")
        dispatch(
          ruleUpdateRequest({
            code: Code,
            body: {
              Value: row.Value,
              Title: row.Title,
            },
            callbackError: (error: string) => {
              console.log(error, "error");
              openNotificationWithIcon(api, NotificationType.ERROR, error, "");
            },
            callbackSuccess: () => {
              openNotificationWithIcon(api, NotificationType.SUCCESS, "Sửa thành công", "");
              handleOk();
            },
          })
        );


        setData(newData);
        setEditingKey("");
      } else {
        newData.push(row);
        setData(newData);
        setEditingKey("");
      }
    } catch (errInfo) {}
  };

  const dataColunms = {
    // HandleEdit: HandleEdit,
    // HandleDelete: async (code: any) => {
    //   dispatch(ruleDeleteRequest({ data: code }));
    // },
    isEditing,
    save,
    cancel,
    editingKey,
    edit,
  };
  const columns = ColunmRule(dataColunms) as any;
  return {
    data,
    setData,
    editingKey,
    setEditingKey,
    isEditing,
    edit,
    cancel,
    save,

    handleSearch,
    handleReload,
    total,
    columnsUpload,
    propsupload,
    // content,
    columns,
    dataSource,
    isModalOpen,
    showModal,
    handleOk,
    handleCancel,
    DataModals,
    fetchData,
    hanldeOpenCreate,
    page,
    setPage,
    form,
    contextHolder,
  };
};

export default ObserverRule_public;
