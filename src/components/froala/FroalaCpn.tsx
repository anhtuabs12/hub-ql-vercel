import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'froala-editor/js/plugins/image.min.js'
import 'froala-editor/js/plugins/font_size.min.js'
import 'froala-editor/js/plugins/colors.min.js'
import 'froala-editor/js/plugins/table.min.js'
import 'froala-editor/js/plugins/lists.min.js'
import FroalaEditor from 'react-froala-wysiwyg';

const MyFroalaEditor = () => {
  return <FroalaEditor />;
};

export default MyFroalaEditor;