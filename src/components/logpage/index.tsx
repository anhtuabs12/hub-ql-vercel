import { ModalStatus } from "@/components/tables/enums";
import { Form, notification } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import withStaffAuth from "../../../withAuths/withStaffAuth";
import TemplateLogpage_public from "@/templates/admin/TemplateLogpage_public";
import ObserverLogpage_public from "@/observers/admin/logpage/ObserverLogpage_public";

type Props = {
  Hotline?:string
};

const Logpage = ({Hotline}: Props) => {
  const [dataSource, setDataSource] = useState({ data: [], total: null });
  const [page, setPage] = useState(1);
  const [editorId, setEditorId] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalStatus, setModalStatus] = useState<ModalStatus | null>(null);
  const [isModalOpenUpload, setIsModalOpenUpload] = useState(false);
  const [selectedRows, setSelectedRows] = useState([]);
  const [filterData, setFilterData] = useState<any>({});
  const { logList, total, isLoading } = useSelector(
    (state: any) => state.logReducer
  );
  const [api, contextHolder] = notification.useNotification();
  const { accountInfo } = useSelector((state: any) => state.authReducer);

  const router = useRouter();
  const dispatch = useDispatch();
  const [form] = Form.useForm();



  let init = {
    Hotline,
    logList,
    contextHolder,
    api,
    loading: isLoading,
    isModalOpen,
    setIsModalOpen,
    modalStatus,
    setModalStatus,
    form,
    dataSource,
    setDataSource,
    page,
    setPage,
    editorId,
    setEditorId,
    dispatch,
    accountInfo,
    total,
    selectedRows,
    setSelectedRows,
    isModalOpenUpload,
    setIsModalOpenUpload,
    filterData,
    setFilterData,
  };

  const Observer = ObserverLogpage_public(init);
  const LoadData = async (Hotline:any) => {
    try {
      let data = await Observer.fetchData(Hotline);
    } catch (error) {}
  };

  useEffect(()=>{
    form.setFieldValue("Hotline", Hotline)
    setFilterData({Hotline})
  },[Hotline])


  useEffect(() => {
    LoadData(Hotline);
  }, [page, Hotline]);
  
  return <TemplateLogpage_public observer={Observer} />;
};

export default withStaffAuth(Logpage);
