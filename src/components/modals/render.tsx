import { useEffect, useState } from "react";
import { FieldModalProductAdmin } from "./fieldModals/fieldAdmin";
import {
  DataCustomerField,
  DataAccountField,
  DataBrandnameField,
  DataEditFieldHotline,
  DataField,
  FieldModalScript,
  FieldModalScripts,
  DataFieldHotline,
  DataFieldHotlineAction,
} from "./fields";
import {
  DEMO_TYPE_MODAL,
  TYPE_MODAL_CATEGORY,
  TYPE_MODAL_FIELDFORM,
  TYPE_MODAL_FIELDGROUPSCRIPT,
  TYPE_MODAL_FIELDSCRIPT,
  TYPE_MODAL_FORM,
  TYPE_MODAL_KEYWORD,
  TYPE_MODAL_PRODUCT_ADMIN,
  TYPE_MODAL_SCRIPT,
  TYPE_MODAL_HOTLINE,
  EDIT_TYPE_MODAL_HOTLINE,
  TYPE_MODAL_CUSTOMER,
  TYPE_MODAL_ACCOUNT,
  TYPE_MODAL_BRANDNAME,
  TYPE_MODAL_HOTLINE_ACTION,

} from "./types";

const RenderContentModal = (data: any) => {
  const [rerender, setRerender] = useState(false);

  useEffect(() => {}, [rerender, data]);

  switch (data.type) {
    case DEMO_TYPE_MODAL:
      return DataField(data.dataModals);
    case TYPE_MODAL_HOTLINE:
      data.dataModals.setRerender = setRerender;
      data.dataModals.rerender = rerender;
      return DataFieldHotline(data.dataModals);
    case TYPE_MODAL_HOTLINE_ACTION:
      data.dataModals.setRerender = setRerender;
      data.dataModals.rerender = rerender;
      return DataFieldHotlineAction(data.dataModals);

    case EDIT_TYPE_MODAL_HOTLINE:
      return DataEditFieldHotline(data.dataModals);
    case TYPE_MODAL_CUSTOMER:
      return DataCustomerField(data.dataModals);
    case TYPE_MODAL_ACCOUNT:
      data.dataModals.setRerender = setRerender;
      data.dataModals.rerender = rerender;
      return DataAccountField(data.dataModals);
    case TYPE_MODAL_BRANDNAME:
      return DataBrandnameField(data.dataModals);
    case TYPE_MODAL_FIELDSCRIPT:
      return FieldModalScript(data.dataModals);
    case TYPE_MODAL_SCRIPT:
      return FieldModalScripts(data.dataModals);
    //kt

    case TYPE_MODAL_PRODUCT_ADMIN:
      return FieldModalProductAdmin(data.dataModals);
    default:
      break;
  }
};

export default RenderContentModal;
