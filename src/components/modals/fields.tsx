import {
  CHECKBOX_TYPES,
  COLOR_PICKER_TYPES,
  INPUT_DATE_PICKER_TYPES,
  INPUT_DEFAULT_TYPES,
  INPUT_NUMBER_TYPES,
  INPUT_PASSWORD_TYPES,
  INPUT_TEXTAREA_TYPES,
  RADIO_TYPES,
  RENDER,
  SELECT_TYPES,
  SWITCH_TYPES,
} from "@/components/forms/FromType";
import {
  HotlineStatusList,
  NetWorkList,
  StatusBrandname,
} from "../../../constants/config";
import { listBrandname } from "../../../apiRoutes/brandname";
import { hotlineListRequest } from "../../../redux/actions/hotlineAction";
import { brandnameListRequest } from "../../../redux/actions/brandnameAction";
import { ModalStatus } from "../tables/enums";
import { formatSelecter_Form } from "../../../constants/formats";
import { Divider } from "antd";
import { StarOutlined } from "@ant-design/icons";

export const DataField = (Datas: any) => {
  const form = Datas.form;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    fields: [
      {
        span: 8,
        type: INPUT_DEFAULT_TYPES,
        label: "Tên",
        onchange: (e: any) => {
          form.setFieldValue("ho", "bui anh");
        },
        name: "name",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 8,
        label: "Họ",
        name: "ho",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập họ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        onchange: (e: any) => {
          form.setFieldValue("name", "tu");
        },
        label: "Tên đệm",
        name: "tendem",
        required: false,
        options: [{ value: "tu", label: "Tu" }],
        message: "Không nên bỏ trống",
        placeholder: "Nhập họ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "kĩ năng",
        name: "skill",
        required: false,
        options: Datas.dataSkill || [],
        message: "Không nên bỏ trống",
        placeholder: "Nhập ",
      },
      {
        type: INPUT_NUMBER_TYPES,
        span: 8,
        label: "Tuổi",
        name: "age",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Tuổi",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 8,
        label: "Ngày sinh",
        name: "ngaysinh",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập ngày sinh",
      },

      {
        type: INPUT_PASSWORD_TYPES,
        span: 8,
        label: "Mật khẩu",
        name: "password",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập ngày mật khẩu",
      },

      {
        type: CHECKBOX_TYPES,
        vertical: false,
        options: [
          { value: "nu", label: "Nữ" },
          { value: "nam", label: "Nam" },
          { value: "namnu", label: "Không xác định" },
        ],
        span: 8,
        label: "Giới tính",
        name: "gioitinh",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: SWITCH_TYPES,
        span: 8,
        label: "Đã đi nghĩa vụ quân sự",
        name: "nghiaVuQuanSu",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: RADIO_TYPES,
        optionType: "button",
        span: 8,
        label: "Nơi sinh",
        name: "noisinh",
        required: false,
        options: [
          { value: "hanoi", label: "Hà Nội" },
          { value: "hcm", label: "Hồ Chí Minh" },
          { value: "namdinh", label: "Nam Định" },
        ],
        message: "Không nên bỏ trống",
        vertical: false,
      },
      {
        type: COLOR_PICKER_TYPES,
        span: 24,
        showText: true,
        defaultValue: "#0079FF",
        label: "Màu sắc",
        name: "mausac",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: INPUT_TEXTAREA_TYPES,
        span: 24,
        label: "Nội dung",
        name: "noidung",
        required: false,
        message: "Không nên bỏ trống",
      },
    ],
  };
};

export const DataBrandnameField = (Datas: any) => {
  const form = Datas.form;
  const checkBrandnameToHotline = form.getFieldValue("CheckBrandnameToHotline");
  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        type: INPUT_DEFAULT_TYPES,
        span: 12,
        label: "Số giấy chứng nhận",
        name: "Certificate_number",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Số giấy chứng nhận",
      },
      {
        type: SELECT_TYPES,
        span: 12,
        label: "Công ty",
        name: "Enterprise_number_company",
        required: true,
        options: [
          { value: "0967671182", label: "anhtu" },
          { value: "0862062842", label: "tiendung" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Nhập Công ty",
        // hidden: Datas.hidden,
        disabled: checkBrandnameToHotline && checkBrandnameToHotline.length > 0,
      },
      {
        type: SELECT_TYPES,
        span: 12,
        // mode: "multiple",
        label: "Trạng thái",
        name: "Status",
        required: true,
        options: StatusBrandname,
        message: "Không nên bỏ trống",
        placeholder: "Trạng thái",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 12,
        label: "Brandname",
        name: "Brandname",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Brandname",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 12,
        showTime: true,
        label: "Ngày hoạt động",
        name: "Lunch_date",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày hoạt động",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 12,
        showTime: true,
        label: "Ngày hết hạn",
        name: "Expiry_date",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày hết hạn",
      },
      {
        type: INPUT_TEXTAREA_TYPES,
        span: 24,
        label: "Ghi chú",
        name: "Note",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ghi chú",
      },
    ],
  };
};
export const DataFieldHotline = (Datas: any) => {
  let customerUnSip = Datas?.customerUnSip || [];
  let modalType = Datas?.modalType;
  const form = Datas.form;
  const dispatch = Datas.dispatch;

  const handlerenderCustomerLabel = (data: any) => {
    return (
      <div className="flex items-center justify-between">
        <span>{data.companyname}</span>
        {data.type == "unSip" ? (
          <span>
            <StarOutlined />
          </span>
        ) : (
          <StarOutlined className="!text-yellow-500" />
        )}
      </div>
    );
  };

  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        type: SELECT_TYPES,
        span: 8,
        label: "Nhà mạng",
        name: "Type_NetWork",
        disabled: modalType == ModalStatus.EDIT,
        required: true,
        options: NetWorkList,
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Nhà mạng-- ",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 8,
        label: "Hotline",
        name: "Hotline",
        disabled: modalType == ModalStatus.EDIT,
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "-- Hotline-- ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 8,
        showTime: true,
        label: "Ngày mua",
        name: "PurchaseDate",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày mua",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        label: "Trạng thái",
        name: "Status",
        required: true,
        onchange: (e: any) => {
          Datas.setRerender(!Datas.rerender);
          // form.setFieldValue("ho", "bui anh");
        },
        options: HotlineStatusList,
        message: "Không nên bỏ trống",
        placeholder: "Chọn trạng thái số",
      },

      {
        type: SELECT_TYPES,
        span: 8,
        label: "Khách hàng",
        name: "CustomerCode",
        onchange: async (e: any) => {
          console.log(e, "Eeeeee");
          dispatch(
            brandnameListRequest({
              data: { customer_code: e },
              callbackSuccess: (value: any) => {
                form.setFieldValue("BrandNameList", value);
                Datas.setRerender(!Datas.rerender);
              },
            })
          );
        },
        required:
          form.getFieldValue("Status") == "doing" ||
          form.getFieldValue("Status") == "keeping" ||
          form.getFieldValue("Status") == "waitbrandname",
        hidden: !(
          form.getFieldValue("Status") == "doing" ||
          form.getFieldValue("Status") == "keeping" ||
          form.getFieldValue("Status") == "waitbrandname"
        ),
        options: customerUnSip
          .concat([
            { enterprise_number: "0967671182", companyname: "royalmoon" },
            { enterprise_number: "0967671183", companyname: "its" },
          ])
          .map((value: any) =>
            formatSelecter_Form(value.enterprise_number, value.companyname)
          ),
        message: "Không nên bỏ trống",
        placeholder: "Chọn khách hàng",
      },

      {
        type: SELECT_TYPES,
        span: 8,
        label: "Brand Name",
        hidden: !(
          form.getFieldValue("Status") == "waitbrandname" ||
          form.getFieldValue("Status") == "doing"
        ),
        name: "BrandName",
        required: form.getFieldValue("Status") == "waitbrandname",
        options: form.getFieldValue("BrandNameList"),
        message: "Không nên bỏ trống",
        placeholder: "--Brand Name -- ",
      },

      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 8,
        showTime: true,
        label: "Ngày cấp",
        hidden: !(
          form.getFieldValue("Status") == "doing" ||
          // form.getFieldValue("Status") == "keeping" ||
          form.getFieldValue("Status") == "waitbrandname"
        ),
        name: "Lunch_date",
        required:
          form.getFieldValue("Status") == "doing" ||
          // form.getFieldValue("Status") == "keeping" ||
          form.getFieldValue("Status") == "waitbrandname",
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày cấp số",
      },
      {
        type: INPUT_NUMBER_TYPES,
        span: 8,
        label: "Giữ trong thời gian(giờ)",
        hidden: !(form.getFieldValue("Status") == "keeping"),
        name: "Extention_periodDate",
        required: form.getFieldValue("Status") == "keeping",
        message: "Không nên bỏ trống",
        placeholder: "-- -- ",
      },
      {
        type: INPUT_TEXTAREA_TYPES,
        span: 24,
        label: "Khách hàng cuối",
        name: "Customer_end",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Điền thông tin khách hàng cuối nếu có",
      },
    ],
  };
};

export const DataFieldHotlineAction = (Datas: any) => {
  const form = Datas.form;
  const dispatch = Datas.dispatch;
  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        type: SELECT_TYPES,
        span: 8,
        label: "Trạng thái",
        name: "Status",
        required: false,
        onchange: (e: any) => {
          Datas.setRerender(!Datas.rerender);
          // form.setFieldValue("ho", "bui anh");
        },
        disabled: Datas?.selectDisable,
        options: HotlineStatusList,
        message: "Không nên bỏ trống",
        placeholder: "Chọn trạng thái số",
      },

      {
        type: SELECT_TYPES,
        span: 8,
        label: "Khách hàng",
        name: "CustomerCode",
        onchange: async (e: any) => {
          console.log(e, "Eeeeee");
          dispatch(
            brandnameListRequest({
              data: { customer_code: e },
              callbackSuccess: (value: any) => {
                form.setFieldValue("BrandNameList", value);
                Datas.setRerender(!Datas.rerender);
              },
            })
          );
        },
        required:
          form.getFieldValue("Status") == "doing" ||
          form.getFieldValue("Status") == "keeping" ||
          form.getFieldValue("Status") == "waitbrandname",
        hidden: !(
          form.getFieldValue("Status") == "doing" ||
          form.getFieldValue("Status") == "keeping" ||
          form.getFieldValue("Status") == "waitbrandname"
        ),
        options: [
          { value: "0967671182", label: "anhtu" },
          { value: "0862062842", label: "tiendung" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Chọn khách hàng",
      },

      {
        type: SELECT_TYPES,
        span: 8,
        label: "Brand Name",
        hidden: !(
          form.getFieldValue("Status") == "waitbrandname" ||
          form.getFieldValue("Status") == "doing"
        ),
        name: "BrandName",
        required: form.getFieldValue("Status") == "waitbrandname",
        options: form.getFieldValue("BrandNameList"),
        message: "Không nên bỏ trống",
        placeholder: "--Brand Name -- ",
      },

      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 8,
        showTime: true,
        label: "Ngày cấp",
        hidden: !(
          (form.getFieldValue("Status") == "doing")
          // form.getFieldValue("Status") == "keeping" ||
          // form.getFieldValue("Status") == "waitbrandname"
        ),
        name: "Lunch_date",
        required: form.getFieldValue("Status") == "doing",
        // form.getFieldValue("Status") == "keeping" ||
        // form.getFieldValue("Status") == "waitbrandname",
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày cấp số",
      },
      {
        type: INPUT_NUMBER_TYPES,
        span: 8,
        label: "Giữ trong thời gian(giờ)",
        hidden: !(form.getFieldValue("Status") == "keeping"),
        name: "Extention_periodDate",
        required: form.getFieldValue("Status") == "keeping",
        message: "Không nên bỏ trống",
        placeholder: "-- -- ",
      },
      {
        type: INPUT_TEXTAREA_TYPES,
        span: 24,
        label: "Nội dung",
        name: "Description",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nội dung",
      },
    ],
  };
};

export const DataEditFieldHotline = (Datas: any) => {
  const form = Datas.form;

  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Nhà mạng",
        name: "nhamang",
        required: false,
        options: [
          { value: "1", label: "Nhà mạng 1" },
          { value: "2", label: "Nhà mạng 2" },
          { value: "3", label: "Nhà mạng 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Nhà mạng-- ",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Hotline",
        name: "hotline",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Hotline-- ",
      },
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Trạng thái",
        name: "trangthai",
        required: false,
        options: [
          { value: "1", label: "Trạng thái 1" },
          { value: "2", label: "Trạng thái 2" },
          { value: "3", label: "Trạng thái 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Trạng thái-- ",
      },
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Khách hàng sử dụng",
        name: "khachhang",
        required: false,
        options: [
          { value: "1", label: "Khách hàng 1" },
          { value: "2", label: "Khách hàng 2" },
          { value: "3", label: "Khách hàng 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Khách hàng-- ",
        disabled: true,
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        showTime: true,
        label: "Ngày cấp",
        name: "ngaycap",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày cấp",
        disabled: true,
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Ngày mua",
        name: "ngaymua",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày mua",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Ngày tạo",
        name: "ngaytao",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày tạo",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Ngày sửa",
        name: "ngaysua",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày sửa",
        disabled: true,
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Số lần chặn Viettel",
        name: "slchanvietteltu",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Từ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: " ",
        name: "slchanviettelden",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Đến",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Số lần chặn Vina",
        name: "slchanVinatu",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Từ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: " ",
        name: "slchanVinaden",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Đến",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Số lần chặn Mobi",
        name: "slchanMobitu",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Từ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: " ",
        name: "slchanMobiden",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Đến",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "Thông mạng Viettel",
        name: "Viettel",
        required: false,
        options: [
          { value: "1", label: "Thông mạng Viettel 1" },
          { value: "2", label: "Thông mạng Viettel 2" },
          { value: "3", label: "Thông mạng Viettel 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Thông mạng Viettel-- ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "Thông mạng Vina",
        name: "Vina",
        required: false,
        options: [
          { value: "1", label: "Thông mạng Vina 1" },
          { value: "2", label: "Thông mạng Vina 2" },
          { value: "3", label: "Thông mạng Vina 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Thông mạng Vina-- ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "Thông mạng Mobi ",
        name: "Mobi",
        required: false,
        options: [
          { value: "1", label: "Thông mạng Mobi 1" },
          { value: "2", label: "Thông mạng Mobi 2" },
          { value: "3", label: "Thông mạng Mobi 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Thông mạng Mobi-- ",
      },
    ],
  };
};

export const DataCustomerField = (Datas: any) => {
  const form = Datas.form;
  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        type: INPUT_DEFAULT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Mã khách hàng",
        name: "code",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "--Nhập Mã khách hàng-- ",
        hidden: Datas.hidden,
        disabled: Datas.disable,
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Tên khách hàng",
        name: "fullname",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "--Nhập Tên khách hàng-- ",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Mô tả",
        name: "description",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "--Nhập Mô tả-- ",
      },
    ],
  };
};
export const DataAccountField = (Datas: any) => {
  const modalStatus = Datas.modalStatus;
  const form = Datas.form;
  const setRerender = Datas.setRerender;
  const rerender = Datas.rerender;
  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        type: SELECT_TYPES,
        span: 24,
        // mode: "multiple",
        label: "Vai trò",
        name: "Role",
        required: true,
        onchange: () => {
          setRerender(!rerender);
        },
        options: [
          { value: "administrator", label: "Quản trị" },
          { value: "saleadmin", label: "Sale Admin" },
          {
            value: "customer",
            label: "Khách hàng (chưa có trên hệ thống SIP)",
          },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Vai trò",
      },
      {
        disabled:
          modalStatus == ModalStatus.EDIT &&
          form.getFieldValue("Role") == "customer",
        type: INPUT_DEFAULT_TYPES,
        span: 24,
        label:
          form.getFieldValue("Role") == "customer" ? "Số đại diện" : "Email",
        name:
          form.getFieldValue("Role") == "customer"
            ? "enterprise_number"
            : "Email",
        required: true,
        message: "Không nên bỏ trống",
        placeholder:
          form.getFieldValue("Role") == "customer"
            ? "Nhập số đại điện"
            : "Nhập email",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 24,
        label:
          form.getFieldValue("Role") == "customer"
            ? "Tên công ty"
            : "Tên nhân viên",
        name:
          form.getFieldValue("Role") == "customer" ? "companyname" : "FullName",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên nhân viên",
      },
    ],
  };
};

export const FieldModalScript = (Datas: any) => {
  const form = Datas.form;

  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        span: 24,
        type: INPUT_DEFAULT_TYPES,
        label: "Tên tiêu chí",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "name",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên nhóm tiêu chí",
      },
      {
        span: 24,
        type: RENDER,
        renderCpn: Datas?.renderCpn || null,
        label: "Hướng dẫn chấm điểm",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "names",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên nhóm tiêu chí",
      },
      {
        type: SELECT_TYPES,
        span: 24,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        label: "Nhóm tiêu chí",
        name: "tendem",
        required: true,
        options: [{ value: "a", label: "Nhóm 1" }],
        message: "Không nên bỏ trống",
        placeholder: "Chọn loại",
      },
      {
        span: 24,
        type: INPUT_NUMBER_TYPES,
        label: "Điểm tối đa",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "amout",
        required: true,
        placeholder: "Điểm tối đa",
      },
      {
        span: 24,
        type: INPUT_DEFAULT_TYPES,
        label: "Mô tả",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "desc",
        required: false,
        placeholder: "Mô tả ",
      },
    ],
  };
};

export const FieldModalScripts = (Datas: any) => {
  const form = Datas.form;

  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        span: 24,
        type: INPUT_DEFAULT_TYPES,
        label: "Tên kịch bản",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "name",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên nhóm tiêu chí",
      },
      {
        type: INPUT_NUMBER_TYPES,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        label: "Điểm dưới trung bình",
        name: "tendems",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Điểm",
      },
      {
        type: INPUT_NUMBER_TYPES,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        label: "Điểm trên trung bình",
        name: "tendems1",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Điểm",
      },
      {
        span: 24,
        type: RENDER,
        renderCpn: Datas?.renderCpn || null,
        label: "Các tiêu chí",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "fieldss",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "",
      },
      {
        span: 24,
        type: INPUT_DEFAULT_TYPES,
        label: "Mô tả",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "desc",
        required: false,
        placeholder: "Mô tả ",
      },
    ],
  };
};

export const DataFieldHotlineLog = (Datas: any) => {
  const form = Datas.form;
  return {
    closeBtn: Datas.closeBtn,
    submitBtn: Datas.submitBtn,
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    onclose: Datas.onClose,
    fields: [
      {
        type: RENDER,
        span: 24,
        label: "Email",
        name: "Email",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập email",
      },
    ],
  };
};
