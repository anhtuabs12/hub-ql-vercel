import { Modal } from "antd";
import React, { useEffect } from "react";
import RenderContentModal from "./render";
import FormRender from "../forms/FormRender";

const Modalcpn = ({ DataModals }: any) => {
  return (
    <Modal
      width={DataModals?.Render?.renderStatus ? 1600:  DataModals?.width || 600}
      footer={null}
      title={DataModals?.Render?.renderStatus ? "" : DataModals?.title}
      open={DataModals.isModalOpen}
      onOk={DataModals.handleOk}
      onCancel={DataModals.handleCancel}
    >
      {DataModals?.Render?.renderStatus ? (
        DataModals.Render.content
      ) : (
        <FormRender
          FieldList={RenderContentModal(DataModals) as any}
          form={DataModals.form}
        />
      )}
    </Modal>
  );
};

export default Modalcpn;
