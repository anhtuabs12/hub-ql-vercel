import { INPUT_DEFAULT_TYPES, INPUT_NUMBER_TYPES, SELECT_TYPES } from "@/components/forms/FromType";


export const FieldModalProductAdmin = (Datas: any) => {
    const form = Datas.form;
  
    return {
      closeBtn: Datas.closeBtn,
      submitBtn: Datas.submitBtn,
      onfinishfailed: Datas.onFinishFailed,
      onfinish: Datas.onFinish,
      onclose: Datas.onClose,
      fields: [
        {
          span: 24,
          type: INPUT_DEFAULT_TYPES,
          label: "Tên tiêu chí",
          onchange: (e: any) => {
            // form.setFieldValue("ho", "bui anh");
          },
          name: "name",
          required: true,
          message: "Không nên bỏ trống",
          placeholder: "Nhập tên nhóm tiêu chí",
        },
        {
          type: SELECT_TYPES,
          span: 24,
          onchange: (e: any) => {
            // form.setFieldValue("name", "tu");
          },
          label: "Nhóm tiêu chí",
          name: "tendem",
          required: true,
          options: [{ value: "a", label: "Nhóm 1" }],
          message: "Không nên bỏ trống",
          placeholder: "Chọn loại",
        },
        {
          span: 24,
          type: INPUT_NUMBER_TYPES,
          label: "Điểm tối đa",
          onchange: (e: any) => {
            // form.setFieldValue("ho", "bui anh");
          },
          name: "amout",
          required: true,
          placeholder: "Điểm tối đa",
        },
        {
          span: 24,
          type: INPUT_DEFAULT_TYPES,
          label: "Mô tả",
          onchange: (e: any) => {
            // form.setFieldValue("ho", "bui anh");
          },
          name: "desc",
          required: false,
          placeholder: "Mô tả ",
        }
      ],
    };
  };
