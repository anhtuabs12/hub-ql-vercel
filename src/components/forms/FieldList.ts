import {
  CHECKBOX_TYPES,
  COLOR_PICKER_TYPES,
  INPUT_DATE_PICKER_TYPES,
  INPUT_DEFAULT_TYPES,
  INPUT_NUMBER_TYPES,
  INPUT_PASSWORD_TYPES,
  INPUT_TEXTAREA_TYPES,
  RADIO_TYPES,
  SELECT_TYPES,
  SWITCH_TYPES,
} from "@/components/forms/FromType";

export const DataDemo = (Datas: any) => {
  const form = Datas.form;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    fields: [
      {
        span: 8,
        type: INPUT_DEFAULT_TYPES,
        label: "Tên",
        onchange: (e: any) => {
          form.setFieldValue("ho", "bui anh");
        },
        name: "name",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 8,
        label: "Họ",
        name: "ho",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập họ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        onchange: (e: any) => {
          form.setFieldValue("name", "tu");
        },
        label: "Tên đệm",
        name: "tendem",
        required: false,
        options: [{ value: "tu", label: "Tu" }],
        message: "Không nên bỏ trống",
        placeholder: "Nhập họ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "kĩ năng",
        name: "skill",
        required: false,
        options: [
          { value: "1", label: "reactjs" },
          { value: "2", label: "nodejs" },
          { value: "3", label: "nextjs" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Nhập ",
      },
      {
        type: INPUT_NUMBER_TYPES,
        span: 8,
        label: "Tuổi",
        name: "age",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Tuổi",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 8,
        label: "Ngày sinh",
        name: "ngaysinh",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập ngày sinh",
      },

      {
        type: INPUT_PASSWORD_TYPES,
        span: 8,
        label: "Mật khẩu",
        name: "password",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập ngày mật khẩu",
      },

      {
        type: CHECKBOX_TYPES,
        vertical: false,
        options: [
          { value: "nu", label: "Nữ" },
          { value: "nam", label: "Nam" },
          { value: "namnu", label: "Không xác định" },
        ],
        span: 8,
        label: "Giới tính",
        name: "gioitinh",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: SWITCH_TYPES,
        span: 8,
        label: "Đã đi nghĩa vụ quân sự",
        name: "nghiaVuQuanSu",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: RADIO_TYPES,
        optionType: "button",
        span: 8,
        label: "Nơi sinh",
        name: "noisinh",
        required: false,
        options: [
          { value: "hanoi", label: "Hà Nội" },
          { value: "hcm", label: "Hồ Chí Minh" },
          { value: "namdinh", label: "Nam Định" },
        ],
        message: "Không nên bỏ trống",
        vertical: false,
      },
      {
        type: COLOR_PICKER_TYPES,
        span: 24,
        showText: true,
        defaultValue: "#0079FF",
        label: "Màu sắc",
        name: "mausac",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: INPUT_TEXTAREA_TYPES,
        span: 24,
        label: "Nội dung",
        name: "noidung",
        required: false,
        message: "Không nên bỏ trống",
      },
    ],
  };
};


export const FormLoginCustomer= (Datas:any) =>{
  const form = Datas.form;
  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      // {
      //   span: 24,
      //   type: INPUT_DEFAULT_TYPES,
      //   label: "Domain",
      //   onchange: (e: any) => {
      //     // form.setFieldValue("ho", "bui anh");
      //   },
      //   name: "domain",
      //   required: true,
      //   message: "Không nên bỏ trống",
      //   placeholder: "Nhập domain",
      // },
      {
        span: 24,
        type: INPUT_DEFAULT_TYPES,
        label: "Tài khoản",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "enterprise_number",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập số tài khoản",
      },
      {
        span: 24,
        type: INPUT_PASSWORD_TYPES,
        label: "Mật khẩu",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "password",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập password",
      },
    ]}
}

export const FormLoginStaff= (Datas:any) =>{
  const form = Datas.form;
  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      // {
      //   span: 24,
      //   type: INPUT_DEFAULT_TYPES,
      //   label: "Domain",
      //   onchange: (e: any) => {
      //     // form.setFieldValue("ho", "bui anh");
      //   },
      //   name: "domain",
      //   required: true,
      //   message: "Không nên bỏ trống",
      //   placeholder: "Nhập domain",
      // },
      {
        span: 24,
        type: INPUT_DEFAULT_TYPES,
        label: "Email",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "Email",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Email",
      },
      {
        span: 24,
        type: INPUT_PASSWORD_TYPES,
        label: "Mật khẩu",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "Password",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập password",
      },
    ]}
}

export const FormRegister= (Datas:any) =>{
  const form = Datas.form;
  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        span: 24,
        type: INPUT_DEFAULT_TYPES,
        label: "Họ tên người dùng",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "fullname",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập họ tên",
      },
      {
        span: 24,
        type: INPUT_DEFAULT_TYPES,
        label: "Số điện thoại",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "phonenumber",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập số điện thoại",
      },
      {
        span: 24,
        type: INPUT_PASSWORD_TYPES,
        label: "Mật khẩu",
        onchange: (e: any) => {
          form.setFieldValue("ho", "bui anh");
        },
        name: "password",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập password",
      },
      {
        span: 24,
        type: INPUT_PASSWORD_TYPES,
        label: "Xác nhận mật khẩu",
        onchange: (e: any) => {
          // form.setFieldValue("ho", "bui anh");
        },
        name: "repassword",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập password",
      },
    ]}
}

export const FormFilterFileHotline= (Datas:any) =>{
  const form = Datas.form;
  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Nhà mạng",
        name: "nhamang",
        required: false,
        options: [
          { value: "1", label: "Nhà mạng 1" },
          { value: "2", label: "Nhà mạng 2" },
          { value: "3", label: "Nhà mạng 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Nhà mạng-- ",
      },
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Trạng thái",
        name: "trangthai",
        required: false,
        options: [
          { value: "1", label: "Trạng thái 1" },
          { value: "2", label: "Trạng thái 2" },
          { value: "3", label: "Trạng thái 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Trạng thái-- ",
      },
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Khách hàng sử dụng",
        name: "khachhang",
        required: false,
        options: [
          { value: "1", label: "Khách hàng 1" },
          { value: "2", label: "Khách hàng 2" },
          { value: "3", label: "Khách hàng 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Khách hàng-- ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Ngày cấp cho khách hàng",
        name: "ngaycap",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày cấp",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Ngày mua",
        name: "ngaymua",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày mua",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Ngày tạo",
        name: "ngaytao",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày tạo",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Ngày sửa",
        name: "ngaysua",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày sửa",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Số lần chặn Viettel",
        name: "slchanvietteltu",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Từ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: " ",
        name: "slchanviettelden",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Đến",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Số lần chặn Vina",
        name: "slchanVinatu",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Từ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: " ",
        name: "slchanVinaden",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Đến",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: "Số lần chặn Mobi",
        name: "slchanMobitu",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Từ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 6,
        label: " ",
        name: "slchanMobiden",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Đến",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "Thông mạng Viettel",
        name: "Viettel",
        required: false,
        options: [
          { value: "1", label: "Thông mạng Viettel 1" },
          { value: "2", label: "Thông mạng Viettel 2" },
          { value: "3", label: "Thông mạng Viettel 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Thông mạng Viettel-- ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "Thông mạng Vina",
        name: "Vina",
        required: false,
        options: [
          { value: "1", label: "Thông mạng Vina 1" },
          { value: "2", label: "Thông mạng Vina 2" },
          { value: "3", label: "Thông mạng Vina 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Thông mạng Vina-- ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "Thông mạng Mobi ",
        name: "Mobi",
        required: false,
        options: [
          { value: "1", label: "Thông mạng Mobi 1" },
          { value: "2", label: "Thông mạng Mobi 2" },
          { value: "3", label: "Thông mạng Mobi 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Thông mạng Mobi-- ",
      },
    ]}
}

export const FormFilterFileRule= (Datas:any) =>{
  const form = Datas.form;
  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Loại",
        name: "type",
        required: true,
        options: [
          { value: "Số lượng hotline chưa sử dụng tối thiểu còn trong kho.", label: "Số lượng hotline chưa sử dụng tối thiểu còn trong kho." },
          { value: "Số lượng hotline khách hàng có thể giữ tối đa.", label: "Số lượng hotline khách hàng có thể giữ tối đa." },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn loại-- ",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Giá trị",
        name: "value",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "--Chọn giá trị-",
      }
    ]}
}
export const FormFilterFileAccount= (Datas:any) =>{
  const form = Datas.form;
  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Tên nhân viên",
        name: "name",
        required: false,
        options: [
          { value: "1", label: "Tên nhân viên 1" },
          { value: "2", label: "Tên nhân viên 2" },
          { value: "3", label: "Tên nhân viên 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Tên nhân viên-- ",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 12,
        label: "Ngày Tạo",
        name: "date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Ngày Tạo",
      },
      {
        type: SELECT_TYPES,
        span: 12,
        mode: "multiple",
        label: "Vai trò",
        name: "role",
        required: false,
        options: [
          { value: "1", label: "Vai trò 1" },
          { value: "2", label: "Vai trò 2" },
          { value: "3", label: "Vai trò 3" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "--Chọn Vai trò-- ",
      },

    ]}
}