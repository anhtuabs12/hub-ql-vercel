import { Button, Col, Form, Input, Row } from "antd";
import React, { ReactNode, useEffect } from "react";
import { RenderFields } from "./FieldsRender";
import { FormLayout } from "./FormEnum";

const FormRender = ({ FieldList, form }: any) => {
  const RenderForm = () => {
    return FieldList?.fields?.map((value: any, index: number) => {
      let field = RenderFields(value) as ReactNode;
      return (
        <Col className={`${value.hidden ?"hidden": ""}`} key={index} span={value.span || 24}>
          <Form.Item
            key={index}
            label={value.label}
            name={value.name}
            rules={[
              {
                required: value.required,
                message: value.message || "Trường không được bỏ trống",
              },
            ]}
          >
            {field}
          </Form.Item>
        </Col>
      );
    });
  };

  const onFinish = (values: any) => {
    console.log(FieldList, "FieldList in form");
    console.log(values , "values in form");
    FieldList.onfinish(values)
  };

  const onFinishFailed = (errorInfo: any) => {
    FieldList.onfinishfailed(errorInfo)
  };

  useEffect(()=>{

  }, [FieldList , form])

  return (
    <div>
      <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        form={form}
        initialValues={FieldList?.initialValues || {}}
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        layout={FieldList?.layout || FormLayout.VERTICAL}
      >
        <Row gutter={24}>{RenderForm()}</Row>
        <Row className={`${FieldList?.submitBtn?.classname}`} gutter={24}>
          <Form.Item className="w-full mx-[12px]" wrapperCol={{ offset: FieldList?.submitBtn?.offset || 0, span:   FieldList?.submitBtn?.span || 16 }}>
          {/* <Button onClick={FieldList?.closeBtn} type="primary" className={`bg-blue-500 w-full ${FieldList?.closeBtn?.classname}`} >
              {FieldList?.closeBtn?.title || "cancel"}
            </Button> */}
            <Button disabled={FieldList?.submitBtn?.disable} type="primary" className={`bg-blue-500 w-full ${FieldList?.submitBtn?.classname}`} htmlType="submit">
              {FieldList?.submitBtn?.title || "Submit"}
            </Button>
          </Form.Item>
        </Row>
      </Form>
    </div>
  );
};

export default FormRender;
