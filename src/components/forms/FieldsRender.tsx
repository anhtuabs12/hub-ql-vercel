import React from "react";
import {
  INPUT_DEFAULT_TYPES,
  SELECT_TYPES,
  INPUT_NUMBER_TYPES,
  INPUT_DATE_PICKER_TYPES,
  INPUT_TEXTAREA_TYPES,
  INPUT_PASSWORD_TYPES,
  CHECKBOX_TYPES,
  SWITCH_TYPES,
  RADIO_TYPES,
  COLOR_PICKER_TYPES,
  RENDER,
} from "./FromType";
import {
  Checkbox,
  ColorPicker,
  DatePicker,
  Input,
  InputNumber,
  Radio,
  Select,
  Space,
  Switch,
} from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import dayjs from "dayjs";
const { TextArea } = Input;

export const RenderFields = (value: any) => {
  value.onchange = value.onchange || (() => {});
  switch (value.type) {
    case RENDER:
      return <div>{value.renderCpn || ""}</div>;
    case INPUT_DEFAULT_TYPES:
      return (
        <Input
          id={value.id}
          className={`h-[38px]`}
          style={{ width: "100%" }}
          placeholder={value.placeholder || ""}
          onChange={(e) => {
            value.onchange(e);
          }}
          disabled={value.disabled}
        />
      );

    case INPUT_NUMBER_TYPES:
      return (
        <InputNumber
          id={value.id}
          className="h-[38px]"
          style={{ width: "100%" }}
          min={value.min || 0}
          max={value.max || 1000000}
          placeholder={value.placeholder || ""}
          onChange={(e) => {
            value.onchange(e);
          }}
          disabled={value.disabled}
        />
      );

    case SELECT_TYPES:
      return (
        <Select
          showSearch
          id={value.id}
          className="h-[38px]"
          mode={value.mode == "multiple" ? "multiple" : undefined}
          style={{ width: "100%" }}
          onChange={(e) => {
            value.onchange(e);
          }}
          placeholder={value.placeholder || ""}
          disabled={value.disabled}
          options={value.options || []}
          filterOption={(input, option) => {
            const label = option?.label;
            if (typeof label === 'string') {
              return label.toLowerCase().includes(input.toLowerCase());
            }
            return false;
          }}
          filterSort={(optionA, optionB) => {
            const labelA = optionA?.label;
            const labelB = optionB?.label;
            if (typeof labelA === 'string' && typeof labelB === 'string') {
              return labelA.toLowerCase().localeCompare(labelB.toLowerCase());
            }
            return 0;
          }}
        />
      );

    case INPUT_DATE_PICKER_TYPES:
      return (
        <DatePicker
          showTime={value?.showTime}
          id={value.id}
          className="h-[38px]"
          style={{ width: "100%" }}
          onChange={(e) => {
            value.onchange(e);
          }}
          placeholder={value.placeholder || ""}
          disabled={value.disabled}
        />
      );
    case INPUT_TEXTAREA_TYPES:
      return (
        <TextArea
          id={value.id}
          style={{ width: "100%" }}
          maxLength={6}
          rows={4}
          onChange={(e) => {
            value.onchange(e);
          }}
          placeholder={value.placeholder || ""}
          disabled={value.disabled}
        />
      );

    case INPUT_PASSWORD_TYPES:
      return (
        <Input.Password
          id={value.id}
          className="h-[38px]"
          style={{ width: "100%" }}
          placeholder={value.placeholder || ""}
          onChange={(e) => {
            value.onchange(e);
          }}
          disabled={value.disabled}
          iconRender={(visible) =>
            visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
          }
        />
      );

    case CHECKBOX_TYPES:
      return (
        <Checkbox.Group
          className="h-[38px]"
          style={{
            display: "flex",
            flexDirection: value.vertical ? "column" : "row",
            width: "100%",
          }}
          disabled={value.disabled}
          options={value.options || []}
          onChange={(e: any) => {
            value.onchange(e);
          }}
        />
      );
    case SWITCH_TYPES:
      return (
        <Switch
          id={value.id}
          disabled={value.disabled}
          onChange={(e: any) => {
            value.onchange(e);
          }}
        />
      );

    case RADIO_TYPES:
      return (
        <Radio.Group
          optionType={value.optionType == "button" ? "button" : "default"}
          onChange={(e: any) => {
            value.onchange(e);
          }}
        >
          <Space direction={value.vertical ? "vertical" : "horizontal"}>
            {value.options?.map((items: any, index: number) => {
              return (
                <Radio key={index} value={items.value}>
                  {items.label}
                </Radio>
              );
            })}
          </Space>
        </Radio.Group>
      );

    case COLOR_PICKER_TYPES:
      return (
        <ColorPicker
          className="h-[38px]"
          showText={value.showText}
          disabled={value.disabled}
          onChange={(e: any) => {
            value.onchange(e);
          }}
        />
      );
    default:
      return null;
  }
};
