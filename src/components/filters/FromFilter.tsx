import React from "react";
import FormRender from "../forms/FormRender";
import { Button, Form, Popover } from "antd";
import { FilterOutlined } from "@ant-design/icons";
import { checkStaffAuth } from "../../../constants/permissions";

type Props = {};

const FromFilter = ({
  form,
  fieldFilterDefault,
  fieldFilterMore,
  handleResetFilter,
  accountInfo,
  unfilterMore = false,
}: any) => {
  
  const content = (
    <>
      <div className=" ">
        <FormRender FieldList={fieldFilterMore} form={form} />
      </div>
    </>
  );
  const handleReset = () => {
    form.resetFields();
    handleResetFilter();
  };
  return (
    <div className="flex items-center">
      <div className=" w-[100%] ">
        <FormRender FieldList={fieldFilterDefault} form={form} />
      </div>
     
       <div className="ml-[20px] flex items-center">
       {checkStaffAuth(accountInfo) && !unfilterMore ? (
         <Popover
         className="ml-[16px]"
         placement="bottomRight"
         content={content}
         title="Bộ lọc"
         trigger="click"
         overlayStyle={{ width: "500px" }}
       >
         <Button className=" !text-blue-500 border-blue-500  ml-[20px] text-[12px]  rounded-md h-[36px] ml-[4px] flex items-center">
           <FilterOutlined />
           Thêm lọc
         </Button>
       </Popover>
      ) : null}
        
          <Button onClick={handleReset} className="ml-6" danger type="text">
            Bỏ lọc
          </Button>
        </div>
    </div>
  );
};

export default FromFilter;
