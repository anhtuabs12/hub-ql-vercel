import {
  CHECKBOX_TYPES,
  COLOR_PICKER_TYPES,
  INPUT_DATE_PICKER_TYPES,
  INPUT_DEFAULT_TYPES,
  INPUT_NUMBER_TYPES,
  INPUT_PASSWORD_TYPES,
  INPUT_TEXTAREA_TYPES,
  RADIO_TYPES,
  SELECT_TYPES,
  SWITCH_TYPES,
} from "@/components/forms/FromType";
import { HotlineStatusList, NetWorkList, Role, StatusBrandname, StatusTicket, TicketType } from "../../../constants/config";
import { LOG_TYPE, SORTDATE_TYPE } from "../../../constants/enum";
import { checkStaffAuth } from "../../../constants/permissions";
import { accountListRequest } from "../../../redux/actions/accountAction";
import { formatSelecter_Form } from "../../../constants/formats";

export const FilterKeyword = (Datas: any) => {
  const form = Datas.form;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    fields: [
      {
        span: 8,
        type: INPUT_DEFAULT_TYPES,
        label: "Tên",
        onchange: (e: any) => {
          form.setFieldValue("ho", "bui anh");
        },
        name: "name",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên",
      },
      {
        type: INPUT_DEFAULT_TYPES,
        span: 8,
        label: "Họ",
        name: "ho",
        required: true,
        message: "Không nên bỏ trống",
        placeholder: "Nhập họ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        onchange: (e: any) => {
          form.setFieldValue("name", "tu");
        },
        label: "Tên đệm",
        name: "tendem",
        required: false,
        options: [{ value: "tu", label: "Tu" }],
        message: "Không nên bỏ trống",
        placeholder: "Nhập họ",
      },
      {
        type: SELECT_TYPES,
        span: 8,
        mode: "multiple",
        label: "kĩ năng",
        name: "skill",
        required: false,
        options: [
          { value: "1", label: "reactjs" },
          { value: "2", label: "nodejs" },
          { value: "3", label: "nextjs" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Nhập ",
      },
      {
        type: INPUT_NUMBER_TYPES,
        span: 8,
        label: "Tuổi",
        name: "age",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập Tuổi",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        span: 8,
        label: "Ngày sinh",
        name: "ngaysinh",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập ngày sinh",
      },

      {
        type: INPUT_PASSWORD_TYPES,
        span: 8,
        label: "Mật khẩu",
        name: "password",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập ngày mật khẩu",
      },

      {
        type: CHECKBOX_TYPES,
        vertical: false,
        options: [
          { value: "nu", label: "Nữ" },
          { value: "nam", label: "Nam" },
          { value: "namnu", label: "Không xác định" },
        ],
        span: 8,
        label: "Giới tính",
        name: "gioitinh",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: SWITCH_TYPES,
        span: 8,
        label: "Đã đi nghĩa vụ quân sự",
        name: "nghiaVuQuanSu",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: RADIO_TYPES,
        optionType: "button",
        span: 8,
        label: "Nơi sinh",
        name: "noisinh",
        required: false,
        options: [
          { value: "hanoi", label: "Hà Nội" },
          { value: "hcm", label: "Hồ Chí Minh" },
          { value: "namdinh", label: "Nam Định" },
        ],
        message: "Không nên bỏ trống",
        vertical: false,
      },
      {
        type: COLOR_PICKER_TYPES,
        span: 24,
        showText: true,
        defaultValue: "#0079FF",
        label: "Màu sắc",
        name: "mausac",
        required: false,
        message: "Không nên bỏ trống",
      },
      {
        type: INPUT_TEXTAREA_TYPES,
        span: 24,
        label: "Nội dung",
        name: "noidung",
        required: false,
        message: "Không nên bỏ trống",
      },
    ],
  };
};

export const FilterHotlineSort = (Datas: any) => {
  let accountInfo = Datas.accountInfo
  const form = Datas.form;
  const onchange = Datas.onchange;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        span: 4,
        type: INPUT_DEFAULT_TYPES,
        label: "Hotline:",
        onchange: (e: any) => {
          onchange("hotline", e.target.value, form.getFieldsValue());
        },
        name: "hotline",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên",
      },

      {
        type: SELECT_TYPES,
        span: 5,
        onchange: (e: any) => {
          onchange("Type_NetWorks", e, form.getFieldsValue());
        },
        mode: "multiple",
        label: "Nhà mạng",
        name: "Type_NetWorks",
        required: false,
        options: NetWorkList,
        message: "Không nên bỏ trống",
        placeholder: "Nhà mạng",
      },
      {
        type: SELECT_TYPES,
        span: 12,
        onchange: (e: any) => {
          onchange("Statuslist", e, form.getFieldsValue());
        },
        mode: "multiple",
        label: "Trạng thái số:",
        name: "Statuslist",
        required: false,
        disabled: !checkStaffAuth(accountInfo),
        options: HotlineStatusList ,
        message: "Không nên bỏ trống",
        placeholder: "Trạng thái số",
      },
      {
        type: SELECT_TYPES,
        span: 3,
        onchange: (e: any) => {
          onchange("Times", e, form.getFieldsValue());
        },
        label: "Thời gian",
        name: "Times",
        required: false,
        options: [
          { value: SORTDATE_TYPE.DAY, label: "Trong ngày" },
          { value: SORTDATE_TYPE.WEEK, label: "Tuần này" },
          { value: SORTDATE_TYPE.MONTH, label: "Tháng này" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Thời gian",
      },
    ],
  };
};

export const FilterlogsSort = (Datas: any) => {
  let Hotline = Datas.Hotline
  const form = Datas.form;
  const onchange = Datas.onchange;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields:  [
      {
        span: 4,
        type: INPUT_DEFAULT_TYPES,
        label: "Đầu số",
        onchange: (e: any) => {
          onchange("hotline", e.target.value, form.getFieldsValue());
        },
        name: "Hotline",
        disabled: Hotline ,
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập tên",
      },
      {
        type: SELECT_TYPES,
        span: 5,
        onchange: (e: any) => {
          onchange("Times", e, form.getFieldsValue());
        },
        label: "Loại tác động",
        name: "Type",
        required: false,
        options: [
          { value: LOG_TYPE.CREATE, label: "Tạo mới" },
          { value: LOG_TYPE.UPDATE, label: "Cập nhật" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Loại tác động",
      },
      {
        type: SELECT_TYPES,
        span: 3,
        onchange: (e: any) => {
          onchange("Times", e, form.getFieldsValue());
        },
        label: "Thời gian",
        name: "Times",
        required: false,
        options: [
          { value: SORTDATE_TYPE.DAY, label: "Trong ngày" },
          { value: SORTDATE_TYPE.WEEK, label: "Tuần này" },
          { value: SORTDATE_TYPE.MONTH, label: "Tháng này" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Thời gian",
      },
    ],
  };
};







export const FilterHotlneMore = (Datas: any) => {
  const form = Datas.form;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        type: SELECT_TYPES,
        span: 24,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: "Khách hàng",
        name: "CustomerCodes",
        required: false,
        options: [
          { value: "0967671182", label: "Anhtu" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Khách hàng",
      },

      {
        type: INPUT_DATE_PICKER_TYPES,
        showTime: true,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: "Từ ngày",
        name: "form_date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Ngày bắt đầu",
      },

      {
        type: INPUT_DATE_PICKER_TYPES,
        showTime: true,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: "Đến ngày",
        name: "to_date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Ngày kết thúc",
      },
      // {
      //   type: SELECT_TYPES,
      //   span: 3,
      //   onchange: (e: any) => {
      //     // form.setFieldValue("name", "tu");
      //   },
      //   label: "Thời gian",
      //   name: "tendem",
      //   required: false,
      //   options: [
      //     { value: "tu", label: "Trong ngày" },
      //     { value: "tu2", label: "Tuần này" },
      //     { value: "tu3", label: "Tháng này" },
      //   ],
      //   message: "Không nên bỏ trống",
      //   placeholder: "Thời gian",
      // },
    ],
  };
};
export const FilterAccountSort = (Datas: any) => {
  const form = Datas.form;
  const onchange = Datas.onchange;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        span: 8,
        type: INPUT_DEFAULT_TYPES,
        label: "Email:",
        onchange: (e: any) => {
          onchange("Email_Staff", e.target.value, form.getFieldsValue());
        },
        name: "Email_Staff",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Email",
      },

      {
        type: SELECT_TYPES,
        span: 5,
        onchange: (e: any) => {
          onchange("Role_Staff", e, form.getFieldsValue());
        },
        mode: "multiple",
        label: "Vai trò:",
        name: "Role_Staff",
        required: false,
        options: Role,
        message: "Không nên bỏ trống",
        placeholder: "Vai trò",
      },
      {
        type: SELECT_TYPES,
        span: 5,
        onchange: (e: any) => {
          onchange("Times", e, form.getFieldsValue());
        },
        label: "Thời gian",
        name: "Times",
        required: false,
        options: [
          { value: SORTDATE_TYPE.DAY, label: "Trong ngày" },
          { value: SORTDATE_TYPE.WEEK, label: "Tuần này" },
          { value: SORTDATE_TYPE.MONTH, label: "Tháng này" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Thời gian",
      },
    ],
  };
};
export const FilterBrandnameSort = (Datas: any) => {
  const form = Datas.form;
  const onchange = Datas.onchange;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        span: 5,
        type: INPUT_DEFAULT_TYPES,
        label: "Số giấy chứng nhận:",
        onchange: (e: any) => {
          onchange("Certificate_number", e.target.value, form.getFieldsValue());
        },
        name: "Certificate_number",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Số giấy chứng nhận",
      },
      {
        span: 5,
        type: INPUT_DEFAULT_TYPES,
        label: "Công ty:",
        onchange: (e: any) => {
          onchange("Number_company", e.target.value, form.getFieldsValue());
        },
        name: "Number_company",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Công ty",
      },

      {
        type: SELECT_TYPES,
        span: 5,
        onchange: (e: any) => {
          onchange("Brandname_Status", e, form.getFieldsValue());
        },
        mode: "multiple",
        label: "Trạng thái:",
        name: "Brandname_Status",
        required: false,
        options: StatusBrandname,
        message: "Không nên bỏ trống",
        placeholder: "Trạng thái",
      },
      {
        span: 5,
        type: INPUT_DEFAULT_TYPES,
        label: "Brandname:",
        onchange: (e: any) => {
          onchange("Brandname", e.target.value, form.getFieldsValue());
        },
        name: "Brandname",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Brandname",
      },
      {
        type: SELECT_TYPES,
        span: 3,
        onchange: (e: any) => {
          onchange("Times", e, form.getFieldsValue());
        },
        label: "Thời gian",
        name: "Times",
        required: false,
        options: [
          { value: SORTDATE_TYPE.DAY, label: "Trong ngày" },
          { value: SORTDATE_TYPE.WEEK, label: "Tuần này" },
          { value: SORTDATE_TYPE.MONTH, label: "Tháng này" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Thời gian",
      },
    ],
  };
};
export const FilterBrandnameMore = (Datas: any) => {
  const form = Datas.form;
  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      // {
      //   type: INPUT_DATE_PICKER_TYPES,
      //   showTime: true,
      //   span: 12,
      //   onchange: (e: any) => {
      //     // form.setFieldValue("name", "tu");
      //   },
      //   mode: "multiple",
      //   label: "Ngày hoạt động",
      //   name: "Lunch_date",
      //   required: false,
      //   message: "Không nên bỏ trống",
      //   placeholder: "Ngày hoạt động",
      // },

      // {
      //   type: INPUT_DATE_PICKER_TYPES,
      //   showTime: true,
      //   span: 12,
      //   onchange: (e: any) => {
      //     // form.setFieldValue("name", "tu");
      //   },
      //   mode: "multiple",
      //   label: "Ngày hết hạn",
      //   name: "Expiry_date",
      //   required: false,
      //   message: "Không nên bỏ trống",
      //   placeholder: "Ngày hết hạn",
      // },
      {
        type: INPUT_DATE_PICKER_TYPES,
        showTime: true,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: "Ngày hoạt động",
        name: "Lunch_form_date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Từ ngày",
      },

      {
        type: INPUT_DATE_PICKER_TYPES,
        showTime: true,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: " ",
        name: "Lunch_to_date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Đến ngày",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        showTime: true,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: "Ngày hết hạn",
        name: "Expiry_form_date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Từ ngày",
      },

      {
        type: INPUT_DATE_PICKER_TYPES,
        showTime: true,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: " ",
        name: "Expiry_to_date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Đến ngày",
      },
      {
        span: 12,
        type: INPUT_DEFAULT_TYPES,
        label: "Số lượng Hotline",
        onchange: (e: any) => {
          
        },
        name: "Hotline_num",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Số lượng Hotline",
      },
      
    ],
  };
};

export const FilterTicketSort = (Datas: any) => {
  const form = Datas.form;
  const onchange = Datas.onchange;

  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        span: 5,
        type: INPUT_DEFAULT_TYPES,
        label: "Hotline:",
        onchange: (e: any) => {
          onchange("Hotline", e.target.value, form.getFieldsValue());
        },
        name: "Hotline",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập hotline",
      },
      {
        span: 5,
        type: INPUT_DEFAULT_TYPES,
        label: "Mã Ticket:",
        onchange: (e: any) => {
          onchange("Ticket_Code", e.target.value, form.getFieldsValue());
        },
        name: "Ticket_Code",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Nhập mã Ticket",
      },

      {
        type: SELECT_TYPES,
        span: 6,
        onchange: (e: any) => {
          console.log(123123, form)
          onchange("Ticket_Type", e, form.getFieldsValue());
        },
        mode: "multiple",
        label: "Loại Ticket",
        name: "Ticket_Type",
        required: false,
        options: TicketType,
        message: "Không nên bỏ trống",
        placeholder: "Chọn loại Ticket",
      },
      {
        type: SELECT_TYPES,
        span: 5,
        onchange: (e: any) => {
          onchange("Ticket_Status", e, form.getFieldsValue());
        },
        mode: "multiple",
        label: "Trạng thái Ticket:",
        name: "Ticket_Status",
        required: false,
        options: StatusTicket,
        message: "Không nên bỏ trống",
        placeholder: "Trạng thái Ticket",
      },
      {
        type: SELECT_TYPES,
        span: 3,
        onchange: (e: any) => {
          onchange("Times", e, form.getFieldsValue());
        },
        label: "Thời gian",
        name: "Times",
        required: false,
        options: [
          { value: SORTDATE_TYPE.DAY, label: "Trong ngày" },
          { value: SORTDATE_TYPE.WEEK, label: "Tuần này" },
          { value: SORTDATE_TYPE.MONTH, label: "Tháng này" },
        ],
        message: "Không nên bỏ trống",
        placeholder: "Thời gian",
      },
    ],
  };
};

export const FilterTicketMore = (Datas: any) => {
  const form = Datas.form;
  const accountAll = Datas?.accountAll;
  return {
    onfinishfailed: Datas.onFinishFailed,
    onfinish: Datas.onFinish,
    submitBtn: Datas.submitBtn,
    fields: [
      {
        type: SELECT_TYPES,
        span: 24,
        onchange: async (e: any) => {
        },
        mode: "multiple",
        label: "Tài khoản gửi ticket",
        name: "Ticket_Tenant_code",
        required: false,
        options: accountAll?.map((value:any)=> formatSelecter_Form(value.Code, value.Email)),
        message: "Không nên bỏ trống",
        placeholder: "Tài khoản gửi",
      },
      {
        type: SELECT_TYPES,
        span: 24,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: "Tài khoản duyệt ticket",
        name: "Ticket_ApproveBy_code",
        required: false,
        options:accountAll?.map((value:any)=> formatSelecter_Form(value.Code, value.Email)),
        message: "Không nên bỏ trống",
        placeholder: "Tài khoản duyệt",
      },
      {
        type: INPUT_DATE_PICKER_TYPES,
        showTime: true,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: "Từ ngày",
        name: "form_date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Ngày bắt đầu",
      },

      {
        type: INPUT_DATE_PICKER_TYPES,
        showTime: true,
        span: 12,
        onchange: (e: any) => {
          // form.setFieldValue("name", "tu");
        },
        mode: "multiple",
        label: "Đến ngày",
        name: "to_date",
        required: false,
        message: "Không nên bỏ trống",
        placeholder: "Ngày kết thúc",
      },
    ],
  };
};
