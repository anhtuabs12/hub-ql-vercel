import { Spin } from 'antd'
import React from 'react'

type Props = {}

const LoadingDefault = (props: Props) => {
  return (
    <div className='w-full h-full flex items-center justify-center'>
         <Spin  />
    </div>
  )
}

export default LoadingDefault