import React, { useEffect, useState, useMemo } from "react";
import { Menu, MenuProps } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import {
  AppstoreOutlined,
  BarsOutlined,
  BookOutlined,
  ClockCircleOutlined,
  ContactsOutlined,
  DesktopOutlined,
  PhoneOutlined,
  PieChartOutlined,
  TeamOutlined,
  UnorderedListOutlined,
  UserOutlined,
} from "@ant-design/icons";

type MenuItem = Required<MenuProps>["items"][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: "group"
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const MenuCpn = ({ collapsed }: any) => {
  const [defaultSelectedKeys, setDefaultSelectedKeys] = useState(["1"]);
  const [openKeys, setOpenKeys] = useState<string[]>([]);

  const findOpenKeys = (items: any[], selectedKey: string): string[] => {
    const openKeys: string[] = [];

    const findKeyInItems = (items: any[], selectedKey: string): boolean => {
      for (const item of items) {
        if (item.key === selectedKey) {
          return true;
        }
        if (item.children && findKeyInItems(item.children, selectedKey)) {
          openKeys.push(item.key as string);
          return true;
        }
      }
      return false;
    };

    findKeyInItems(items, selectedKey);
    return openKeys;
  };

  const router = useRouter();

  const items = useMemo(
    () => [
      getItem(
        <>
          <Link href={"/"}>Tổng quan</Link>
        </>,
        "/",
        <DesktopOutlined />
      ),

      getItem(
        <>
          <Link href={"/admin/hotline"}>Kho số</Link>
        </>,
        "/admin/hotline",
        <PhoneOutlined />
      ),
      getItem(
        <>
          <Link href={"/admin/ticket"}>Ticket</Link>
        </>,
        "/admin/ticket",
        <BarsOutlined />
      ),
      getItem(
        <>
          <Link href={"/admin/brandname"}>Brandname</Link>
        </>,
        "/admin/brandname",
        <ContactsOutlined />
      ),
      getItem(
        <>
          <Link href={"/admin/log"}>Lịch sử tác động</Link>
        </>,
        "/admin/log",
        <ClockCircleOutlined />
      ),
      getItem(
        <>
          <Link href={"/admin/rule"}>Rule cảnh báo kho số</Link>
        </>,
        "/admin/rule",
        <BookOutlined />
      ),
      getItem(
        <>
          <Link href={"/admin/account"}>Tài khoản</Link>
        </>,
        "/admin/account",
        <TeamOutlined />
      ),

      // getItem("Cấu hình", "sub1", <AppstoreOutlined />, [
      //   getItem(
      //     <>
      //       <Link href={"/admin/category"}>Danh mục</Link>
      //     </>,
      //     "/admin/category",
      //     <AppstoreOutlined />
      //   ),
      // ]),

      // getItem("Cài đặt", "setting", <AppstoreOutlined />, [
      //   getItem(
      //     <>
      //       <Link href={"/staff"}>Nhân viên</Link>
      //     </>,
      //     "/staff",
      //     <AppstoreOutlined />
      //   ),
      //   getItem(
      //     <>
      //       <Link href={"/permission"}>Phân quyền</Link>
      //     </>,
      //     "/permission",
      //     <AppstoreOutlined />
      //   ),
      // ]),
    ],
    []
  );

  useEffect(() => {
    setDefaultSelectedKeys([`${router.route}`]);
    setOpenKeys(findOpenKeys(items, router.route));
  }, [router, items]);

  const handleOpenChange = (keys: string[]) => {
    setOpenKeys(keys);
  };

  return (
    <div
      style={{
        height: "calc(100vh - 68px)",
        overflow: "auto",
        borderRight: "1px rounded-custom1 solid rgba(5, 5, 5, 0.06)",
      }}
      className={`${
        collapsed ? "w-fit" : "w-[256px]"
      } !overflow-x-hidden duration-300`}
    >
      <Menu
        selectedKeys={defaultSelectedKeys}
        openKeys={openKeys}
        mode="inline"
        // theme="dark"
        inlineCollapsed={collapsed}
        items={items}
        onOpenChange={handleOpenChange}
      />
    </div>
  );
};

export default MenuCpn;
