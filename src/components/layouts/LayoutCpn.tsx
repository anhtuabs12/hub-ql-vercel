import React, { useEffect, useState } from 'react'
import HeaderCpn from './HeaderCpn';
import MenuCpn from './MenuCpn';
import { io } from 'socket.io-client';
import { useSelector } from 'react-redux';

type Props = {}

const LayoutCpn = ({children}: any) => {

    const [collapsed, setCollapsed] = useState<boolean>(false);
    // const { accountInfo } = useSelector((state: any) => state.authReducer);

    // useEffect(()=>{
    //   if(!accountInfo){
        
    //   }
    // },[accountInfo])

  return (
    <div className='w-screen h-screen'>  
        <HeaderCpn setCollapsed={setCollapsed}  collapsed={collapsed}/>
        <div className='flex items-start '>
            <MenuCpn  collapsed={collapsed}/>
            <div style={{ height: "calc(100vh - 60px)", overflow: "auto"}} className='duration-300 bg-[#fafafb]  overflow-hidden p-[12px] w-screen'>
                <div style={{ height: "calc(100vh - 84px)", overflow: "auto"}} className='bg-white rounded-[4px] overflow-auto border border-[#f0f0f0] p-[20px]'>
                {children}
                </div>
            </div>
        </div>
    </div>
  )
}

export default LayoutCpn