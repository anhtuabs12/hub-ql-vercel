import {
  BellOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  SettingOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Avatar, Button, Dropdown, MenuProps, Space } from "antd";
import Link from "next/link";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { logoutRequest } from "../../../redux/actions/authActions";

type Props = {};

const HeaderCpn = ({ collapsed, setCollapsed }: any) => {
  const { accountInfo } = useSelector((state: any) => state.authReducer);
  const dispatch = useDispatch();
  const router = useRouter();

  // useEffect(() => {
  //   if (!accountInfo) {
  //     dispatch(
  //       detail_accountRequest({
  //         callbackSuccess: () => {

  //         },
  //       })
  //     );
  //   }
  // }, []);

  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };

  const handleLogout = () => {
    dispatch(
      logoutRequest({
        callback: () => {
          router.push("/login");
        },
      })
    );
  };

  const items: MenuProps["items"] = [
    {
      label: <div onClick={handleLogout}>Đăng xuất</div>,
      key: "1",
    },
  ];

  return (
    <div className="h-[60px] flex items-center justify-between bg-white shadow-sm border-b rounded-custom1 flex items-center w-full justify-between px-8">
      <div
        className={`flex items-center justify-between w-[234px] duration-300 ${
          !collapsed ? "w-[234px]" : "w-[94px]"
        }`}
      >
        <Link href={"/"}>
          <img
            src="/logo.png"
            className={`  ${
              !collapsed ? "w-[52px] mr-[20px]" : "w-[32px]"
            } duration-300`}
            alt=""
          />
        </Link>
        <div
          //   type="primary"
          className="cursor-pointer hover:scale-[1.1] duration-300"
          onClick={toggleCollapsed}
          //   style={{ marginBottom: 16 }}
        >
          {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        </div>
      </div>
      {/* Notification */}
      <div className="flex items-center">
        <div className="mr-[12px]">
          <BellOutlined className="cursor-pointer" />
        </div>

        <div className="flex items-center  px-[12px] py-[6px]">
          <div className="flex items-center mr-[12px]">
            <Avatar icon={<UserOutlined />} className="mr-[16px]" />
            <div>
              <h3 className="text-[14px] font-semibold">
                {accountInfo?.FullName || accountInfo?.enterprise_number}
              </h3>
              <h3 className="text-[12px]">
                {accountInfo?.Email || accountInfo?.companyname}
              </h3>
            </div>
          </div>
          <Dropdown menu={{ items }} trigger={["click"]}>
            <a
              className="cursor-pointer hover:scale-[1.2] hover:rotate-90 duration-300"
              onClick={(e) => e.preventDefault()}
            >
              <Space>
                <SettingOutlined className="text-[20px] mt-[5px]" />
              </Space>
            </a>
          </Dropdown>
        </div>
      </div>
      {/* accountinfo */}
    </div>
  );
};

export default HeaderCpn;
