import React from 'react'

type Props = {
    children:any
}

const TitlePage = ({children}: Props) => {
  return (
    <h1 className='text-[22px] font-semibold text-[#1b3051] mb-[26px]'>{children}</h1>
  )
}

export default TitlePage