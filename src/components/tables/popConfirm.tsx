import React from 'react';
import type { PopconfirmProps } from 'antd';
import { Button, message, Popconfirm } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';



const PopConfirms = ({id, HandleDelete}:any) => {
    const confirm: PopconfirmProps['onConfirm'] = (e) => {
        HandleDelete(id);
        message.success('Xóa thành công');
      };
      
      const cancel: PopconfirmProps['onCancel'] = (e) => {
        message.error('Click on No');
      };
    return (
        <Popconfirm
          title="Delete the task"
          description="Ban có muốn xóa không?"
          onConfirm={confirm}
          onCancel={cancel}
          okText="Yes"
          cancelText="No"
        >
          
          <DeleteOutlined className="text-red-500 text-[16px] ml-[8px] cursor-pointer hover:text-red-800  hover:scale-[1.1]" />
        </Popconfirm>
    );
}

export default PopConfirms;