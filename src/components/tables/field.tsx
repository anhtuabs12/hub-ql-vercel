import {
  DeleteOutlined,
  EditOutlined,
  HistoryOutlined,
} from "@ant-design/icons";
import TableEditer from "./TableEditer";
import { formatDateTime } from "../../../constants/formats";
import { Popconfirm, Typography } from "antd";
import {
  HotlineStatusList,
  logTypeKey,
  NetWorkList,
  TypeLogs,
} from "../../../constants/config";
import {
  BRANDNAMESTATUS_TYPE,
  TICKETSTATUS_TYPE,
  TICKET_TYPE,
  statusColors,
} from "../../../constants/enum";
import { checkStaffAuth } from "../../../constants/permissions";

export const ColunmRule = (DataColums?: any) => {
  let isEditing = DataColums?.isEditing;
  let save = DataColums?.save;
  let cancel = DataColums?.cancel;
  let editingKey = DataColums?.editingKey;
  let edit = DataColums?.edit;
  return [
    {
      title: "STT",
      dataIndex: "Ordernumber",
      key: "Ordernumber",
      align: "center",
      render: (text: any, record: any, index: any) => index + 1,
    },
    {
      title: "Tiêu đề",
      dataIndex: "Title",
      key: "Title",
      align: "center",
      editable: true,
    },
    {
      title: "Giá trị",
      dataIndex: "Value",
      key: "Value",
      align: "center",
      editable: true,
    },
    {
      title: "Loại",
      dataIndex: "Type",
      key: "Type",
      width: 160,
      render: (value: any) => {
        return <div className="">{value}</div>;
      },
    },
    {
      title: "Ngày tạo",
      dataIndex: "Created_at",
      key: "Created_at",
      align: "center",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },

    {
      title: "Ngày sửa",
      dataIndex: "Updated_at",
      key: "Updated_at",
      align: "center",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Thao tác",
      dataIndex: "operation",
      render: (_: any, record: any) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.key)}
              style={{ marginRight: 8 }}
            >
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link
            disabled={editingKey !== ""}
            onClick={() => edit(record)}
          >
            Edit
          </Typography.Link>
        );
      },
    },
  ];
};

export const ColunmAccount = (DataColums?: any) => {
  let handleEdit = DataColums?.HandleEdit;
  let handleDelete = DataColums?.HandleDelete;
  return [
    {
      title: "STT",
      dataIndex: "STT",
      key: "STT",
      width: 40,
      render: (text: any, record: any, index: any) => index + 1,
    },
    {
      title: "Mã nhân viên",
      dataIndex: "Code",
      key: "Code",
      width: 100,
    },
    {
      title: "Tên nhân viên",
      dataIndex: "FullName",
      key: "FullName",
      width: 120,
    },
    {
      title: "Email",
      dataIndex: "Email",
      key: "Email",
      width: 120,
    },
    {
      title: "Ngày tạo",
      dataIndex: "Created_at",
      key: "Created_at",
      width: 120,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Vai trò",
      dataIndex: "Role",
      key: "Role",
      width: 120,
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      fixed: "right",
      width: 50,
      render: (value: any, data: any) => {
        return (
          <TableEditer
            popUp={true}
            HandleEdit={handleEdit}
            HandleDelete={handleDelete}
            code={data.Code}
          />
        );
      },
    },
  ];
};

export const ColumnsAccountCustomer = (DataColums?: any) => {
  let modalStatus = DataColums?.modalStatus;
  let handleEdit = DataColums?.HandleEdit;
  let handleDelete = DataColums?.HandleDelete;
  return [
    {
      title: "STT",
      dataIndex: "STT",
      key: "STT",
      width: 40,
      render: (text: any, record: any, index: any) => index + 1,
    },
    {
      title: "Số đại diện",
      dataIndex: "enterprise_number",
      key: "enterprise_number",
      width: 120,
    },
    {
      title: "Tên khách hàng",
      dataIndex: "companyname",
      key: "companyname",
      width: 120,
    },
    {
      title: "Ngày tạo",
      dataIndex: "Created_at",
      key: "Created_at",
      width: 120,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Vai trò",
      dataIndex: "Role",
      key: "Role",
      width: 120,
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      fixed: "right",
      width: 50,
      render: (value: any, data: any) => {
        return (
          <TableEditer
            popUp={true}
            HandleEdit={handleEdit}
            HandleDelete={handleDelete}
            code={data.Code}
          />
        );
      },
    },
  ];
};

export const ColunmHotline = (DataColums?: any) => {
  let accountInfo = DataColums?.accountInfo;
  let handleEdit = DataColums?.HandleEdit;
  let handleDelete = DataColums?.HandleDelete;
  let deletedAction = DataColums?.deletedAction;
  let handleShowLog = DataColums?.handleShowLog;
  return [
    {
      title: "STT",
      dataIndex: "STT",
      key: "STT",
      width: 60,
      fixed: "left",
      // render: (text: any, record: any, index: any) => record.key + 1,
    },
    {
      title: "Hotline",
      dataIndex: "Hotline",
      key: "Hotline",
      fixed: "left",
      width: 150,
      render(text:any, record:any) {
        return {
          props: {
            style: { background: parseInt(text) > 50 ? "#fffb34" : "green" }
          },
          children: <div>{text}</div>
        };
      }
    },
    {
      title: "Nhà mạng",
      dataIndex: "Type_NetWork",
      key: "Type_NetWork",
      align: "center",
      width: 150,
      render: (value: any) => {
        return NetWorkList.find((item: any) => item.value == value)?.label;
      },
      // fixed: "left",
    },

    {
      title: "Khách hàng",
      dataIndex: ["CustomerInfo", "companyname"],
      hidden: !checkStaffAuth(accountInfo),
      align: "center",
      key: ["CustomerInfo", "companyname"],
      width: 200,
    },
    {
      title: "Ngày cấp cho khách hàng",
      dataIndex: "Lunch_date",
      align: "center",
      key: "Lunch_date",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Brand name",
      dataIndex: ["BrandnameInfo", "Brandname"],
      align: "center",
      key: ["BrandnameInfo", "Brandname"],
      width: 200,
    },
    {
      title: "Ngày mua",
      dataIndex: "PurchaseDate",
      align: "center",
      key: "PurchaseDate",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Giữ đến ngày",
      dataIndex: "Extention_periodDate",
      align: "center",
      key: "Extention_periodDate",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },

    {
      title: "Khách hàng cuối",
      dataIndex: "Customer_end",
      key: "Customer_end",
      width: 200,
    },
    {
      title: "Ngày tạo",
      dataIndex: "Created_at",
      align: "center",
      key: "Created_at",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Ngày sửa",
      dataIndex: "Updated_at",
      align: "center",
      key: "Updated_at",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Trạng thái",
      dataIndex: "Status",
      align: "center",
      key: "Status",
      width: 150,
      fixed: "right",
      render: (value: any) => {
        const backgroundColor = statusColors[value] || "#FFFFFF";
        return (
          <span
            className="text-xs shadow-md"
            style={{
              backgroundColor,
              padding: "5px 8px",
              borderRadius: "5px",
              color: "#FFFFFF",
            }}
          >
            {HotlineStatusList.find((item: any) => item.value == value)?.label}
          </span>
        );
      },
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      hidden: !checkStaffAuth(accountInfo),
      key: "action",
      align: "center",
      fixed: "right",
      width: 100,
      render: (value: any, data: any) => {
        return (
          <div className="flex items-center justify-center">
            <HistoryOutlined
              onClick={() => {
                handleShowLog(data.Hotline);
              }}
              className="mr-4 hover:text-blue-500 cursor-pointer	text-[18px] duration-300 "
            />
            <TableEditer
              deletedAction={deletedAction}
              popUp={true}
              HandleEdit={handleEdit}
              HandleDelete={handleDelete}
              code={data.Code}
            />
          </div>
        );
      },
    },
  ];
};

export const ColunmBrandname = (DataColums?: any) => {
  let handleEdit = DataColums?.HandleEdit;
  let handleDelete = DataColums?.HandleDelete;
  return [
    {
      title: "STT",
      dataIndex: "STT",
      key: "STT",
      width: 80,
      render: (text: any, record: any, index: any) => index + 1,
    },
    {
      title: "Số giấy chứng nhận",
      dataIndex: "Certificate_number",
      key: "Certificate_number",
      width: 200,
    },
    {
      title: "Công ty",
      dataIndex: "Enterprise_number_company",
      key: "Enterprise_number_company",
      width: 200,
      // render: (value: any) => {
      //   return value + ", ";
      // },
    },
    {
      title: "Trạng thái",
      dataIndex: "Status",
      key: "Status",
      width: 120,
      render: (value: any) => {
        if (value == BRANDNAMESTATUS_TYPE.Pause) {
          return "Tạm ngưng";
        } else {
          return "Kích hoạt";
        }
      },
    },
    {
      title: "Brandname",
      dataIndex: "Brandname",
      key: "Brandname",
      width: 200,
      // render: (value: any) => {
      //   return DataColums.accountAll.find((code: any) => {
      //     return value == code.Code;
      //   })?.Email;
      // },
    },
    {
      title: "Ngày hoạt động",
      dataIndex: "Lunch_date",
      key: "Lunch_date",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Ngày hết hạn",
      dataIndex: "Expiry_date",
      key: "Expiry_date",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Mã Brandname",
      dataIndex: "Code",
      key: "Code",
      width: 200,
    },
    {
      title: "Ngày tạo",
      dataIndex: "Created_at",
      key: "Created_at",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Ngày sửa",
      dataIndex: "Updated_at",
      key: "Updated_at",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Ghi chú",
      dataIndex: "Note",
      key: "Note",
      width: 200,
    },
    {
      title: "Số lượng Hotline",
      dataIndex: "hotlineTotal",
      key: "hotlineTotal",
      width: 200,
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      align: "center",
      key: "action",
      fixed: "right",
      width: 200,
      render: (value: any, data: any) => {
        return (
          <div className="flex items-center justify-center">
            <HistoryOutlined className="mr-4 hover:text-blue-500 cursor-pointer	text-[18px] duration-300 " />
            <TableEditer
              popUp={true}
              HandleEdit={handleEdit}
              HandleDelete={handleDelete}
              code={data.Code}
            />
          </div>
        );
      },
    },
  ];
};

export const ColunmTicket = (DataColums?: any) => {
  let handleEdit = DataColums?.HandleEdit;
  let handleDelete = DataColums?.HandleDelete;
  return [
    {
      title: "STT",
      dataIndex: "STT",
      key: "STT",
      width: 80,
      render: (text: any, record: any, index: any) => index + 1,
    },
    {
      title: "Loại",
      dataIndex: "Type",
      key: "Type",
      width: 200,
      render: (value: any) => {
        if (value == TICKET_TYPE.WITHDRAWAL) {
          return "Trạng thái từ đạng sử dụng =>  thu hồi";
        } else if (value == TICKET_TYPE.CANCEL_NUMBER) {
          return "Trạng thái thu hồi =>  hủy số";
        } else {
          return "Trạng thái thu hồi => chưa sử dụng";
        }
      },
    },
    {
      title: "Hotline",
      dataIndex: "Hotlines_codes",
      key: "Hotlines_codes",
      width: 200,
      render: (value: any) => {
        return value + ", ";
      },
    },
    {
      title: "Trạng thái",
      dataIndex: "Status",
      key: "Status",
      width: 120,
      render: (value: any) => {
        // Status: TICKETSTATUS_TYPE.AGREE
        // console.log(value,"value")
        if (value == TICKETSTATUS_TYPE.AGREE) {
          return "Đã xác nhận";
        } else if (value == TICKETSTATUS_TYPE.WAITING) {
          return "Đang chờ";
        } else {
          return "Đã hủy";
        }
      },
    },
    {
      title: "Tài khoản gửi Ticket",
      dataIndex: "Tenant_code",
      key: "Tenant_code",
      width: 200,
      render: (value: any) => {
        return DataColums.accountAll?.find((code: any) => {
          return value == code.Code;
        })?.Email;
      },
    },
    {
      title: "Tài khoản duyệt Ticket",
      dataIndex: "ApproveBy_code",
      key: "ApproveBy_code",
      width: 200,
      render: (value: any) => {
        return DataColums.accountAll?.find((code: any) => {
          return value == code.Code;
        })?.Email;
      },
    },
    {
      title: "Mã Ticket",
      dataIndex: "Code",
      key: "Code",
      width: 200,
    },
    {
      title: "Ngày tạo",
      dataIndex: "Created_at",
      key: "Created_at",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Ngày sửa",
      dataIndex: "Updated_at",
      key: "Updated_at",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      align: "center",
      key: "action",
      fixed: "right",
      width: 200,
      render: (value: any, data: any) => {
        return (
          <div>
            {data.Status == TICKETSTATUS_TYPE.AGREE ? (
              <div className="text-[#00d389] font-semibold">Đã xác nhận</div>
            ) : data.Status == TICKETSTATUS_TYPE.DISAGREE ? (
              <div className="text-[#ff6f74] font-semibold">Đã hủy</div>
            ) : (
              <TableEditer
                popUp={true}
                HandleEdit={handleEdit}
                HandleDelete={handleDelete}
                code={data.Code}
                accountInfo={DataColums.accountInfo}
                actionCustom={DataColums.actionCustom}
              />
            )}
          </div>
        );
      },
    },
  ];
};

export const ColunmLogpage = (DataColums?: any) => {
  let handleEdit = DataColums?.HandleEdit;
  let handleDelete = DataColums?.HandleDelete;
  return [
    {
      title: "STT",
      dataIndex: "STT",
      key: "STT",
      width: 80,
      render: (text: any, record: any, index: any) => index + 1,
    },
    {
      title: "Thời gian",
      dataIndex: "Created_at",
      key: "Created_at",
      fixed: "left",
      width: 200,
      render: (value: any) => formatDateTime(value),
    },
    {
      title: "Người tác động",
      dataIndex: ["Tenant_info", "Email"],
      key: ["Tenant_info", "Email"],
      fixed: "left",
      width: 200,
      // render: (value: any) => formatDateTime(value),
    },
    {
      title: "Role tác động",
      dataIndex: "Tenant_role",
      key: "Tenant_role",
      width: 200,
      // render: (value: any) => formatDateTime(value),
    },

    {
      title: "Loại tác động",
      dataIndex: "Type",
      key: "Type",
      width: 120,
      render: (type: any) => {
        return TypeLogs.find((value: any) => value.value == type)?.label;
      },
    },
    {
      title: "Chi tiết tác động",
      dataIndex: "Update_key",
      key: "Update_key",
      width: 200,
      render: (value: any) => {
        return logTypeKey.filter((item: any) => {
          let check = false
          value.map((val:any) =>{
            check = item.value == val 
          })
          return check
        }).map((e:any)=> e.label).join(",");
      },
    },

    {
      title: "Hiện trạng số sau tác động",
      dataIndex: "Hotline_info",
      key: "Hotline_info",
      width: 200,
      children: [
        {
          title: "Hotline",
          dataIndex: ["Hotline_info", "Hotline"],
          key: ["Hotline_info", "Hotline"],
          width: 200,
        },
        {
          title: "Nhà mạng",
          dataIndex: ["Hotline_info", "Type_NetWork"],
          key: ["Hotline_info", "Type_NetWork"],
          align: "center",
          width: 200,
          render: (value: any) => {
            return NetWorkList.find((item: any) => item.value == value)?.label;
          },
          // fixed: "left",
        },

        {
          title: "Khách hàng",
          dataIndex: ["Hotline_info", "CustomerCode"],
          align: "center",
          key: ["Hotline_info", "CustomerCode"],
          width: 200,
        },
        {
          title: "Ngày cấp cho khách hàng",
          dataIndex: ["Hotline_info", "Lunch_date"],
          align: "center",
          key: ["Hotline_info", "Lunch_date"],
          width: 200,
          render: (value: any) => formatDateTime(value),
        },
        {
          title: "Brand name",
          dataIndex: ["Hotline_info", "BrandName"],
          align: "center",
          key: ["Hotline_info", "BrandName"],
          width: 200,
        },
        {
          title: "Ngày mua",
          dataIndex: ["Hotline_info", "PurchaseDate"],
          align: "center",
          key: ["Hotline_info", "PurchaseDate"],
          width: 200,
          render: (value: any) => formatDateTime(value),
        },
        {
          title: "Giữ đến ngày",
          dataIndex: ["Hotline_info", "Extention_periodDate"],
          align: "center",
          key: ["Hotline_info", "Extention_periodDate"],
          width: 200,
          render: (value: any) => formatDateTime(value),
        },

        {
          title: "Note",
          dataIndex: ["Hotline_info", "Description"],
          key: ["Hotline_info", "Description"],
          width: 200,
        },
        {
          title: "Trạng thái",
          dataIndex: ["Hotline_info", "Status"],
          align: "center",
          key: ["Hotline_info", "Status"],
          width: 150,
          render: (value: any) => {
            const backgroundColor = statusColors[value] || "#FFFFFF";
            return (
              <span
                className="text-xs shadow-md"
                style={{
                  backgroundColor,
                  padding: "5px 8px",
                  borderRadius: "5px",
                  color: "#FFFFFF",
                }}
              >
                {
                  HotlineStatusList.find((item: any) => item.value == value)
                    ?.label
                }
              </span>
            );
          },
        },
      ],
    },

    {
      title: "Mã hotline",
      dataIndex: "Hotline_code",
      key: "Hotline_code",
      width: 200,
      align: "center",
      fixed: "right",
      // render: (value: any) => formatDateTime(value),
    },
    // {
    //   title: "Thao tác",
    //   dataIndex: "action",
    //   align: "center",
    //   key: "action",
    //   fixed: "right",
    //   width: 200,
    //   render: (value: any, data: any) => {
    //     return (
    //       <div>
    //         {data.Status == TICKETSTATUS_TYPE.AGREE ? (
    //           "Đã xác nhận"
    //         ) : data.Status == TICKETSTATUS_TYPE.DISAGREE ? (
    //           "Đã hủy"
    //         ) : (
    //           <TableEditer
    //             popUp={true}
    //             HandleEdit={handleEdit}
    //             HandleDelete={handleDelete}
    //             code={data.Code}
    //             accountInfo={DataColums.accountInfo}
    //             actionCustom={DataColums.actionCustom}
    //           />
    //         )}
    //       </div>
    //     );
    //   },
    // },
  ];
};

export const ColunmUploadFiles = (DataColums?: any) => {
  let handleEdit = DataColums?.HandleEdit;
  let handleDelete = DataColums?.HandleDelete;
  return [
    {
      title: "Tên file ghi âm",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Dung lượng",
      dataIndex: "help",
      key: "help",
    },
    {
      title: "Trạng thái",
      dataIndex: "helps",
      key: "helps",
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      fixed: "right",
      width: 100,
      render: () => {
        return (
          <TableEditer
            HandleEdit={handleEdit}
            HandleDelete={handleDelete}
            code={undefined}
          />
        );
      },
    },
  ];
};

export const ColunmFieldGroupScipt = (DataColums?: any) => {
  let handleEdit = DataColums?.HandleEdit;
  let handleDelete = DataColums?.HandleDelete;
  return [
    {
      title: "Tên tiêu chí",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Loại tiêu chí",
      dataIndex: "amount",
      key: "amount",
    },
    {
      title: "Mô tả",
      dataIndex: "desc",
      key: "desc",
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      fixed: "right",
      width: 100,
      render: () => {
        return (
          <TableEditer
            HandleEdit={handleEdit}
            HandleDelete={handleDelete}
            code={undefined}
          />
        );
      },
    },
  ];
};
