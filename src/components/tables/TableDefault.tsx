import React, { useEffect, useState } from "react";
import { Button, Checkbox, Pagination, Popover, Space, Table, Tag } from "antd";
import { TableOutlined } from "@ant-design/icons";

const TableDefault = ({
  columns,
  dataSource,
  total,
  setPage,
  loading,
  rowSelectionStatus,
  actionCustom,
  rowSelectionDatas = {},
}: any) => {
  const setSelectedRows = rowSelectionDatas.setSelectedRows;
  const selectedRows = rowSelectionDatas.selectedRows;
  const onChangeSelection = rowSelectionDatas.onChangeSelection;
  const handleChangePage = (page: any) => {
    setPage(Number(page));
  };
  const [selectedRowKeysCustom, setSelectedRowKeysCustom] = useState<any>([]);

  useEffect(() => {
    if (selectedRows?.length == 0) {
      setSelectedRowKeysCustom([]);
    }
  }, [selectedRows]);
  const rowSelection = {
    selectedRowKeys: selectedRowKeysCustom,
    onSelectAll: (selected: any, record: any, key: any) => {
      let data = [...selectedRows];
      if (selected) {
        let news = [...selectedRows, ...record];
        news = news.filter(
          (value: any, index: any) => news.indexOf(value) == index
        );
        data = [...news];
      } else {
        data = data.filter((value: any) => {
          let check = false;
          record.map((item: any) => {
            check = value?.key === item?.key;
          });
          return check;
        });
      }
      let rowkey = data.map((value: any) => value?.key);
      setSelectedRowKeysCustom(rowkey);
      setSelectedRows(data);
    },
    onSelect: (record: any, selected: any, key: any) => {
      console.log(record);
      if (selected) {
        let data = [...selectedRows, record];
        let rowkey = data.map((value: any) => value.key);
        setSelectedRowKeysCustom(rowkey);
        setSelectedRows(data);
        onChangeSelection(rowkey, data);
      } else {
        let data = [...selectedRows].filter(
          (value: any) => value.Code !== record.Code
        );
        let rowkey = data.map((value: any) => value.key);
        setSelectedRowKeysCustom(rowkey);
        setSelectedRows(data);
        onChangeSelection(rowkey, data);
      }
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
      name: record.name,
    }),
  };

  const [colunmShowing, setColunmShowing] = useState(
    columns.map((value: any) => {
      value.hidden = false;
      return value;
    })
  );

  const onChange = (e: any, key: any) => {
    let newColunm = [...colunmShowing].map((item: any) => {
      if (item.key == key) {
        item.hidden = !e.target.checked;
      }
      return item;
    });
    setColunmShowing(newColunm);
    console.log(`checked = ${e.target.checked}`);
  };

  const content = (
    <div>
      {colunmShowing?.map((value: any, index: any) => {
        return (
          <div key={index}>
            <Checkbox
              checked={!value.hidden}
              onChange={(e) => {
                onChange(e, value.key);
              }}
            >
              {value.title}
            </Checkbox>
          </div>
        );
      })}
    </div>
  );

  return (
    <div>
      <div className="flex items-center  justify-between mb-3">
        <div>
          {actionCustom && selectedRows.length > 0 ? (
            <div className="flex items-center">
              <span className="text-sm"> Đã chọn {selectedRows?.length}:</span>
              {actionCustom()}
            </div>
          ) : null}
        </div>
        <div className="">
          <Popover
            placement="leftBottom"
            trigger="click"
            title={"Cột"}
            content={content}
          >
            <Button>
              <TableOutlined />
            </Button>
          </Popover>
        </div>
      </div>

      <div className="mt-[0px]  relative overflow-auto box-custom">
        <Table
          rowSelection={
            rowSelectionStatus
              ? {
                  type: "checkbox",
                  ...rowSelection,
                }
              : undefined
          }
          loading={loading}
          // className=" max-w-[2000px] w-[2000px]"
          bordered
          dataSource={dataSource}
          scroll={{ x: 1000 }}
          columns={colunmShowing}
          pagination={false}
        />
      </div>
      <div className="mt-[28px] flex items-center justify-between px-[20px] pb-[16px]">
        <div className="text-sm font-semibold">Tổng số: {total}</div>
        <Pagination
          showSizeChanger={false}
          onChange={handleChangePage}
          defaultCurrent={1}
          total={total || 300}
        />
      </div>
    </div>
  );
};

export default TableDefault;
