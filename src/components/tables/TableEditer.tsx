import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Popconfirm } from "antd";
import React from "react";
import { checkStaffSaleAdminAuth } from "../../../constants/permissions";

type Props = {
  HandleEdit: any;
  HandleDelete: any;
  code: any;
  popUp?: any;
  actionCustom?: any;
  accountInfo?: any;
  deletedAction?: any;
};

const TableEditer = ({
  deletedAction =false,
  HandleEdit,
  HandleDelete,
  code,
  actionCustom,
  accountInfo,
}: Props) => {
  return (
    <div className="flex items-center justify-center ">
      {actionCustom == 1 ? (
        <div className="flex">
          {checkStaffSaleAdminAuth(accountInfo) ? null : (
            <Popconfirm
              placement="topRight"
              title={"Xác nhận duyệt"}
              description={"Bạn có chắc chắn muốn duyệt ?"}
              okText="Xác nhận"
              cancelText="Cancel"
              onConfirm={() => HandleEdit(code)}
            >
              <Button className="text-sm mx-2 text-blue-500 border-blue-500">Xác nhận</Button>
            </Popconfirm>
          )}

          <Popconfirm
            placement="topRight"
            title={"Xác nhận hủy"}
            description={"Bạn có chắc chắn muốn hủy ?"}
            okText="Hủy"
            cancelText="Cancel"
            onConfirm={() => HandleDelete(code)}
          >
            <Button className="text-sm ">Hủy</Button>
          </Popconfirm>
        </div>
      ) : (
        <div className="flex items-center mr-[20px]">
          <EditOutlined
            onClick={() => {
              HandleEdit(code);
            }}
            className="text-blue-500 hover:text-blue-800 text-[16px] mr-[8px] cursor-pointer hover:scale-[1.1] duration-300 "
          />
          {!deletedAction ? (
            <Popconfirm
              placement="topRight"
              title={"Xác nhận xóa"}
              description={"Bạn có chắc chắn muốn xóa bỏ ?"}
              okText="Xóa"
              cancelText="Hủy"
              onConfirm={() => HandleDelete(code)}
            >
              <DeleteOutlined
                // onClick={() => {
                //   HandleDelete(code);
                // }}
                className="text-red-500 text-[16px] ml-[8px] cursor-pointer hover:text-red-800  hover:scale-[1.1]"
              />
            </Popconfirm>
          ) : null}
        </div>
      )}
    </div>
  );
};

export default TableEditer;
