import LayoutCpn from "@/components/layouts/LayoutCpn";
import { ModalStatus } from "@/components/tables/enums";
import ObserverHotline_public from "@/observers/admin/hotline/ObserverHotline_public";
import TemplateHotline_public from "@/templates/admin/TemplateHotline_public";
import { Form, notification } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import withLogin from "../../../withAuths/withLogin";

type Props = {};

const Hotline = (props: Props) => {
  const [dataSource, setDataSource] = useState({ data: [], total: null });
  const [page, setPage] = useState(1);
  const [editorId, setEditorId] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpenUpload, setIsModalOpenUpload] = useState(false);
  const [modalStatus, setModalStatus] = useState<ModalStatus | null>(
    ModalStatus.CREATE
  );
  const [selectedRows, setSelectedRows] = useState<any>([]);
  const [filterData, setFilterData] = useState<any>({});
  const [changModalType, setChangeModalType] = useState<any>(null);

  const dispatch = useDispatch();
  const [api, contextHolder] = notification.useNotification();
  const { hotlineList, total, isLoading, customerUnSip } = useSelector(
    (state: any) => state.hotlineReducer
  );
  const { accountInfo } = useSelector((state: any) => state.authReducer);

  const [form] = Form.useForm();

  const [windowObject, setWindowObject] = useState<any>(null);

  useEffect(() => {}, [modalStatus]);

  useEffect(() => {
    // Kiểm tra xem có tồn tại window hay không trước khi thực hiện bất kỳ hành động nào
    if (typeof window !== "undefined") {
      // Gán giá trị của window vào state
      setWindowObject(window);
    }
  }, []); // Chạy chỉ một lần khi component được tạo
  let init = {
    customerUnSip,
    changModalType,
    setChangeModalType,
    accountInfo,
    window: windowObject || {},
    filterData,
    setFilterData,
    loading: isLoading,
    api,
    total,
    contextHolder,
    dispatch,
    selectedRows,
    setSelectedRows,
    setIsModalOpenUpload,
    isModalOpenUpload,
    isModalOpen,
    setIsModalOpen,
    modalStatus,
    setModalStatus,
    form,
    dataSource: hotlineList?.map((value: any, index: any) => {
      value.STT = (Number(page) - 1) * 10 + index + 1;
      value.key = value.Code;

      return value;
    }),
    setDataSource,
    page,
    setPage,
    editorId,
    setEditorId,
  };

  const Observer = ObserverHotline_public(init);
  const LoadData = async () => {
    try {
      await Observer.fetchData();
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    LoadData();
  }, [page, accountInfo]);
  return <TemplateHotline_public observer={Observer} />;
};

export default withLogin(Hotline);
