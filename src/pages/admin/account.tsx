import LayoutCpn from "@/components/layouts/LayoutCpn";
import { ModalStatus } from "@/components/tables/enums";
import ObserverAccount_public from "@/observers/admin/account/ObserverAccount_public";
import TemplateAccount_public from "@/templates/admin/TemplateAccount_public";
import { Form, notification } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import withLogin from "../../../withAuths/withLogin";
import withAdminAuth from "../../../withAuths/withAdminAuth";
import { ROLE_TYPE } from "../../../constants/enum";

type Props = {};

const Account = (props: Props) => {
  const [dataSource, setDataSource] = useState({ data: [], total: null });
  const [page, setPage] = useState(1);
  const [editorId, setEditorId] = useState(null);
  const [filterData, setFilterData] = useState<any>({
    Role: [ROLE_TYPE.ADMINISTRATOR, ROLE_TYPE.SALEADMIN],
  });
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalStatus, setModalStatus] = useState<ModalStatus | null>(null);
  const { accountList, total, isLoading } = useSelector(
    (state: any) => state.accountReducer
  );
  const [api, contextHolder] = notification.useNotification();

  const router = useRouter();
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  let init = {
    setFilterData,
    filterData,
    contextHolder,
    api,
    loading: isLoading,
    isModalOpen,
    setIsModalOpen,
    modalStatus,
    setModalStatus,
    form,
    dataSource: accountList?.map((value: any, index: any) => {
      value.key = index;
      return value;
    }),
    setDataSource,
    page,
    setPage,
    editorId,
    setEditorId,
    dispatch,
    accountList,
    total,
  };

  const Observer = ObserverAccount_public(init);
  const LoadData = async () => {
    try {
      let data = await Observer.fetchData();
    } catch (error) {}
  };

  useEffect(() => {
    LoadData();
  }, [page, filterData]);
  return <TemplateAccount_public observer={Observer} />;
};

export default withAdminAuth(Account);
