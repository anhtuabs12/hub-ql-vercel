import LayoutCpn from '@/components/layouts/LayoutCpn'
import { ModalStatus } from '@/components/tables/enums';
import { Form, notification } from 'antd';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import withLogin from '../../../withAuths/withLogin';
import TemplateRule_public from '@/templates/admin/TemplateRule_public';
import ObserverRule_public from '@/observers/admin/rule/ObserverRule_public';

type Props = {}

const Rule = (props: Props) => {
    const [dataSource, setDataSource] = useState({ data: [], total: null });
  const [page, setPage] = useState(1);
  const [editorId, setEditorId] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalStatus, setModalStatus] = useState<ModalStatus | null>(null);
  const { ruleList , total } = useSelector((state: any) => state.ruleReducer);
  const [queryData, setQueryData] = useState({ offset: (page - 1) * 10, limit: 10 });

  const dispatch = useDispatch()

  const [form] = Form.useForm();
  const [api, contextHolder] = notification.useNotification();

  const originData = [];
for (let i = 0; i < 100; i++) {
  originData.push({
    key: i.toString(),
    name: `Edward ${i}`,
    checked: i % 2 == 0,
    age: 32,
    address: `London Park no. ${i}`,
  });
}
const [data , setData] = useState(ruleList)

useEffect(()=>{
  if(ruleList){
  setData(ruleList)
    
  }
},[ruleList])
const [editingKey, setEditingKey] = useState("");

  let init = {
    queryData, 
    setQueryData,
    isModalOpen,
    setIsModalOpen,
    modalStatus,
    setModalStatus,
    form,
    dataSource,
    setDataSource,
    page,
    setPage,
    editorId,
    setEditorId,
    dispatch,
    ruleList,
    total,

    data,
    setData,
    editingKey,
    setEditingKey,
    api,
    contextHolder,
  };

  const Observer = ObserverRule_public(init);
  const LoadData = async () => {
    Observer.fetchData();
  };

  useEffect(() => {
    LoadData();
  }, [page]);
  return <TemplateRule_public observer={Observer} />;
}

export default  withLogin(Rule)