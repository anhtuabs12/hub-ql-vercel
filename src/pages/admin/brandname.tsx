import LayoutCpn from "@/components/layouts/LayoutCpn";
import { ModalStatus } from "@/components/tables/enums";
import ObserverAccount_public from "@/observers/admin/account/ObserverAccount_public";
import TemplateAccount_public from "@/templates/admin/TemplateAccount_public";
import { Form, notification } from "antd";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import withLogin from "../../../withAuths/withLogin";
import withAdminAuth from "../../../withAuths/withAdminAuth";
import TemplateTicket_public from "@/templates/admin/TemplateTicket_public";
import ObserverTicket_public from "@/observers/admin/ticket/ObserverTicket_public";
import withStaffAuth from "../../../withAuths/withStaffAuth";
import ObserverBrandname_public from "@/observers/admin/brandname/ObserverBrandname_public";
import TemplateBrandname_public from "@/templates/admin/TemplateBrandname_public";

type Props = {};

const Brandname = (props: Props) => {
  const [dataSource, setDataSource] = useState({ data: [], total: null });
  const [page, setPage] = useState(1);
  const [editorId, setEditorId] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalStatus, setModalStatus] = useState<ModalStatus | null>(null);
  const [isModalOpenUpload, setIsModalOpenUpload] = useState(false);
  const [selectedRows, setSelectedRows] = useState([]);
  const [filterData, setFilterData] = useState<any>({});
  const { brandnameList, total, isLoading } = useSelector(
    (state: any) => state.BrandnameReducer
  );
  const [api, contextHolder] = notification.useNotification();
  const { accountInfo } = useSelector((state: any) => state.authReducer);

  const router = useRouter();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  let init = {
    contextHolder,
    api,
    loading: isLoading,
    isModalOpen,
    setIsModalOpen,
    modalStatus,
    setModalStatus,
    form,
    dataSource,
    setDataSource,
    page,
    setPage,
    editorId,
    setEditorId,
    dispatch,
    brandnameList,
    accountInfo,
    total,
    selectedRows,
    setSelectedRows,
    isModalOpenUpload,
    setIsModalOpenUpload,
    filterData,
    setFilterData,
  };

  const Observer = ObserverBrandname_public(init);
  const LoadData = async () => {
    try {
      let data = await Observer.fetchData();
    } catch (error) {}
  };

  useEffect(() => {
    LoadData();
  }, [page]);
  
  return <TemplateBrandname_public observer={Observer} />;
};

export default withStaffAuth(Brandname);
