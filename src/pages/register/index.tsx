import { FormRegister } from "@/components/forms/FieldList";
import FormRender from "@/components/forms/FormRender";
import { Form } from "antd";
import Link from "next/link";
import React from "react";
import { useDispatch } from "react-redux";
// import { reigisterRequest } from "../../../redux/actions/authActions";
import { data } from "autoprefixer";
import { useRouter } from "next/router";
import withUnLogin from "../../../withAuths/withUnLogin";

type Props = {};

const Register = (props: Props) => {
  const [form] = Form.useForm();
  const initialValues = {
    mausac: "#0079FF",
  };

  const dispatch = useDispatch();
  const router = useRouter();
  const onFinish = async (values: any) => {
    // if (!(values.phonenumber == values.repassword)) {
    //   return null;
    // }
    console.log(123123132);
    let payload = {
      fullname: values.fullname,
      password: values.password,
      phonenumber: values.phonenumber,
    };

    // dispatch(
    //   reigisterRequest({
    //     data: payload,
    //     callbackSuccess: () => {
    //       router.push("/login");
    //     },
    //   })
    // );
  };

  const onFinishFailed = (errorInfo: any) => {
    // console.log(errorInfo, "---value da nhan o ngoai");
  };
  const Datas = {
    submitBtn: {
      title: (
        <>
          <div className="font-semibold ">Đăng ký</div>
        </>
      ),
      offset: 0,
      span: 24,
      classname: "h-[36px] !rounded-lg",
    },
    initialValues,
    onFinish,
    onFinishFailed,
    form,
  };
  let fieldList = FormRegister(Datas as any);

  return (
    <div className="flex items-center justify-center w-screen h-screen bg-[#fafafb]">
      <div className="w-[1000px] h-[580px] box-custom grid grid-cols-5  overflow-hidden bg-white">
        <div className="col-span-2  ">
          <h1 className="text-[#1b3051] text-[28px] text-center font-semibold mt-[20px]">
            Đăng ký
          </h1>
          <p className="text-center text-[12px] mt-[12px]">
            Do have an account yet?{" "}
            <Link href={"/login"}>
              <span className="text-blue-500 ">Sign in</span>
            </Link>
          </p>
          <div className="w-full py-[20px] px-[28px]">
            <FormRender FieldList={fieldList} form={form} />
          </div>
        </div>
        <div className="col-span-3  bg-[#5773f8]  rounded-tl-[180px] rounded-bl-[280px] relative">
          <img
            src="./logologin.png"
            alt=""
            className="w-[80px] right-[20px] top-[10px] absolute"
          />
          <div className="ml-[60px] mr-[20px] ">
            <h1 className="text-[30px] font-semibold mt-[66px]  mb-[20px] text-[#fd9551]">
              {" "}
              <span className="">Voice Emotion – Smart QA</span>{" "}
            </h1>
            <p className="text-justify text-white ">
              <strong>Voice Emotion – Smart QA (gọi tắt: Voice Emotion)</strong>
              &nbsp;là một trong bộ sản phẩm Voice AI (gồm{" "}
              <a href="https://interits.com/giai-phap/callbot-tong-dai-tu-van-vien-ao/">
                Callbot
              </a>
              ,{" "}
              <a href="https://interits.com/giai-phap/voice-biometrics-xac-thuc-giong-noi/">
                Voice Biometrics
              </a>
              , Voice Emotion – Smart QA) của ITS, Voice Emotion là công cụ hỗ
              trợ dịch vụ chăm sóc khách hàng trong việc phân loại, đánh giá
              chất lượng cuộc gọi của Khách hàng, qua đó tiết kiệm thời gian và
              đưa ra hướng xử lý thích hợp và nhanh nhất. Voice Emotion &amp;
              Smart QA có khả năng tích hợp dễ dàng với các hệ thống khác, đặc
              biệt là Tổng đài Contact Center.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withUnLogin(Register);
