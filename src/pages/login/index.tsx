import {
  FormLoginCustomer,
  FormLoginStaff,
} from "@/components/forms/FieldList";
import FormRender from "@/components/forms/FormRender";
import { Button, Form, notification } from "antd";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { loginRequest } from "../../../redux/actions/authActions";
import LoadingDefault from "@/components/loadings/LoadingDefault";
import Link from "next/link";
import withUnLogin from "../../../withAuths/withUnLogin";
import { openNotificationWithIcon } from "../../../constants/notification";

type Props = {};

export enum TypeLogin {
  CUSTOMER = "CUSTOMER",
  STAFF = "STAFF",
}

export enum NotificationType {
  SUCCESS = "success",
  INFO = "info",
  WARNING = "warning",
  ERROR = "error",
}
const Login = (props: Props) => {
  const { isLoading } = useSelector((state: any) => state.authReducer);
  const [typeLogin, setTypeLogin] = useState<TypeLogin | null>(null);

  const [api, contextHolder] = notification.useNotification();

  const [form] = Form.useForm();
  const initialValues = {
    mausac: "#0079FF",
  };

  const router = useRouter();
  const dispatch = useDispatch();
  const onFinish = async (values: any) => {
    dispatch(
      loginRequest({
        typeLogin,
        data: values,
        callbackError: (error: string) => {
          openNotificationWithIcon(api, NotificationType.ERROR, error, "");
        },
        callbackSuccess: () => {
          openNotificationWithIcon(
            api,
            NotificationType.SUCCESS,
            "Đăng nhập thành công",
            ""
          );
          let url = typeLogin == TypeLogin.CUSTOMER ? "/admin/hotline" : "/";
          let timeout = setTimeout(() => {
            router.push(url);
            clearTimeout(timeout);
          }, 800);
        },
      })
    );
  };

  const onFinishFailed = (errorInfo: any) => {
    // console.log(errorInfo, "---value da nhan o ngoai");
  };

  const Datas = {
    submitBtn: {
      title: (
        <>
          <div className="font-semibold ">Đăng nhập</div>
        </>
      ),
      offset: 0,
      span: 24,
      classname: "h-[36px] !rounded-lg",
    },
    initialValues,
    onFinish,
    onFinishFailed,
    form,
  };

  let FormfieldListCustomer = FormLoginCustomer(Datas as any);
  let FormfieldListStaff = FormLoginStaff(Datas as any);

  const handleChangeTypeLogin = (type: TypeLogin | null) => {
    console.log(type, "type");
    setTypeLogin(type);
  };

  return (
    <>
      {contextHolder}
      <div className="relative">
        <div className="flex items-center justify-center w-screen h-screen bg-[#fafafb]">
          <div className="w-[1000px] h-[580px] box-custom grid grid-cols-5  relative overflow-hidden bg-white">
            {isLoading ? (
              <div className="z-[99] absolute top-5 left-5">
                <LoadingDefault />
              </div>
            ) : (
              <div
                onClick={() => {
                  handleChangeTypeLogin(null);
                }}
                className="z-[99] absolute top-5 left-5 cursor-pointer"
              >
                back
              </div>
            )}
            <div className="col-span-2  ">
              <h1 className="text-[#1b3051] text-[28px] text-center font-semibold mt-[60px]">
                Đăng nhập
              </h1>
              <p className="text-center text-[12px] mt-[12px]">
                Đăng nhập với tư cách{" "}
                <span className="text-blue-500 ">
                  {" "}
                  {typeLogin === TypeLogin.STAFF ? "chuyên viên" : "khách hàng"}
                </span>
                {/* <Link href={"/register"}>
                
              </Link> */}
              </p>

              {!typeLogin ? (
                <div className="flex items-center mt-[20px] justify-center">
                  <Button
                    onClick={() => {
                      handleChangeTypeLogin(TypeLogin.STAFF);
                    }}
                    className="mx-2 h-[40px]"
                  >
                    Chuyên viên
                  </Button>
                  <Button
                    onClick={() => {
                      handleChangeTypeLogin(TypeLogin.CUSTOMER);
                    }}
                    type="primary"
                    className="mx-2 h-[40px]  bg-blue-500"
                  >
                    Khách hàng
                  </Button>
                </div>
              ) : (
                <div className="w-full py-[20px] px-[28px]">
                  <FormRender
                    FieldList={
                      typeLogin === TypeLogin.CUSTOMER
                        ? FormfieldListCustomer
                        : typeLogin === TypeLogin.STAFF
                        ? FormfieldListStaff
                        : {}
                    }
                    form={form}
                  />
                </div>
              )}
            </div>
            <div className="col-span-3  bg-[#5773f8]  rounded-tl-[180px] rounded-bl-[280px] relative">
              <img
                src="./logologin.png"
                alt=""
                className="w-[80px] right-[20px] top-[10px] absolute"
              />
              <div className="ml-[60px] mr-[20px] ">
                <h1 className="text-[30px] font-semibold mt-[66px]  mb-[20px] text-[#fd9551]">
                  {" "}
                  <span className="">Voice Emotion – Smart QA</span>{" "}
                </h1>
                <p className="text-justify text-white ">
                  <strong>
                    Voice Emotion – Smart QA (gọi tắt: Voice Emotion)
                  </strong>
                  &nbsp;là một trong bộ sản phẩm Voice AI (gồm{" "}
                  <a href="https://interits.com/giai-phap/callbot-tong-dai-tu-van-vien-ao/">
                    Callbot
                  </a>
                  ,{" "}
                  <a href="https://interits.com/giai-phap/voice-biometrics-xac-thuc-giong-noi/">
                    Voice Biometrics
                  </a>
                  , Voice Emotion – Smart QA) của ITS, Voice Emotion là công cụ
                  hỗ trợ dịch vụ chăm sóc khách hàng trong việc phân loại, đánh
                  giá chất lượng cuộc gọi của Khách hàng, qua đó tiết kiệm thời
                  gian và đưa ra hướng xử lý thích hợp và nhanh nhất. Voice
                  Emotion &amp; Smart QA có khả năng tích hợp dễ dàng với các hệ
                  thống khác, đặc biệt là Tổng đài Contact Center.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default withUnLogin(Login);
