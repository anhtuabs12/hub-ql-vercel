import { Form } from "antd";
import { useEffect, useState } from "react";
import TemplateHomepage_public from "@/templates/homepages/TemplateHomepage_public";
// import ObserverHomepage_public from "@/observers/homepage/ObserverHomepage_public";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import withLogin from "../../withAuths/withLogin";
import withAdminAuth from "../../withAuths/withAdminAuth";

 function Home() {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const { accountInfo } = useSelector((state: any) => state.authReducer);
  const router = useRouter();

  const [form] = Form.useForm();

  let init = {
    isModalOpen,
    setIsModalOpen,
    form,
  };

  // const Observer = ObserverHomepage_public(init);

  return (
    <>
      <TemplateHomepage_public observer={null} />
    </>
  );
}

export default  withAdminAuth(Home)
