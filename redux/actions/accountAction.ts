export const ACCOUNT_CREATE_REQUEST = "ACCOUNT_CREATE_REQUEST";
export const ACCOUNT_CREATE_SUCCESS = "ACCOUNT_CREATE_SUCCESS";

export const ACCOUNT_DELETE_REQUEST = "ACCOUNT_DELETE_REQUEST";
export const ACCOUNT_DELETE_SUCCESS = "ACCOUNT_DELETE_SUCCESS";

export const ACCOUNT_UPDATE_REQUEST = "ACCOUNT_UPDATE_REQUEST";
export const ACCOUNT_UPDATE_SUCCESS = "ACCOUNT_UPDATE_SUCCESS";

export const ACCOUNT_LIST_REQUEST = "ACCOUNT_LIST_REQUEST";
export const ACCOUNT_LIST_SUCCESS = "ACCOUNT_LIST_SUCCESS";

export const ACCOUNT_PENDING = "ACCOUNT_PENDING";
export const ACCOUNT_FAILURE = "ACCOUNT_FAILURE";

export const accountListRequest = (payload: any) => ({
  type: ACCOUNT_LIST_REQUEST,
  payload: payload,
});

export const accountListSuccess = (payload: any) => ({
  type: ACCOUNT_LIST_SUCCESS,
  payload: payload,
});

export const accountCreateRequest = (payload: any) => ({
  type: ACCOUNT_CREATE_REQUEST,
  payload: payload,
});

export const accountCreateSuccess = (payload: any) => ({
  type: ACCOUNT_CREATE_SUCCESS,
  payload: payload,
});

export const accountUpdateRequest = (payload: any) => ({
  type: ACCOUNT_UPDATE_REQUEST,
  payload: payload,
});

export const accountUpdateSuccess = (payload: any) => ({
  type: ACCOUNT_UPDATE_SUCCESS,
  payload: payload,
});

export const accountDeleteRequest = (payload: any) => ({
  type: ACCOUNT_DELETE_REQUEST,
  payload: payload,
});

export const accountDeleteSuccess = (payload: any) => ({
  type: ACCOUNT_DELETE_SUCCESS,
  payload: payload,
});

export const accountPending = (payload: any) => ({
  type: ACCOUNT_PENDING,
  payload: payload,
});

export const accountFailure = (error: string) => ({
  type: ACCOUNT_FAILURE,
  payload: error,
});



