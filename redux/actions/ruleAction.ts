export const RULE_CREATE_REQUEST = "RULE_CREATE_REQUEST";
export const RULE_CREATE_SUCCESS = "RULE_CREATE_SUCCESS";

export const RULE_DELETE_REQUEST = "RULE_DELETE_REQUEST";
export const RULE_DELETE_SUCCESS = "RULE_DELETE_SUCCESS";

export const RULE_UPDATE_REQUEST = "RULE_UPDATE_REQUEST";
export const RULE_UPDATE_SUCCESS = "RULE_UPDATE_SUCCESS";

export const RULE_LIST_REQUEST = "RULE_LIST_REQUEST";
export const RULE_LIST_SUCCESS = "RULE_LIST_SUCCESS";

export const RULE_PENDING = " RULE_PENDING";
export const RULE_FAILURE = " RULE_FAILURE";

export const ruleCreateRequest = (payload: any) => ({
  type: RULE_CREATE_REQUEST,
  payload: payload,
});

export const ruleCreateSuccess = (payload: any) => ({
  type: RULE_CREATE_SUCCESS,
  payload: payload,
});

export const ruleUpdateRequest = (payload: any) => ({
  type: RULE_UPDATE_REQUEST,
  payload: payload,
});

export const ruleUpdateSuccess = (userData: any) => ({
  type: RULE_UPDATE_SUCCESS,
  payload: userData,
});

export const ruleDeleteRequest = (payload: any) => ({
  type: RULE_DELETE_REQUEST,
  payload: payload,
});

export const ruleDeleteSuccess = (userData: any) => ({
  type: RULE_DELETE_SUCCESS,
  payload: userData,
});

export const rulePending = (payload: any) => ({
  type: RULE_PENDING,
  payload: payload,
});

export const ruleFailure = (error: string) => ({
  type: RULE_FAILURE,
  payload: error,
});

export const ruleListRequest = (payload: any) => ({
  type: RULE_LIST_REQUEST,
  payload: payload,
});

export const ruleListSuccess = (payload: any) => ({
  type: RULE_LIST_SUCCESS,
  payload: payload,
});
