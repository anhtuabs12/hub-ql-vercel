export const TICKET_CREATE_REQUEST = "TICKET_CREATE_REQUEST";
export const TICKET_CREATE_SUCCESS = "TICKET_CREATE_SUCCESS";

export const TICKET_DELETE_REQUEST = "TICKET_DELETE_REQUEST";
export const TICKET_DELETE_SUCCESS = "TICKET_DELETE_SUCCESS";

export const TICKET_UPDATE_REQUEST = "TICKET_UPDATE_REQUEST";
export const TICKET_UPDATE_SUCCESS = "TICKET_UPDATE_SUCCESS";

export const TICKET_LIST_REQUEST = "TICKET_LIST_REQUEST";
export const TICKET_LIST_SUCCESS = "TICKET_LIST_SUCCESS";

export const TICKET_PENDING = " TICKET_PENDING";
export const TICKET_FAILURE = " TICKET_FAILURE";

export const ticketCreateRequest = (payload: any) => ({
  type: TICKET_CREATE_REQUEST,
  payload: payload,
});

export const ticketCreateSuccess = (payload: any) => ({
  type: TICKET_CREATE_SUCCESS,
  payload: payload,
});

export const ticketUpdateRequest = (payload: any) => ({
  type: TICKET_UPDATE_REQUEST,
  payload: payload,
});

export const ticketUpdateSuccess = (userData: any) => ({
  type: TICKET_UPDATE_SUCCESS,
  payload: userData,
});

export const ticketDeleteRequest = (payload: any) => ({
  type: TICKET_DELETE_REQUEST,
  payload: payload,
});

export const ticketDeleteSuccess = (userData: any) => ({
  type: TICKET_DELETE_SUCCESS,
  payload: userData,
});

export const ticketPending = (payload: any) => ({
  type: TICKET_PENDING,
  payload: payload,
});

export const ticketFailure = (error: string) => ({
  type: TICKET_FAILURE,
  payload: error,
});

export const ticketListRequest = (payload: any) => ({
  type: TICKET_LIST_REQUEST,
  payload: payload,
});

export const ticketListSuccess = (payload: any) => ({
  type: TICKET_LIST_SUCCESS,
  payload: payload,
});
