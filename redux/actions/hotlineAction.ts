export const HOTLINE_CREATE_REQUEST = "HOTLINE_CREATE_REQUEST";
export const HOTLINE_CREATE_SUCCESS = "HOTLINE_CREATE_SUCCESS";

export const HOTLINE_DELETE_REQUEST = "HOTLINE_DELETE_REQUEST";
export const HOTLINE_DELETE_SUCCESS = "HOTLINE_DELETE_SUCCESS";

export const HOTLINE_UPDATE_REQUEST = "HOTLINE_UPDATE_REQUEST";
export const HOTLINE_UPDATE_SUCCESS = "HOTLINE_UPDATE_SUCCESS";

export const HOTLINE_LIST_REQUEST = "HOTLINE_LIST_REQUEST";
export const HOTLINE_LIST_SUCCESS = "HOTLINE_LIST_SUCCESS";

export const HOTLINE_IMPORT_REQUEST = "HOTLINE_IMPORT_REQUEST";
export const HOTLINE_IMPORT_SUCCESS = "HOTLINE_IMPORT_SUCCESS";

export const HOTLINE_FINDBYCODE_REQUEST = "HOTLINE_FINDBYCODE_REQUEST";
export const HOTLINE_FINDBYCODE_SUCCESS = "HOTLINE_FINDBYCODE_SUCCESS";

export const HOTLINE_PENDING = "HOTLINE_PENDING";
export const HOTLINE_FAILURE = "HOTLINE_FAILURE";

//action
export const HOTLINE_KEEPING_REQUEST = "HOTLINE_KEEPING_REQUEST";
export const HOTLINE_EVICTING_REQUEST = "HOTLINE_EVICTING_REQUEST";
export const HOTLINE_READY_REQUEST = "HOTLINE_READY_REQUEST";
export const HOTLINE_CANCEL_REQUEST = "HOTLINE_CANCEL_REQUEST";
export const HOTLINE_DOING_REQUEST = "HOTLINE_DOING_REQUEST";
export const HOTLINE_WAITINGBRANDNAME_REQUEST = "HOTLINE_WAITINGBRANDNAME_REQUEST";

export const hotlineWaitingBrandnameRequest = (payload: any) => ({
  type: HOTLINE_WAITINGBRANDNAME_REQUEST,
  payload: payload,
});

export const hotlineDoingRequest = (payload: any) => ({
  type: HOTLINE_DOING_REQUEST,
  payload: payload,
});

export const hotlineCancelRequest = (payload: any) => ({
  type: HOTLINE_CANCEL_REQUEST,
  payload: payload,
});

export const hotlineReadyRequest = (payload: any) => ({
  type: HOTLINE_READY_REQUEST,
  payload: payload,
});

export const hotlineEvictingRequest = (payload: any) => ({
  type: HOTLINE_EVICTING_REQUEST,
  payload: payload,
});

export const hotlineFindbyCodeRequest = (payload: any) => ({
  type: HOTLINE_FINDBYCODE_REQUEST,
  payload: payload,
});

export const hotlineFindbyCodeSuccess = (payload: any) => ({
  type: HOTLINE_FINDBYCODE_SUCCESS,
  payload: payload,
});

export const hotlineKeepingRequest = (payload: any) => ({
  type: HOTLINE_KEEPING_REQUEST,
  payload: payload,
});

export const hotlineImportRequest = (payload: any) => ({
  type: HOTLINE_IMPORT_REQUEST,
  payload: payload,
});

export const hotlineImportSuccess = (payload: any) => ({
  type: HOTLINE_IMPORT_SUCCESS,
  payload: payload,
});

export const hotlineListRequest = (payload: any) => ({
  type: HOTLINE_LIST_REQUEST,
  payload: payload,
});

export const hotlineListSuccess = (payload: any) => ({
  type: HOTLINE_LIST_SUCCESS,
  payload: payload,
});

export const hotlineCreateRequest = (payload: any) => ({
  type: HOTLINE_CREATE_REQUEST,
  payload: payload,
});

export const hotlineCreateSuccess = (payload: any) => ({
  type: HOTLINE_CREATE_SUCCESS,
  payload: payload,
});

export const hotlineUpdateRequest = (payload: any) => ({
  type: HOTLINE_UPDATE_REQUEST,
  payload: payload,
});

export const hotlineUpdateSuccess = (payload: any) => ({
  type: HOTLINE_UPDATE_SUCCESS,
  payload: payload,
});

// export const accountDeleteRequest = (payload: any) => ({
//   type: ACCOUNT_DELETE_REQUEST,
//   payload: payload,
// });

// export const accountDeleteSuccess = (payload: any) => ({
//   type: ACCOUNT_DELETE_SUCCESS,
//   payload: payload,
// });

export const hotlinePending = (payload: any) => ({
  type: HOTLINE_PENDING,
  payload: payload,
});

export const hotlineFailure = (error: string) => ({
  type: HOTLINE_FAILURE,
  payload: error,
});
