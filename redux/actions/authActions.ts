export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_PENDING = "LOGIN_PENDING";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const CHECK_LOGIN_REQUEST = "CHECK_LOGIN_REQUEST";

export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";





export const logoutRequest = (payload: any) => ({
  type: LOGOUT_REQUEST,
  payload: payload,
});

export const logoutSuccess = (payload: any) => ({
  type: LOGOUT_SUCCESS,
  payload: payload,
});


export const loginRequest = (payload: any) => ({
  type: LOGIN_REQUEST,
  payload: payload,
});

export const loginSuccess = (userData: any) => ({
  type: LOGIN_SUCCESS,
  payload: userData,
});

export const loginPending = (payload: any) => ({
  type: LOGIN_PENDING,
  payload: payload,
});

export const loginFailure = (error: string) => ({
  type: LOGIN_FAILURE,
  payload: error,
});


export const accountInfoRequest = (payload: any) => ({
  type: CHECK_LOGIN_REQUEST,
  payload: payload,
});


