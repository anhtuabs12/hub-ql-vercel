export const LOG_LIST_REQUEST = "LOG_LIST_REQUEST";
export const LOG_LIST_SUCCESS = "LOG_LIST_SUCCESS";

export const LOG_PENDING = "LOG_PENDING";
export const LOG_FAILURE = "LOG_FAILURE";

export const logListRequest = (payload: any) => ({
  type: LOG_LIST_REQUEST,
  payload: payload,
});

export const logListSuccess = (payload: any) => ({
  type: LOG_LIST_SUCCESS,
  payload: payload,
});



export const logPending = (payload: any) => ({
    type: LOG_PENDING,
    payload: payload,
  });
  
  export const logFailure = (error: string) => ({
    type: LOG_FAILURE,
    payload: error,
  });
  
  
  
  