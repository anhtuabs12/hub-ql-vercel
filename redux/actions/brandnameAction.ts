export const BRANDNAME_CREATE_REQUEST = "BRANDNAME_CREATE_REQUEST";
export const BRANDNAME_CREATE_SUCCESS = "BRANDNAME_CREATE_SUCCESS";

export const BRANDNAME_DELETE_REQUEST = "BRANDNAME_DELETE_REQUEST";
export const BRANDNAME_DELETE_SUCCESS = "BRANDNAME_DELETE_SUCCESS";

export const BRANDNAME_UPDATE_REQUEST = "BRANDNAME_UPDATE_REQUEST";
export const BRANDNAME_UPDATE_SUCCESS = "BRANDNAME_UPDATE_SUCCESS";

export const BRANDNAME_LIST_REQUEST = "BRANDNAME_LIST_REQUEST";
export const BRANDNAME_LIST_SUCCESS = "BRANDNAME_LIST_SUCCESS";

export const BRANDNAME_PENDING = " BRANDNAME_PENDING";
export const BRANDNAME_FAILURE = " BRANDNAME_FAILURE";

export const brandnameCreateRequest = (payload: any) => ({
  type: BRANDNAME_CREATE_REQUEST,
  payload: payload,
});

export const brandnameCreateSuccess = (payload: any) => ({
  type: BRANDNAME_CREATE_SUCCESS,
  payload: payload,
});

export const brandnameUpdateRequest = (payload: any) => ({
  type: BRANDNAME_UPDATE_REQUEST,
  payload: payload,
});

export const brandnameUpdateSuccess = (payload: any) => ({
  type: BRANDNAME_UPDATE_SUCCESS,
  payload: payload,
});

export const brandnameDeleteRequest = (payload: any) => ({
  type: BRANDNAME_DELETE_REQUEST,
  payload: payload,
});

export const brandnameDeleteSuccess = (payload: any) => ({
  type: BRANDNAME_DELETE_SUCCESS,
  payload: payload,
});

export const brandnamePending = (payload: any) => ({
  type: BRANDNAME_PENDING,
  payload: payload,
});

export const brandnameFailure = (error: string) => ({
  type: BRANDNAME_FAILURE,
  payload: error,
});

export const brandnameListRequest = (payload: any) => ({
  type: BRANDNAME_LIST_REQUEST,
  payload: payload,
});

export const brandnameListSuccess = (payload: any) => ({
  type: BRANDNAME_LIST_SUCCESS,
  payload: payload,
});
