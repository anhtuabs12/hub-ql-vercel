import {
  RULE_FAILURE,
  RULE_LIST_SUCCESS,
  RULE_PENDING,
} from "../actions/ruleAction";

export const initialState_ruleReducer = {
  ruleList: null,
  isLoading: false,
  error: null,
  total: null,
};

const ruleReducer = (state = initialState_ruleReducer, action: any) => {
  switch (action.type) {
    case RULE_PENDING: // Xử lý action LOGIN_PENDING
      return { ...state, isLoading: action.payload, error: null };
    case RULE_LIST_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        ruleList: action.payload.list,
        isLoading: false,
        error: null,
      };
    case RULE_FAILURE:
      return {
        ...state,
        ruleList: null,
        isLoading: false,
        total: null,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default ruleReducer;
