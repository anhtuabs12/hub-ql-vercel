import {
  ACCOUNT_FAILURE,
  ACCOUNT_LIST_SUCCESS,
  ACCOUNT_PENDING,
  } from "../actions/accountAction";
  
 export const initialState_accountReducer = {
    accountList: null,
    isLoading: false,
    error: null,
    total: null,
  
    // user: null,
    // error: null,
    // isLoading: false,
    // isLoading_Template: false, // Thêm trạng thái isLoading
  };
  
  const accountReducer = (state = initialState_accountReducer, action: any) => {
    switch (action.type) {
      case ACCOUNT_PENDING: // Xử lý action LOGIN_PENDING
        return { ...state, isLoading: action.payload, error: null };
      case ACCOUNT_LIST_SUCCESS:
        return {
          ...state,
          isLoggedIn: true,
          accountList: action.payload.list,
          total: action.payload.total,
          isLoading: false,
          error: null,
        };
      case ACCOUNT_FAILURE:
        return {
          ...state,
          isLoggedIn: false,
          isLoading: false,
          total: null,
          error: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default accountReducer;
  