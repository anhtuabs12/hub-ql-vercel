import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_PENDING,
  LOGOUT_SUCCESS,
} from "../actions/authActions";

export const initialState_authReducer :any  = {
  accountInfo: null || undefined,
  isLoading: false|| undefined,
  error: null|| undefined,
  isLoggedIn: false|| undefined,
};

const authReducer = (state = initialState_authReducer, action: any) => {
  switch (action.type) {
    case LOGIN_PENDING: // Xử lý action LOGIN_PENDING
      return { ...state, isLoading: action.payload, error: null };
    case LOGIN_SUCCESS:
      console.log(action.payload.data.info, "action.payload.data.info")
      return {
        ...state,
        isLoggedIn: true,
        accountInfo: action.payload.data.info,
        isLoading: false,
        error: null,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        isLoggedIn: false,
        accountInfo: null,
        isLoading: false,
        error: action.payload,
      };

      case LOGOUT_SUCCESS:
        return {
          accountInfo: null,
          isLoading: false,
          error: null,
          isLoggedIn: false,
        }
    default:
      return state;
  }
};

export default authReducer;
