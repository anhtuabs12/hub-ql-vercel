import { combineReducers, Reducer } from "redux";
import authReducer, { initialState_authReducer } from "./AuthReducer";
import ruleReducer, { initialState_ruleReducer } from "./RuleReducer";
import accountReducer, { initialState_accountReducer } from "./AccountReducer";
import TicketReducer, { initialState_TicketReducer } from "./TicketReducer";
import hotlineReducer, { initialState_hotlineReducer } from "./HotlineReducer";
import BrandnameReducer, { initialState_BrandnameReducer } from "./BrandnameReducer";
import logReducer, { initialState_logReducer } from "./LogReducer";

const rootReducer = combineReducers({
  authReducer: authReducer as Reducer<typeof initialState_authReducer, any>,
  ruleReducer:  ruleReducer as Reducer<typeof initialState_ruleReducer, any>,
  accountReducer:  accountReducer as Reducer<typeof initialState_accountReducer, any>,
  TicketReducer:  TicketReducer as Reducer<typeof initialState_TicketReducer, any>,
  hotlineReducer:  hotlineReducer as Reducer<typeof initialState_hotlineReducer, any>,
  BrandnameReducer:  BrandnameReducer as Reducer<typeof initialState_BrandnameReducer, any>,
  logReducer:  logReducer as Reducer<typeof initialState_logReducer, any>,
  // Add other reducers here
});

export default rootReducer;
