import {
    BRANDNAME_FAILURE,
    BRANDNAME_LIST_SUCCESS,
    BRANDNAME_PENDING,
  } from "../actions/brandnameAction";
  
 export const initialState_BrandnameReducer= {
    brandnameList: null,
    isLoading: false,
    error: null,
    total: null,
  };
  
  const BrandnameReducer = (state = initialState_BrandnameReducer, action: any) => {
    
    switch (action.type) {
      case BRANDNAME_PENDING: // Xử lý action LOGIN_PENDING
        return { ...state, isLoading: action.payload, error: null };
      case BRANDNAME_LIST_SUCCESS:
        return {
          ...state,
          isLoggedIn: true,
          brandnameList: action.payload.list,
          isLoading: false,
          error: null,
          total:action.payload.total,
        };
      case BRANDNAME_FAILURE:
        return {
          ...state,
          brandnameList: null,
          isLoading: false,
          total: null,
          error: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default BrandnameReducer;
  