import { LOG_FAILURE, LOG_LIST_SUCCESS, LOG_PENDING } from "../actions/logAction";

    
   export const initialState_logReducer = {
      logList: null,
      isLoading: false,
      error: null,
      total: null,
    
      // user: null,
      // error: null,
      // isLoading: false,
      // isLoading_Template: false, // Thêm trạng thái isLoading
    };
    
    const logReducer = (state = initialState_logReducer, action: any) => {
      switch (action.type) {
        case LOG_PENDING: // Xử lý action LOGIN_PENDING
          return { ...state, isLoading: action.payload, error: null };
        case LOG_LIST_SUCCESS:
          return {
            ...state,
            isLoggedIn: true,
            logList: action.payload.list,
            total: action.payload.total,
            isLoading: false,
            error: null,
          };
        case LOG_FAILURE:
          return {
            ...state,
            isLoggedIn: false,
            isLoading: false,
            total: null,
            error: action.payload,
          };
        default:
          return state;
      }
    };
    
    export default logReducer;
    