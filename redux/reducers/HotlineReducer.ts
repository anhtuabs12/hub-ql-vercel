import { StarOutlined } from "@ant-design/icons";
import {
  HOTLINE_FAILURE,
  HOTLINE_LIST_SUCCESS,
  HOTLINE_PENDING,
} from "../actions/hotlineAction";
import { formatSelecter_Form } from "../../constants/formats";

export const initialState_hotlineReducer = {
  hotlineList: null,
  customerUnSip: null,
  isLoading: false,
  error: null,
  total: null,

  // user: null,
  // error: null,
  // isLoading: false,
  // isLoading_Template: false, // Thêm trạng thái isLoading
};

const hotlineReducer = (state = initialState_hotlineReducer, action: any) => {
  switch (action.type) {
    case HOTLINE_PENDING: // Xử lý action LOGIN_PENDING
      return { ...state, isLoading: action.payload, error: null };
    case HOTLINE_LIST_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        hotlineList: action.payload.hotline.list,
        customerUnSip: action.payload.customerUnSip.list.map((value: any) => {
          value.type = "unSip";
          return value;
        }),
        total: action.payload.hotline.total,
        isLoading: false,
        error: null,
      };
    case HOTLINE_FAILURE:
      return {
        ...state,
        isLoggedIn: false,
        isLoading: false,
        total: null,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default hotlineReducer;
