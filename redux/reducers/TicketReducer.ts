import {
    TICKET_FAILURE,
    TICKET_LIST_SUCCESS,
    TICKET_PENDING,
  } from "../actions/ticketAction";
  
export  const initialState_TicketReducer = {
    ticketList: null,
    accountAll:null,
    isLoading: false,
    error: null,
    total: null,
  };
  
  const TicketReducer = (state = initialState_TicketReducer, action: any) => {
    
    switch (action.type) {
      case TICKET_PENDING: // Xử lý action LOGIN_PENDING
        return { ...state, isLoading: action.payload, error: null };
      case TICKET_LIST_SUCCESS:
        console.log(action.payload.ticket.list,"actionactionactionactionaction")
        return {
          ...state,
          isLoggedIn: true,
          ticketList: action.payload.ticket.list,
          accountAll: action.payload.account.list,
          isLoading: false,
          error: null,
          total:action.payload.ticket.total,
        };
      case TICKET_FAILURE:
        return {
          ...state,
          ticketList: null,
          isLoading: false,
          total: null,
          error: action.payload,
        };
      default:
        return state;
    }
  };
  
  export default TicketReducer;
  