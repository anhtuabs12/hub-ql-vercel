// store/sagas/yourSaga.js
import { takeLatest, put, call, throttle } from "redux-saga/effects";
import {
  ACCOUNT_CREATE_REQUEST,
  ACCOUNT_DELETE_REQUEST,
  ACCOUNT_LIST_REQUEST,
  ACCOUNT_UPDATE_REQUEST,
  accountFailure,
  accountListSuccess,
  accountPending,
} from "../actions/accountAction";
import {
  CreateAccount,
  DeleteAccount,
  ListAccount,
  UpdateAccount,
} from "../../apiRoutes/acount";
import { ROLE_TYPE } from "../../constants/enum";

function* ListAccountSaga(action: any): any {
  try {
    yield put(accountPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const listAccount = yield call(ListAccount,action.payload);
    if (listAccount.error) {
      action.payload.callbackError(listAccount.message);
      return yield put(accountFailure(listAccount.message));
    }
    yield put(accountListSuccess(listAccount.data));
  } catch (error) {
    yield put(accountFailure("lỗi"));
  }
}

function* CreateAccountSaga(action: any): any {
  try {
    yield put(accountPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const newAccount = yield call(CreateAccount, action.payload.data);
    if (newAccount.error) {
      action.payload.callbackError(newAccount.message);
      return yield put(accountFailure(newAccount.message));
    }
    yield call(ListAccountSaga, { payload: { limit: 10, offset: 0 , ...action.payload.filterData} });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(accountFailure("lỗi"));
  }
}

function* DeleteAccountSaga(action: any): any {
  try {
    yield put(accountPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const deleted = yield call(DeleteAccount, action.payload.data);
    if (deleted.error) {
      yield put(accountFailure(deleted.message));
    }
    yield call(ListAccountSaga, { payload: { limit: 10, offset: 0, ...action.payload.filterData} });
  } catch (error) {
    yield put(accountFailure("lỗi"));
  }
}

function* UpdateAccountSaga(action: any): any {
  try {
    yield put(accountPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const update = yield call(UpdateAccount, action.payload.data);
    if (update.error) {
      action.payload.callbackError(update.message);
      yield put(accountFailure(update.message));
    }
    yield call(ListAccountSaga, { payload: { limit: 10, offset: 0 , ...action.payload.filterData} });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(accountFailure("lỗi"));
  }
}

function* AccountSaga() {
  yield takeLatest(ACCOUNT_LIST_REQUEST, ListAccountSaga);
  yield takeLatest(ACCOUNT_CREATE_REQUEST, CreateAccountSaga);
  yield takeLatest(ACCOUNT_DELETE_REQUEST, DeleteAccountSaga);
  yield takeLatest(ACCOUNT_UPDATE_REQUEST, UpdateAccountSaga);
}

export default AccountSaga;