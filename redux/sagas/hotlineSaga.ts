// store/sagas/yourSaga.js
import { takeLatest, put, call, throttle } from "redux-saga/effects";
import {
  HOTLINE_CANCEL_REQUEST,
  HOTLINE_CREATE_REQUEST,
  HOTLINE_DELETE_REQUEST,
  HOTLINE_DOING_REQUEST,
  HOTLINE_EVICTING_REQUEST,
  HOTLINE_FINDBYCODE_REQUEST,
  HOTLINE_IMPORT_REQUEST,
  HOTLINE_KEEPING_REQUEST,
  HOTLINE_LIST_REQUEST,
  HOTLINE_READY_REQUEST,
  HOTLINE_UPDATE_REQUEST,
  HOTLINE_WAITINGBRANDNAME_REQUEST,
  hotlineFailure,
  hotlineListSuccess,
  hotlinePending,
} from "../actions/hotlineAction";
import {
  CancelHotline,
  CreateHotline,
  DoingHotline,
  EvictingHotline,
  FindbyCodeHotline,
  KeepingHotline,
  ListHotline,
  ReadyHotline,
  UpdateHotline,
  WaitingbrandnameHotline,
} from "../../apiRoutes/hotline";
import { importHotline } from "../../apiRoutes/import";
import { ListAccount } from "../../apiRoutes/acount";
import { ROLE_TYPE } from "../../constants/enum";

function* ListHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const listHotline = yield call(ListHotline, action.payload);
    let listCustomerUnSip = {} as any;
    if (action.payload.accountInfoRole !== ROLE_TYPE.CUSTOMER) {
      listCustomerUnSip = yield call(ListAccount, {
        Role: [ROLE_TYPE.CUSTOMER],
      });
    }

    if (listHotline.error) {
      action.payload.callbackError(listHotline.message);
      return yield put(hotlineFailure(listHotline.message));
    }
    yield put(
      hotlineListSuccess({
        hotline: listHotline.data,
        customerUnSip: listCustomerUnSip?.data || { list: [] },
      })
    );
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* CreateHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const newAccount = yield call(CreateHotline, action.payload.data);
    if (newAccount.error) {
      action.payload.callbackError(newAccount.message);
      return yield put(hotlineFailure(newAccount.message));
    }
    yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* ImportHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const importAccount = yield call(importHotline, action.payload.data);
    if (importAccount.error) {
      action.payload.callbackError(importAccount.message);
      return yield put(hotlineFailure(importAccount.message));
    }
    // yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    // action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* FindbyCodeHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const deleted = yield call(FindbyCodeHotline, action.payload.data);
    if (deleted.error) {
      yield put(hotlineFailure(deleted.message));
    }
    // yield call(ListAccountSaga, { payload: { limit: 10, offset: 0 } });
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* KeepingHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const update = yield call(KeepingHotline, action.payload.data);
    if (update.error) {
      action.payload.callbackError(update.message);
      return yield put(hotlineFailure(update.message));
    }
    yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* EvictingHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const update = yield call(EvictingHotline, action.payload.data);
    if (update.error) {
      action.payload.callbackError(update.message);
      return yield put(hotlineFailure(update.message));
    }
    yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* ReadyHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const update = yield call(ReadyHotline, action.payload.data);
    if (update.error) {
      action.payload.callbackError(update.message);
      return yield put(hotlineFailure(update.message));
    }
    yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* CancelHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const update = yield call(CancelHotline, action.payload.data);
    if (update.error) {
      action.payload.callbackError(update.message);
      return yield put(hotlineFailure(update.message));
    }
    yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* DoingHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const update = yield call(DoingHotline, action.payload.data);
    if (update.error) {
      action.payload.callbackError(update.message);
      return yield put(hotlineFailure(update.message));
    }
    yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* WaitingbrandnameHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const update = yield call(WaitingbrandnameHotline, action.payload.data);
    if (update.error) {
      action.payload.callbackError(update.message);
      return yield put(hotlineFailure(update.message));
    }
    yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* UpdateHotlineSaga(action: any): any {
  try {
    yield put(hotlinePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const update = yield call(UpdateHotline, action.payload.data);
    if (update.error) {
      action.payload.callbackError(update.message);
      return yield put(hotlineFailure(update.message));
    }
    yield call(ListHotlineSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(hotlineFailure("lỗi"));
  }
}

function* HotlineSaga() {
  yield takeLatest(HOTLINE_LIST_REQUEST, ListHotlineSaga);
  yield takeLatest(HOTLINE_CREATE_REQUEST, CreateHotlineSaga);
  yield takeLatest(HOTLINE_UPDATE_REQUEST, UpdateHotlineSaga);

  yield takeLatest(HOTLINE_IMPORT_REQUEST, ImportHotlineSaga);
  yield takeLatest(HOTLINE_FINDBYCODE_REQUEST, FindbyCodeHotlineSaga);

  //action
  yield takeLatest(HOTLINE_KEEPING_REQUEST, KeepingHotlineSaga);
  yield takeLatest(HOTLINE_EVICTING_REQUEST, EvictingHotlineSaga);
  yield takeLatest(HOTLINE_READY_REQUEST, ReadyHotlineSaga);
  yield takeLatest(HOTLINE_CANCEL_REQUEST, CancelHotlineSaga);
  yield takeLatest(HOTLINE_DOING_REQUEST, DoingHotlineSaga);
  yield takeLatest(
    HOTLINE_WAITINGBRANDNAME_REQUEST,
    WaitingbrandnameHotlineSaga
  );
}

export default HotlineSaga;
