// store/sagas/yourSaga.js
import { takeLatest, put, call, throttle } from "redux-saga/effects";
import {
  CHECK_LOGIN_REQUEST,
  LOGIN_REQUEST,
  loginFailure,
  loginPending,
  loginSuccess,
  LOGOUT_REQUEST,
  logoutSuccess,
} from "../actions/authActions"; // Import your action types and creators
import {
  getAccountInfo,
  logninStaff,
  logninCustomer,
} from "../../apiRoutes/auth";
import { TypeLogin } from "@/pages/login";

function* LoginSaga(action: any): any {
  try {
    yield put(loginPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const userData = yield call(
      action.payload.typeLogin === TypeLogin.STAFF
        ? logninStaff
        : logninCustomer,
      action.payload.data
    );
    if (userData.error) {
      localStorage.clear();
      action.payload.callbackError(userData.message);
      return yield put(loginFailure(userData.message));
    }

    localStorage.setItem("hub_token", userData.data.token);

    console.log( userData?.data?.info ,"  userData?.data?.info")

    localStorage.setItem(
      "hub_key",
      userData?.data?.info?.Email ||
        userData?.data?.info?.enterprise_number ||
        ""
    );

    yield put(loginSuccess(userData));
    action.payload.callbackSuccess();
  } catch (error) {
    action.payload.callbackError("lỗi");
    yield put(loginFailure("lỗi"));
  }
}

function* CheckLoginSaga(action: any): any {
  try {
    yield put(loginPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const userData = yield call(getAccountInfo);
    if (userData.error) {
      yield put(loginFailure(userData.message));
      localStorage.clear();
      return action.payload.callbackerror();
    }

    console.log(userData,"userData")
    yield put(loginSuccess(userData));
    action.payload.callbackSuccess();
  } catch (error) {
    localStorage.clear();
    yield put(loginFailure("lỗi"));
  }
}

function* LogoutSaga(action: any): any {
  try {
    // Dispatch action LOGIN_PENDING trước khi gọi API
    yield put(logoutSuccess({}));
    localStorage.clear();
    action.payload.callback();
  } catch (error) {
    yield put(loginFailure("lỗi"));
  }
}

function* AuthSaga() {
  yield takeLatest(LOGIN_REQUEST, LoginSaga);
  yield takeLatest(CHECK_LOGIN_REQUEST, CheckLoginSaga);
  yield takeLatest(LOGOUT_REQUEST, LogoutSaga);
}

export default AuthSaga;
