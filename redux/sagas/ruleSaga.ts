// store/sagas/yourSaga.js
import { takeLatest, put, call, throttle } from "redux-saga/effects";
import {
  ruleListSuccess,
  ruleFailure,
  rulePending,
  RULE_CREATE_REQUEST,
  RULE_DELETE_REQUEST,
  RULE_LIST_REQUEST,
  RULE_UPDATE_REQUEST,
} from "../actions/ruleAction";
import { deleteRule, listRule, postRule, putRule } from "../../apiRoutes/rule";

function* CreateRuleSaga(action: any): any {
  try {
    yield put(rulePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const newRule = yield call(postRule, action.payload.data);
    if (newRule.error) {
      action.payload.callbackError(newRule.message);
      return yield put(ruleFailure(newRule.message));
    }
    yield call(ListRuleSaga, { limit: 10, offset: 0 });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(ruleFailure("lỗi"));
  }
}

  function* UpdateRuleSaga(action: any): any {
    try {
      yield put(rulePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
      const updateRule = yield call(putRule, {code: action.payload.code, body: action.payload.body});
      if (updateRule.error) {
       return yield put(ruleFailure(updateRule.message));
      }
       yield call(ListRuleSaga, { payload: {limit: 10, offset: 0} });
      action.payload.callbackSuccess();
    } catch (error) {
      yield put(ruleFailure("lỗi"));
    }
  }

function* ListRuleSaga(action: any): any {
  try {
    yield put(rulePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    let option = { limit: action.payload.limit || 10, offset:  action.payload.offset ||0  } as any
    if(action.payload.name ){
      option.name = action.payload.name 
    }
    const listRules = yield call(listRule, option);
    if (listRules.error) {
      yield put(ruleFailure(listRules.message));
    }
    yield put(ruleListSuccess(listRules.data));
  } catch (error) {
    yield put(ruleFailure("lỗi"));
  }
}

function* DeleteRuleSaga(action: any): any {
  try {
    yield put(rulePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const deleted = yield call(deleteRule, action.payload.data);
    if (deleted.error) {
      return yield put(ruleFailure(deleted.message));
    }
    yield call(ListRuleSaga, { payload: {limit: 10, offset: 0} });
  } catch (error) {
    yield put(ruleFailure("lỗi"));
  }
}



function* CustomerSaga() {
  yield takeLatest(RULE_CREATE_REQUEST, CreateRuleSaga);
  yield takeLatest(RULE_LIST_REQUEST, ListRuleSaga);
  yield takeLatest(RULE_DELETE_REQUEST, DeleteRuleSaga);
  yield takeLatest(RULE_UPDATE_REQUEST, UpdateRuleSaga);
}

export default CustomerSaga;
