// store/sagas/yourSaga.js
import { takeLatest, put, call, throttle } from "redux-saga/effects";
import {
  ticketListSuccess,
  ticketFailure,
  ticketPending,
  TICKET_CREATE_REQUEST,
  TICKET_DELETE_REQUEST,
  TICKET_LIST_REQUEST,
  TICKET_UPDATE_REQUEST,
} from "../actions/ticketAction";
import {
  deleteTicket,
  listTicket,
  postTicket,
  putTicket,
} from "../../apiRoutes/ticket";
import { ListAccount } from "../../apiRoutes/acount";

function* CreateTicketSaga(action: any): any {
  try {
    yield put(ticketPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const newTicket = yield call(postTicket, action.payload.data);
    if (newTicket.error) {
      action.payload.callbackError(newTicket.message);
      return yield put(ticketFailure(newTicket.message));
    }
    yield call(ListTicketSaga, { limit: 10, offset: 0 });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(ticketFailure("lỗi"));
  }
}

function* UpdateTicketSaga(action: any): any {
  try {
    console.log(action,"action")
    yield put(ticketPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const updateTicket = yield call(putTicket, {
      code: action.payload.data.code,
      body: action.payload.data.body,
    });
    if (updateTicket.error) {
      action.payload.callbackError(updateTicket.message);
      return yield put(ticketFailure(updateTicket.message));
    }
    yield call(ListTicketSaga, { payload: { limit: 10, offset: 0 } });
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(ticketFailure("lỗi"));
  }
}

function* ListTicketSaga(action: any): any {
  try {
    yield put(ticketPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const listTickets = yield call(listTicket, action.payload);
    const listAccount = yield call(ListAccount, {});
    if (listTickets.error) {
      action.payload.callbackError(listTickets.message);
      yield put(ticketFailure(listTickets.message));
    }
    yield put(ticketListSuccess({ticket:listTickets.data,account:listAccount.data}));
  } catch (error) {
    yield put(ticketFailure("lỗi"));
  }
}

function* DeleteTicketSaga(action: any): any {
  try {
    yield put(ticketPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const deleted = yield call(deleteTicket, action.payload.data);
    if (deleted.error) {
      action.payload.callbackError(deleted.message);
      return yield put(ticketFailure(deleted.message));
    }
    yield call(ListTicketSaga, { payload: { limit: 10, offset: 0 } });
  } catch (error) {
    yield put(ticketFailure("lỗi"));
  }
}

function* TicketSaga() {
  yield takeLatest(TICKET_CREATE_REQUEST, CreateTicketSaga);
  yield takeLatest(TICKET_LIST_REQUEST, ListTicketSaga);
  yield takeLatest(TICKET_DELETE_REQUEST, DeleteTicketSaga);
  yield takeLatest(TICKET_UPDATE_REQUEST, UpdateTicketSaga);
}

export default TicketSaga;
