// store/sagas/yourSaga.js
import { takeLatest, put, call, throttle } from "redux-saga/effects";
import {
  brandnameListSuccess,
  brandnameFailure,
  brandnamePending,
  BRANDNAME_CREATE_REQUEST,
  BRANDNAME_DELETE_REQUEST,
  BRANDNAME_LIST_REQUEST,
  BRANDNAME_UPDATE_REQUEST,
} from "../actions/brandnameAction";
import {
  deleteBrandname,
  listBrandname,
  postBrandname,
  putBrandname,
} from "../../apiRoutes/brandname";
import { formatSelecter_Form } from "../../constants/formats";

function* CreateBrandnameSaga(action: any): any {
  try {
    yield put(brandnamePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const newBrandname = yield call(postBrandname, action.payload.data);
    if (newBrandname.error) {
      action.payload.callbackError(newBrandname.message);
      return yield put(brandnameFailure(newBrandname.message));
    }
    yield call(ListBrandnameSaga, {payload: {data:{ limit: 10, offset: 0 }}});
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(brandnameFailure("lỗi"));
  }
}

function* UpdateBrandnameSaga(action: any): any {
  try {
    console.log(action,"action")
    yield put(brandnamePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const updateBrandname = yield call(putBrandname, {
      code: action.payload.data.code,
      body: action.payload.data.body,
    });
    if (updateBrandname.error) {
      action.payload.callbackError(updateBrandname.message);
      return yield put(brandnameFailure(updateBrandname.message));
    }
    yield call(ListBrandnameSaga, {payload: {data:{ limit: 10, offset: 0 }}});
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(brandnameFailure("lỗi"));
  }
}

function* DeleteBrandnameSaga(action: any): any {
  console.log(action,"actionactionactionaction")
  try {
    yield put(brandnamePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const deleted = yield call(deleteBrandname, action.payload.data);
    if (deleted.error) {
      action.payload.callbackError(deleted.message);
      return yield put(brandnameFailure(deleted.message));
    }
    yield call(ListBrandnameSaga, {payload: {data:{ limit: 10, offset: 0 }}});
    action.payload.callbackSuccess();
  } catch (error) {
    yield put(brandnameFailure("lỗi"));
  }
}

function* ListBrandnameSaga(action: any): any {
  
    try {
      yield put(brandnamePending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
      const listBrandnames = yield call(listBrandname, action.payload.data);
      console.log(listBrandnames,"listBrandnameslistBrandnameslistBrandnames")
      if (listBrandnames.error) {
        action.payload.callbackError(listBrandnames.message);
        yield put(brandnameFailure(listBrandnames.message));
      }
      if(action.payload.callbackSuccess){
        action.payload.callbackSuccess(listBrandnames.data.list.map((value:any)=> formatSelecter_Form(value.Code, value.Brandname)))
      }
      yield put(brandnameListSuccess(listBrandnames.data));
    } catch (error) {
      yield put(brandnameFailure("lỗi"));
    }
  }

function* BrandnameSaga() {
  yield takeLatest(BRANDNAME_CREATE_REQUEST, CreateBrandnameSaga);
  yield takeLatest(BRANDNAME_LIST_REQUEST, ListBrandnameSaga);
  yield takeLatest(BRANDNAME_DELETE_REQUEST, DeleteBrandnameSaga);
  yield takeLatest(BRANDNAME_UPDATE_REQUEST, UpdateBrandnameSaga);
}

export default BrandnameSaga;
