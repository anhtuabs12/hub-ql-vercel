// store/sagas/yourSaga.js
import { takeLatest, put, call, throttle } from "redux-saga/effects";
import {
  LOG_LIST_REQUEST,
  logFailure,
  logListSuccess,
  logPending,
} from "../actions/logAction";
import { ListLogs } from "../../apiRoutes/log";

function* ListLogSaga(action: any): any {
  try {
    yield put(logPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
    const listLog = yield call(ListLogs, action.payload);
    if (listLog.error) {
      action.payload.callbackError(listLog.message);
      return yield put(logFailure(listLog.message));
    }
    yield put(logListSuccess(listLog.data));
  } catch (error) {
    yield put(logFailure("lỗi"));
  }
}

// function* CreateAccountSaga(action: any): any {
//   try {
//     yield put(accountPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
//     const newAccount = yield call(CreateAccount, action.payload.data);
//     if (newAccount.error) {
//       action.payload.callbackError(newAccount.message);
//       return yield put(accountFailure(newAccount.message));
//     }
//     yield call(ListAccountSaga, { payload: { limit: 10, offset: 0 } });
//     action.payload.callbackSuccess();
//   } catch (error) {
//     yield put(accountFailure("lỗi"));
//   }
// }

// function* DeleteAccountSaga(action: any): any {
//   try {
//     yield put(accountPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
//     const deleted = yield call(DeleteAccount, action.payload.data);
//     if (deleted.error) {
//       yield put(accountFailure(deleted.message));
//     }
//     yield call(ListAccountSaga, { payload: { limit: 10, offset: 0 } });
//   } catch (error) {
//     yield put(accountFailure("lỗi"));
//   }
// }

// function* UpdateAccountSaga(action: any): any {
//   try {
//     yield put(accountPending(true)); // Dispatch action LOGIN_PENDING trước khi gọi API
//     const update = yield call(UpdateAccount, action.payload.data);
//     if (update.error) {
//       action.payload.callbackError(update.message);
//       yield put(accountFailure(update.message));
//     }
//     yield call(ListAccountSaga, { payload: { limit: 10, offset: 0 } });
//     action.payload.callbackSuccess();
//   } catch (error) {
//     yield put(accountFailure("lỗi"));
//   }
// }

function* LogSaga() {
  yield takeLatest(LOG_LIST_REQUEST, ListLogSaga);
  //   yield takeLatest(ACCOUNT_CREATE_REQUEST, CreateAccountSaga);
  //   yield takeLatest(ACCOUNT_DELETE_REQUEST, DeleteAccountSaga);
  //   yield takeLatest(ACCOUNT_UPDATE_REQUEST, UpdateAccountSaga);
}

export default LogSaga;
