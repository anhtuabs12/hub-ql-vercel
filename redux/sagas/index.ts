// store/sagas/index.js
import { all } from "redux-saga/effects";
import AuthSaga from "./authSaga";
import AccountSaga from "./acountSaga";
import RuleSaga from "./ruleSaga";
import TicketSaga from "./ticketSaga";
import HotlineSaga from "./hotlineSaga";
import BrandnameSaga from "./brandnameSaga";
import LogSaga from "./logSaga";

function* rootSaga() {
  yield all([
    AuthSaga(),
    AccountSaga(),
    RuleSaga(),
    TicketSaga(),
    HotlineSaga(),
    BrandnameSaga(),
    LogSaga(),

    // Add other sagas here
  ]);
}

export default rootSaga;
