import { NetWorkList } from "../config";
import { formatDateTime } from "../formats";
import { checkStaffAuth } from "../permissions";

export const ColunmHotlineExcell = (DataColums?: any) => {
    let accountInfo = DataColums?.accountInfo;
    let handleEdit = DataColums?.HandleEdit;
    let handleDelete = DataColums?.HandleDelete;
    let deletedAction = DataColums?.deletedAction;
    let handleShowLog = DataColums?.handleShowLog;
    return [
      {
        title: "STT",
        dataIndex: "STT",
        key: "STT",
        width: 60,
        fixed: "left",
        // render: (text: any, record: any, index: any) => record.key + 1,
      },
      {
        title: "Hotline",
        dataIndex: "Hotline",
        key: "Hotline",
        fixed: "left",
        width: 200,
      },
      {
        title: "Nhà mạng",
        dataIndex: "Type_NetWork",
        key: "Type_NetWork",
        align: "center",
        width: 200,
        render: (value: any) => {
          return NetWorkList.find((item: any) => item.value == value)?.label;
        },
      },

      {
        title: "Khách hàng",
        dataIndex: ["CustomerInfo", "companyname"],
        hidden: !checkStaffAuth(accountInfo),
        align: "center",
        key: ["CustomerInfo", "companyname"],
        width: 200,
      },
      {
        title: "Ngày cấp cho khách hàng",
        dataIndex: "Lunch_date",
        align: "center",
        key: "Lunch_date",
        width: 200,
        render: (value: any) => formatDateTime(value),
      },
      {
        title: "Brand name",
        dataIndex: ["BrandnameInfo", "Brandname"],
        align: "center",
        key: ["BrandnameInfo", "Brandname"],
        width: 200,
      },
      {
        title: "Ngày mua",
        dataIndex: "PurchaseDate",
        align: "center",
        key: "PurchaseDate",
        width: 200,
        render: (value: any) => formatDateTime(value),
      },
      {
        title: "Giữ đến ngày",
        dataIndex: "Extention_periodDate",
        align: "center",
        key: "Extention_periodDate",
        width: 200,
        render: (value: any) => formatDateTime(value),
      },
  
      {
        title: "Khách hàng cuối",
        dataIndex: "Customer_end",
        key: "Customer_end",
        width: 200,
      },
      {
        title: "Ngày tạo",
        dataIndex: "Created_at",
        align: "center",
        key: "Created_at",
        width: 200,
        render: (value: any) => formatDateTime(value),
      },
      {
        title: "Ngày sửa",
        dataIndex: "Updated_at",
        align: "center",
        key: "Updated_at",
        width: 200,
        render: (value: any) => formatDateTime(value),
      },
      {
        title: "Trạng thái",
        dataIndex: "Status",
        align: "center",
        key: "Status",
        width: 150,
        fixed: "right",
      },
      
    ];
  };