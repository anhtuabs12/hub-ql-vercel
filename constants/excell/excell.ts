// import React from "react";
// import ExcelJS from "exceljs";
// export const handleExportExcel = async (columns: any, data: any) => {
//   const workbook = new ExcelJS.Workbook();
//   const worksheet = workbook.addWorksheet("Sheet 1");

//   // Add column headers
//   worksheet.columns = columns.map((col: any) => ({
//     header: col.title,
//     key: col.dataIndex || col.key,
//     width: col.width ? col.width / 10 : undefined, // Convert pixel width to Excel column width
//   }));

//   // Add rows
//   data.forEach((row: any, index: any) => {
//     const excelRow = {} as any;
//     columns.forEach((col: any) => {
//       let value = row[col.dataIndex];
//       if (col.render) {
//         // If there's a custom render function, use it
//         value = col.render(value, row, index);
//       } else if (Array.isArray(col.dataIndex)) {
//         // If dataIndex is an array (for nested fields)
//         value = col.dataIndex.reduce((acc: any, key: any) => acc[key], row);
//       }
//       excelRow[col.key] = value;
//     });
//     worksheet.addRow(excelRow);
//   });

//   // Style the rows
//   worksheet.eachRow((row, rowNumber) => {
//     row.eachCell((cell, colNumber) => {
//       const colDef = columns[colNumber - 1];
//       if (colDef.key === "Status") {
//         const statusValue = data[rowNumber - 1].Status;
//         const backgroundColor = colDef.statusColors[statusValue] || "#FFFFFF";
//         cell.fill = {
//           type: "pattern",
//           pattern: "solid",
//           fgColor: { argb: backgroundColor.replace("#", "") },
//         };
//         cell.font = {
//           color: { argb: "FFFFFF" },
//           bold: true,
//         };
//       }
//     });
//   });

//   const buffer = await workbook.xlsx.writeBuffer();
//   const blob = new Blob([buffer], { type: "application/octet-stream" });
//   const url = window.URL.createObjectURL(blob);
//   const a = document.createElement("a");
//   a.href = url;
//   a.download = "exceljs-demo.xlsx";
//   document.body.appendChild(a);
//   a.click();
//   window.URL.revokeObjectURL(url);
//   document.body.removeChild(a);
// };
import ExcelJS from "exceljs";

export const handleExportExcel = async (columns:any, data:any) => {
  const workbook = new ExcelJS.Workbook();
  const worksheet = workbook.addWorksheet("Sheet 1");

  // Add column headers
  worksheet.columns = columns.map((col:any) => ({
    header: col.title,
    key: col.dataIndex || col.key,
    width: col.width ? col.width / 10 : undefined, // Convert pixel width to Excel column width
  }));

  // Add rows
  data.forEach((row:any, index:any) => {
    const excelRow = {} as any;
    columns.forEach((col:any) => {
      let value = row;
      if (Array.isArray(col.dataIndex)) {
        // If dataIndex is an array (for nested fields)
        col.dataIndex.forEach((key:any) => {
          value = value && value[key] !== undefined ? value[key] : null;
        });
      } else {
        value = row[col.dataIndex];
      }
      if (col.render) {
        // If there's a custom render function, use it
        value = col.render(value, row, index);
      }
      excelRow[col.key] = value !== undefined ? value : '';
    });
    worksheet.addRow(excelRow);
  });

  const buffer = await workbook.xlsx.writeBuffer();
  const blob = new Blob([buffer], { type: "application/octet-stream" });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement("a");
  a.href = url;
  a.download = "exceljs-demo.xlsx";
  document.body.appendChild(a);
  a.click();
  window.URL.revokeObjectURL(url);
  document.body.removeChild(a);
};
