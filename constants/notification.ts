import { NotificationType } from "@/pages/login";

export   const openNotificationWithIcon = ( api:any, type: NotificationType, message:string , description:string) => {
    api[type]({
      message,
      description
    });
  };