export enum SORTDATE_TYPE {
  DAY,
  WEEK,
  MONTH,
}
export enum TICKETSTATUS_TYPE {
  WAITING = 1,
  AGREE = 2,
  DISAGREE = 3,
}

export enum BRANDNAMESTATUS_TYPE {
  Pause=1,
  Activate=2,
}


export enum NETWORK_TYPE {
  VIETTEL= "1",
  VINA="2",
  MOBI ="3"
}

export enum TICKET_TYPE {
  WITHDRAWAL = 1,
  CANCEL_NUMBER = 2,
  UNUSED = 3,
  DOING = 4,
  WAITING_BRANDNAME = 5,
  KEEPING = 6,
}

export enum ROLE_TYPE {
  ADMINISTRATOR = "administrator",
  SALEADMIN = "saleadmin",
  CUSTOMER = "customer",
}

export enum HOTLINE_STATUS {
  READY = "ready",
  KEEPING = "keeping",
  WAITINGBRANDNAME = "waitbrandname",
  DOING = "doing",
  EVICT = "evict",
  OFF = "off",

}


export enum  SOCKET_TYPE{
  CHECK_LOGINED = "socket-login-token",
  REQEST_RELOAD_HOTLINE = "request-reload-hotline",
}

export enum  LOG_TYPE{
  CREATE = "create",
  UPDATE = "update",
}


export const statusColors: any = {
  ready: "#008cff",
  keeping: "#a461d8",
  waitbrandname: "#3e82f7",
  doing: "#04d182",
  evict: "#ff6b72  ",
  off: "#FFFF99",
};
