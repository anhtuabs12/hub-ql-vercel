export const NetWorkList = [
  { value: "1", label: "Viettel" },
  { value: "2", label: "Vina" },
  { value: "3", label: "Mobi" },
];

export const HotlineStatusList = [
  { value: "ready", label: "Chưa sử dụng" },
  { value: "keeping", label: "Đang giữ" },
  { value: "waitbrandname", label: "Chờ gán name" },
  { value: "doing", label: "Đang sử dụng" },
  { value: "evict", label: "Thu hồi" },
  { value: "off", label: "Hủy" },
];

export const StatusTicket = [
  { value: 1, label: "Đang chờ" },
  { value: 2, label: "Đã xác nhận" },
  { value: 3, label: "Đã hủy" },
];

export const TicketType = [
  { value: 1, label: "Trạng thái từ đạng sử dụng =>  thu hồi" },
  { value: 2, label: "Trạng thái thu hồi =>  hủy số" },
  { value: 3, label: "Trạng thái thu hồi => chưa sử dụng" },
];

export const StatusBrandname = [
  { value: "1", label: "Tạm ngưng" },
  { value: "2", label: "Kích hoạt" },
];

export const Role = [
  { value: "administrator", label: "ADMINISTRATOR" },
  { value: "saleadmin", label: "SALEADMIN" },
  { value: "customer", label: "CUSTOMER" },
];

export const TypeLogs = [
  { value: "update", label: "Cập nhật" },
  { value: "create", label: "Tạo mới" },
];

export const logTypeKey = [
  { value: "status_keeping", label: "Cập nhật thành Giữ số" },
  { value: "status_evicting", label: "Cập nhật thành số bị thu hồi" },
  { value: "status_canceling", label: "Cập nhật thành số bị hủy" },
  { value: "status_ready", label: "Cập nhật thành số chưa sử dụng" },
  { value: "status_doing", label: "Cập nhật thành số đang sử dụng" },
  {
    value: "status_waitingbrandname",
    label: "Cập nhật thành số chờ gắn hame",
  },
  {value: "Type_NetWork" , label: "Nhà mạng"},
  {value: "PurchaseDate" , label: "Ngày mua"},
  {value: "Lunch_date" , label: "Ngày cấp"},
  {value: "Status" , label: "Trạng thái"},
  {value: "BrandName" , label: "Brand name"},

];
