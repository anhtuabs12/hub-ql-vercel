import dayjs from "dayjs";

export const formatDateTime = (dateTimeString: any) => {
  if(!dateTimeString){
    return null
  }
  const formattedDateTime = dayjs(dateTimeString).format("HH:mm - DD/MM/YYYY");
  return formattedDateTime;
};

export const formatSelecter_Form =(value:any, label:any) =>{
  return (
      {value, label}
  )
}