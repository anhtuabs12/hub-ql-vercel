export  const checkAdminAuth = (accountInfo:any) => {
    if(accountInfo){
     return accountInfo.Role == "administrator"
    }else{
      return false
    }
  };

  export  const checkStaffAuth = (accountInfo:any) => {
    if(accountInfo){
     return accountInfo.Role != "customer"
    }else{
      return false
    }
  };

  export  const checkStaffSaleAdminAuth = (accountInfo:any) => {
    if(accountInfo){
     return accountInfo.Role == "saleadmin"
    }else{
      return false
    }
  };



  export  const checkLogined = (accountInfo:any) => {
    if(accountInfo){
     return accountInfo.Role == 0
    }else{
      return false
    }
  };