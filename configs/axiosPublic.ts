import axios from "axios";

const axiosPublic = axios.create({
//   baseURL: "https://jsonplaceholder.typicode.com/",
  headers: {
    "Content-Type": "application/json",
  },
});

axiosPublic.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (err) => {
    return Promise.reject(err);
  }
);

const getPublic = async (url:any, body?:any)=>{
    try {
     return await axiosPublic.get( url, body);
    } catch (error) {
      throw error;
    }
  }

  const putPublic = async (url:any, body?:any)=>{
    try {
      return await axiosPublic.put( url, body);
     } catch (error) {
       throw error;
     }
  }

  const patchPublic = async (url:any, body?:any)=>{
    try {
      return await axiosPublic.patch( url, body);
     } catch (error) {
       throw error;
     }
  }

  const deletePublic  = async (url:any, body?:any)=>{
    try {
      return await axiosPublic.delete( url, body);
     } catch (error) {
       throw error;
     }
  }

  const postPublic = async (url:any, body?:any)=>{
    try {
      return await axiosPublic.post( url, body);
     } catch (error) {
       throw error;
     }
  }

export { axiosPublic , postPublic, getPublic, putPublic, patchPublic, deletePublic};