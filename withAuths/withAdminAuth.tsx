import { useRouter } from "next/router";
import React, { ComponentType, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { detail_accountRequest } from "../redux/actions/authActions";
import { checkAdminAuth } from "../constants/permissions";
import { accountInfoRequest } from "../redux/actions/authActions";
import withLogin from "./withLogin";

type Props = {};

const withAdminAuth = <P extends object>(
  WrappedComponent: ComponentType<P>
) => {
  
  const AuthComponent = (props: P) => {
    const router = useRouter();
    const { accountInfo } = useSelector((state: any) => state.authReducer);

    useEffect(() => {
      if (!accountInfo) {
      } else {
        let isAdmin = checkAdminAuth(accountInfo); // Kiểm tra quyền admin
        if (!isAdmin) {
          router.push("/admin/hotline"); // Chuyển hướng nếu không có quyền
        }
      }
    }, [accountInfo]);

    return <WrappedComponent {...props} />;
  };

  return withLogin(AuthComponent) ;
};

export default withAdminAuth;
