import { useRouter } from "next/router";
import React, { ComponentType, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { accountInfoRequest } from "../redux/actions/authActions";
import { checkAdminAuth } from "../constants/permissions";
import { io } from "socket.io-client";

type Props = {};

const withUnLogin = <P extends object>(
  WrappedComponent: ComponentType<P>
) => {
  const AuthComponent = (props: P) => {
    const router = useRouter();
    const dispatch = useDispatch()
    const {  accountInfo } = useSelector(
      (state: any) => state.authReducer
    );

    const checkPermission = ()=>{
        dispatch(
          accountInfoRequest({
              callbackSuccess: () => {
                router.push("/");
              },
            })
          );
    }

    useEffect(() => {
      const socket = io('http://localhost:3001'); // Thay đổi URL này thành URL của máy chủ Socket.IO của bạn
      
      socket.on('connect', () => {
          console.log('Connected to server');
          socket.emit('sendMessage', 'Hello server');
      });

      socket.on('disconnect', () => {
          console.log('Disconnected from server');
      });

      socket.on('socket-login-token', (message) => {
        console.log('socket-login-token:', message);
    });

      return () => {
          socket.disconnect();
      };
  }, []);

    useEffect(() => {
        checkPermission()
      }, []);

    return <WrappedComponent {...props} />;
  };

  return AuthComponent;
};

export default withUnLogin;
