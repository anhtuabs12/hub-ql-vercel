import { useRouter } from "next/router";
import React, { ComponentType, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  accountInfoRequest,
  logoutRequest,
} from "../redux/actions/authActions";
import { checkAdminAuth } from "../constants/permissions";
import { io } from "socket.io-client";
import { SOCKET_TYPE } from "../constants/enum";
import { hotlineListRequest } from "../redux/actions/hotlineAction";

type Props = {};

const withLogin = <P extends object>(WrappedComponent: ComponentType<P>) => {
  const AuthComponent = (props: P) => {
    const router = useRouter();
    const dispatch = useDispatch();
    const { accountInfo } = useSelector((state: any) => state.authReducer);

    const checkPermission = () => {
      dispatch(
        accountInfoRequest({
          callbackSuccess: () => {
            // router.push("/");
          },

          callbackerror: () => {
            router.push("/login");
          },
        })
      );
    };

    useEffect(() => {
      const socket = io("http://localhost:3001"); // Thay đổi URL này thành URL của máy chủ Socket.IO của bạn

      socket.on("connect", () => {
        console.log("Connected to server");
        socket.emit("sendMessage", "Hello server");
      });

      socket.on("disconnect", () => {
        console.log("Disconnected from server");
      });

      socket.on(SOCKET_TYPE.CHECK_LOGINED, (data) => {
        const accessToken = localStorage.getItem("hub_token");
        const key = localStorage.getItem("hub_key");
        if (accessToken) {
          if (data.newToken !== accessToken && data.key == key) {
            dispatch(
              logoutRequest({
                callback: () => {
                  router.push("/login");
                },
              })
            );
          }
        }
      });

      // socket.on(SOCKET_TYPE.REQEST_RELOAD_HOTLINE, (data) => {
      //   if (data.status) {
      //     dispatch(hotlineListRequest({ offset: (1 - 1) * 10, limit: 10 }));
      //   }
      // });

      // return () => {
      //   socket.disconnect();
      // };
    }, []);

    useEffect(() => {
      checkPermission();
    }, []);

    return <WrappedComponent {...props} />;
  };

  return AuthComponent;
};

export default withLogin;
